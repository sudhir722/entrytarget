package android.sud.com.entryntarget;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.sud.com.entryntarget.corelogic.OrderInfo;
import android.sud.com.entryntarget.corelogic.StockSdk;
import android.sud.com.entryntarget.corelogic.jobs.ReminderUtilities;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.database.DatabaseConstants;
import android.sud.com.entryntarget.fragments.DashboardFragment;
import android.sud.com.entryntarget.fragments.LoginFragment;
import android.sud.com.entryntarget.fragments.UpstoxConnectionFragment;
import android.sud.com.entryntarget.fragments.UserProfileFragment;
import android.sud.com.entryntarget.fragments.interfaces.OnFragmentInteractionListener;
import android.sud.com.entryntarget.utility.EntConstants;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.sud.com.entryntarget.utility.EntConstants.ACTION_LOGIN_SUCCESS;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener,View.OnClickListener{

    private static final int READ_EXTERNAL_STORAGE_PERMISSION_CODE = 1212;
    private LoginFragment loginFragment = new LoginFragment();
    private UserProfileFragment userProfileFragment = new UserProfileFragment();
    private DashboardFragment dashboardFragment = new DashboardFragment();;
    FragmentManager fragmentManager = getSupportFragmentManager();

    private FirebaseAuth mAuth;
    FloatingActionButton fabActionRefresh;
    private String TAG ="MainAct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fabActionRefresh = (FloatingActionButton) findViewById(R.id.fab);
        fabActionRefresh.setOnClickListener(this);
        /*DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_ORDER_INFO);
        DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_TRIGGER_INFO);*/

        mAuth = FirebaseAuth.getInstance();
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                READ_EXTERNAL_STORAGE_PERMISSION_CODE);

        if(mAuth.getCurrentUser()!=null){
            EntPreferences.setRegistration(getApplicationContext(),true);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.main_content, dashboardFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();

        }else{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.main_content, loginFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }

        EntPreferences.setUpstoxKeyOrderType(this,"");

        String[] stockNameList = getResources().getStringArray(R.array.stocklist);
        insertFavlist(stockNameList);

        EntPreferences.putBoolean(this, EntConstants.IS_PAID,true);
        ReminderUtilities.initSDK(this);
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentmin = rightNow.get(Calendar.MINUTE);

        if(currentHour >= 9 && currentHour < 16) {
            if(currentHour ==9 && currentmin <14){
                DBAdapter.getDBAdapter(this).clearOldData();
            }else if(currentHour==10 && currentmin>20){
                DBAdapter.getDBAdapter(this).closeAllOrders();
            }else if(currentHour>16 ){
                DBAdapter.getDBAdapter(this).closeAllOrders();
            }else {
                StockSdk.getInstance().retriveStockInfo(this, null);
            }
        }else{
            DBAdapter.getDBAdapter(this).closeAllOrders();
        }
        //StockSdk.getInstance().retriveStockInfo(this, null);
        //DBAdapter.getDBAdapter(this).canclePendingOrder("RAMCOCEM");
    }

    private void addSudhirFavList(){
        String[] favList = new String[]{
                "WABCOINDIA",
                "biocon",
                "escorts",
                "DIVISLAB",
                "bharatfin",
                "cholafin",
                "pel",
                "CENTURYTEX",
                "M&M",
                "srf",
                "indusindbk",
                "heromotoco",
                "maruti"
        };
        DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        insertFavlist(favList);
    }


    private void addAmarStocks(){
        //DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        String[] favList = new String[]{
                "HDFCBANK",
                "LT",
                "LUPIN",
                "TATASTEEL",
                "NESTLEIND",
                "WIPRO",
                "HDFC",
                "AUROPHARMA",
                "JUBLFOOD",
                "TECHM",
                "ASIANPAINT",
                "MCDOWELL-N",
                "BATAINDIA",
                "GRASIM",
                "ULTRACEMCO",
                "SUNPHARMA",
                "APOLLOHOSP",
                "OFSS",
                "TORNTPHARM",
                "COLPAL",
                "RBLBANK",
                "MINDTREE",
                "BIOCON",
                "DALMIABHA",
                "HAVELLS",
                "CEATLTD",
                "MARUTI",
                "BAJAJ-AUTO",
                "VEDL",
                "CENTURYTEX",
                "UBL",
                "MUTHOOTFIN",
                "HEROMOTOCO",
                "YESBANK",
                "INFIBEAM"
        };
        insertFavlist(favList);
    }
    private void insertFavlist(String[] favList){
        DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        for (int index=0;index<favList.length;index++){
            DBAdapter.getDBAdapter(this).insertFavSymbol(favList[index]);
        }
    }

    private void addFav(){
        //DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        StockSdk.getInstance().retriveStockInfo(this, null);

        List<String> stocksList = new ArrayList<>();
        stocksList.add("JUSTDIAL");
        stocksList.add("TCS");
        stocksList.add("RELIANCE");
        stocksList.add("LUPIN");
        stocksList.add("CIPLA");
        /*stocksList.add("BOSCHLTD");
        stocksList.add("MARUTI");
        stocksList.add("ULTRACEMCO");
        stocksList.add("INDUSINDBK");
        stocksList.add("BAJFINANCE");
        stocksList.add("BAJAJ-AUTO");
        stocksList.add("DRREDDY");
        stocksList.add("TCS");
        stocksList.add("RELIANCE");
        stocksList.add("LUPIN");
        stocksList.add("CIPLA");
        stocksList.add("IBULHSGFIN");
        stocksList.add("INFY");
        stocksList.add("KOTAKBANK");
        stocksList.add("BRITANNIA");
        stocksList.add("TORNTPHARM");
        stocksList.add("ACC");
        stocksList.add("SRTRANSFIN");*/
        for (int index=0;index<stocksList.size();index++){
            DBAdapter.getDBAdapter(this).insertFavSymbol(stocksList.get(index));
        }
    }

    private void sendMail(){

        File sd = Environment.getExternalStorageDirectory();
        String filename  = DBAdapter.DATABASENAME;

        File file = new File(sd, filename);
        if (!file.exists() || !file.canRead()) {
            return;
        }
        Uri uri = Uri.fromFile(file);


        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // set the type to 'email'
        emailIntent.setType("*/*");
        String to[] = {"patilsudhir722@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        //emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+filename));
// the attachment
        //emailIntent .putExtra(Intent.EXTRA_STREAM, path);
// the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        startActivity(Intent.createChooser(emailIntent , "Send email..."));
    }

    private void sendIntentToGmailApp(File fileToSend) {
        if(fileToSend != null){
            Intent email = new Intent(Intent.ACTION_SEND);
            email.putExtra(Intent.EXTRA_SUBJECT, "Send Text File As Attachment Example");
            email.putExtra(Intent.EXTRA_TEXT, "This is todays data");
            email.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + fileToSend.getAbsoluteFile()));
            email.setType("message/rfc822");
            startActivity(Intent.createChooser(email , "Send Text File"));
        }
    }

    private void exportDatabase(){
            // TODO Auto-generated method stub
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String  currentDBPath= "//data//" + getPackageName()
                        + "//databases//" + DBAdapter.DATABASENAME;
                String backupDBPath  = "/"+DBAdapter.DATABASENAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);


                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(getBaseContext(), backupDB.toString(),
                        Toast.LENGTH_LONG).show();
                sendMail();
            }
        } catch (Exception e) {

            Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }



    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_dashboard, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.user:
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        if(userProfileFragment.isAdded()){
                            fragmentTransaction.remove(userProfileFragment);
                            fragmentTransaction.commitAllowingStateLoss();

                        }
                        fragmentTransaction = fragmentManager.beginTransaction();

                        fragmentTransaction.replace(R.id.main_content, userProfileFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commitAllowingStateLoss();
                        return true;
                    case R.id.export:
                        //archive(item);
                        exportDatabase();
                        Toast.makeText(getApplicationContext(),"Export process has been created..",Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.report:
                        //delete(item);
                        Toast.makeText(getApplicationContext(),"Functionality is under development...",Toast.LENGTH_SHORT).show();
                        //startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                        return true;
                    case R.id.upstox_connect:
                        Intent intent = new Intent(MainActivity.this, UpstoxConnectionFragment.class);
                        startActivity(intent);
                        return true;

                    default:
                        return false;
                }
            }
        });
        popup.show();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL_STORAGE_PERMISSION_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        if(!TextUtils.isEmpty(EntPreferences.getUpstoxToken(MainActivity.this))) {
                            //FileUtil.getInstance(MainActivity.this).getHistory();
                        }
                    }
                }
            }
        }
    }

    private void addNikhilsFavList(){
        DBAdapter.getDBAdapter(this).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        String[] favList = new String[]{
                "BAJAJFINSV",
                "OFSS",
                "HINDUNILVR",
                "ULTRACEMCO",
                "HEROMOTOCO",
                "BAJAJ-AUTO",
                "DRREDDY",
                "TCS",
                "HDFCBANK",
                "HDFC",
                "INDUSINDBK",
                "KOTAKBANK",
                "IBULHSGFIN",
                "LUPIN",
                "TITAN",
                "INFY",
                "SUNPHARMA",
                "TATASTEEL",
                "AXISBANK",
                "RBLBANK",
                "LICHSGFIN",
                "M&MFIN",
                "YESBANK",
                "SRTRANSFIN",
                "BRITANNIA"
        };
        insertFavlist(favList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteractionChange(int action) {
        if(action == ACTION_LOGIN_SUCCESS){
            EntPreferences.setRegistration(this,true);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if(dashboardFragment.isAdded()) {
                fragmentTransaction.replace(R.id.main_content, dashboardFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            }else{
                fragmentTransaction.replace(R.id.main_content, dashboardFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==fabActionRefresh.getId()){
            dashboardFragment.refreshData();
        }else{
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }
}
