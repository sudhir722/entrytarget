/*
package android.sud.com.entryntarget;

ackage android.sud.com.entryntarget;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.os.Handler;
import android.sud.com.entryntarget.models.HeadingModel;
import android.sud.com.entryntarget.models.TriggerModel;
import android.sud.com.entryntarget.presenters.ListItemsPresenter;
import android.sud.com.entryntarget.ui.adapter.RecyclerViewBindingAdapter;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import static android.sud.com.entryntarget.BR.presenter;


public class RecyclerActivity extends AppCompatActivity implements ListItemsPresenter {

    private ActivityRecyclerBinding mBinding ;
    private ObservableList<RecyclerViewBindingAdapter.AdapterDataItem> listItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this, R.layout.activity_recycler);
        mBinding.setListLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mBinding.setModelList(initList());
        mBinding.setItemAnimator(new DefaultItemAnimator());
        loadDataWithDelay(1500);
    }

    private ObservableList initList() {
        listItems = new ObservableArrayList<>();
        listItems.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.layout_heading, new Pair<Integer, Object>(BR.headingModel, new HeadingModel("Content Heading"))));
        listItems.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.layout_loading));
        return listItems;
    }

    private void loadDataWithDelay(int delayMilli) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listItems.addAll(listItems.size()-1,getItems());//insert before loading cell
                listItems.remove(listItems.size()-1);//remove loading cell
                listItems.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.layout_load_more, new Pair<Integer, Object>(BR.presenter,RecyclerActivity.this)));
            }
        },delayMilli);
    }

    private List<RecyclerViewBindingAdapter.AdapterDataItem> getItems() {
        List list = new ArrayList();
        TriggerModel triggerModel = new TriggerModel("NIFTY", "BUY", "11500", "11500", "11450", "0.0", "T1 : 11550", "13 Sep");
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));
        list.add(new RecyclerViewBindingAdapter.AdapterDataItem(R.layout.list_row, new Pair<Integer, Object>(BR.triggerModel,triggerModel),
                new Pair<Integer, Object>(BR.triggerModel,presenter)));


        return list;
    }

    @Override
    public void onClick(TriggerModel itemModel) {

    }

    @Override
    public void onLoadMoreClick() {

    }
}
*/
