package android.sud.com.entryntarget.corelogic;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.sud.com.entryntarget.receivers.PostingReceiver;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;



/**
 * Created by prasma04 on 23-03-2018.
 */

public class MeterSdk {
    private final static int POSTING_INDEX = 10012;
    private final static long MIN_INTERVAL = 15000; // in miliseconds
    public final static int DEFAULT_INTERVAL = 1;//in minute
    private final static String TAG = "AlarmSchedular";
    private final static long ONE_MINUTE = (60 * 60);

    public static final String KEY_POSTING_INTERVAL = "posting_interval";
    public static final String KEY_LAST_SYNC = "last_sync";

    private static final MeterSdk ourInstance = new MeterSdk();


    public static MeterSdk getInstance() {
        return ourInstance;
    }

    private MeterSdk() {
    }

    public void setAlarm(Context context){
        if(context!=null && isAppRegistered()){
            if(!isAlarmSet(context)){
                //schedulePostingAlarm(context);
                schedulePostingAlarmSp(context);
            }
        }

    }
    private boolean isAlarmSet(Context context){
        return  (PendingIntent.getBroadcast(context, POSTING_INDEX,
                new Intent(context, PostingReceiver.class).setAction("POSTING_ACTION"),
                PendingIntent.FLAG_NO_CREATE) != null);
    }

    private boolean isAppRegistered(){
        return true;
    }

    public static void schedulePostingAlarm(Context context) {
        if (!TextUtils.isEmpty(context.getPackageName())) {
            int postingInterval = EntPreferences.getInt(context,KEY_POSTING_INTERVAL,DEFAULT_INTERVAL);

            Calendar calendar = Calendar.getInstance();
            long triggerTime = calendar.getTimeInMillis() + MIN_INTERVAL;
            long currentTime = System.currentTimeMillis();
            long lastSync = EntPreferences.getLong(context,KEY_LAST_SYNC,0);

            if (lastSync <= 0) {
                triggerTime += MIN_INTERVAL;
            } else {
                calendar.setTimeInMillis(lastSync);
                calendar.add(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                if (currentTime > calendar.getTimeInMillis()) {
                    calendar.add(Calendar.MINUTE, postingInterval);
                }
                if (!isValidInterval(calendar.getTimeInMillis(),postingInterval)) {
                    calendar.setTimeInMillis(currentTime + MIN_INTERVAL);
                }
                triggerTime = calendar.getTimeInMillis();
            }

            Intent postingAlarm = new Intent(context, PostingReceiver.class);
            postingAlarm.setAction("POSTING_ACTION");
            postingAlarm.putExtra("schedule", "Hourly");
            postingAlarm.putExtra("source", context.getPackageName());
            PendingIntent postingPendingIntent = PendingIntent.getBroadcast(context, POSTING_INDEX, postingAlarm, PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager postingArmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                postingArmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerTime, postingPendingIntent);
            } else {
                postingArmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                        triggerTime, ONE_MINUTE * postingInterval, postingPendingIntent);
            }
        }
    }

    public static void schedulePostingAlarmSp(Context context) {
        int interval = EntPreferences.getInt(context,KEY_POSTING_INTERVAL,DEFAULT_INTERVAL);
        long intervalInMillis = interval * 30 * 1000;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intentAlarm = new Intent(context, PostingReceiver.class).setAction("POSTING_ACTION");
            //Bundle extras = new Bundle();
            // extras.putInt(METER_ID, meterId);
            Log.d(TAG, "posting scheduled");
            //intentAlarm.putExtras(extras);
            PendingIntent intervalPendingIntent = PendingIntent.getBroadcast(context,
                    POSTING_INDEX, intentAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + intervalInMillis, intervalPendingIntent);
        } else {
            boolean alarmCollection = (PendingIntent.getBroadcast(context, POSTING_INDEX,
                    new Intent(context, PostingReceiver.class),
                    PendingIntent.FLAG_NO_CREATE) != null);
            if (!alarmCollection) {
                Intent intentAlarm = new Intent(context, PostingReceiver.class).setAction("POSTING_ACTION");
                //Bundle extras = new Bundle();
                // extras.putInt(METER_ID, meterId);
                Log.d(TAG, "posting scheduled");
                //intentAlarm.putExtras(extras);
                PendingIntent intervalPendingIntent = PendingIntent.getBroadcast(context,
                        POSTING_INDEX, intentAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
                alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + intervalInMillis, intervalInMillis, intervalPendingIntent);
            }
        }

    }



    private static boolean isValidInterval(long lastSync,int postingInterval) {
        int diff = getTimeDiffInMinutes(lastSync);

        if (diff < 0) {
            return true;
        } else if (diff > postingInterval) {
            return true;
        } else {
            return false;
        }
    }

    public static synchronized int getTimeDiffInMinutes(long lastposting) {
        long currTime = new Date().getTime();
        long diff = currTime - lastposting;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        if (diffHours >= 1) {
            diffMinutes = (diffHours * 60) + diffMinutes;
        }
        return (int) diffMinutes;

    }

}
