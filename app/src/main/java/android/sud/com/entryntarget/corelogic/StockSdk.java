package android.sud.com.entryntarget.corelogic;

import android.content.Context;
import android.sud.com.entryntarget.corelogic.listener.ResponseBody;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.services.models.ScriptModel;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.ScriptLoader;
import android.sud.com.entryntarget.upstockService.common.constants.OrderStatus;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.feed.FeedService;
import android.sud.com.entryntarget.upstockService.feed.models.Feed;
import android.sud.com.entryntarget.upstockService.feed.models.SubscriptionResponse;
import android.sud.com.entryntarget.upstockService.orders.OrderService;
import android.sud.com.entryntarget.upstockService.orders.models.Order;
import android.sud.com.entryntarget.upstockService.orders.models.OrderRequest;
import android.sud.com.entryntarget.upstockService.users.UserService;
import android.sud.com.entryntarget.upstockService.users.models.Position;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.utils.Utility;
import android.text.TextUtils;
import android.util.Log;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.sud.com.entryntarget.utility.EntConstants.CONST_SELL;

public class StockSdk {
    private static final StockSdk ourInstance = new StockSdk();

    public static StockSdk getInstance() {
        return ourInstance;
    }

    private StockSdk() {
    }

    /*private synchronized void retriveBankNifty(final Context context) {
        //Creating an object of our api interface
        ApiService api = RetroClient.getApiService();
        *//**
         * Calling JSON
         *//*
        Call<StockModel> call = api.getBankNifty();
        *//**
         * Enqueue Callback will be call when get response...
         *//*
        call.enqueue(new Callback<StockModel>() {
            @Override
            public void onResponse(Call<StockModel> call, Response<StockModel> response) {
                if (response.isSuccessful()) {
                    *//**
                     * Got Successfully
                     *//*
                    StockModel stockList = response.body();
                    DBAdapter.getDBAdapter(context).updateIndexData(stockList);

                } else {
                    *//*if(listener!=null)
                        listener.onResponse(null);*//*
                }
            }

            @Override
            public void onFailure(Call<StockModel> call, Throwable t) {

                *//*if(listener!=null)
                    listener.onFailure(t);*//*
            }

        });
    }*/

    /*private synchronized void retriveNifty(final Context context) {
        //Creating an object of our api interface
        ApiService api = RetroClient.getApiService();
        *//**
         * Calling JSON
         *//*
        Call<StockModel> call = api.getNifty();
        *//**
         * Enqueue Callback will be call when get response...
         *//*
        call.enqueue(new Callback<StockModel>() {
            @Override
            public void onResponse(Call<StockModel> call, Response<StockModel> response) {
                if (response.isSuccessful()) {
                    *//**
                     * Got Successfully
                     *//*
                    StockModel stockList = response.body();
                    DBAdapter.getDBAdapter(context).updateIndexData(stockList);

                } else {
                    *//*if(listener!=null)
                        listener.onResponse(null);*//*
                }
            }

            @Override
            public void onFailure(Call<StockModel> call, Throwable t) {

                *//*if(listener!=null)
                    listener.onFailure(t);*//*
            }

        });
    }*/

//    private synchronized void retriveFNO(final Context context, final ResponseBody listener) {
//        //Creating an object of our api interface
//        ApiService api = RetroClient.getApiService();
//        /**
//         * Calling JSON
//         */
//        Call<StockModel> call = api.getFNO();
//        /**
//         * Enqueue Callback will be call when get response...
//         */
//        call.enqueue(new Callback<StockModel>() {
//            @Override
//            public void onResponse(Call<StockModel> call, Response<StockModel> response) {
//                if (response.isSuccessful()) {
//                    /**
//                     * Got Successfully
//                     */
//                    StockModel stockList = response.body();
//                    String dateFormatNse = Utility.getInstance().getDayNseFormat();
//                    if (stockList.getTime().contains(dateFormatNse)) {
//                        DBAdapter.getDBAdapter(context).updateStockIndex(stockList);
//                        if (listener != null)
//                            listener.onResponse(stockList);
//                    }
//                } else {
//                    if (listener != null)
//                        listener.onResponse(null);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<StockModel> call, Throwable t) {
//                if (listener != null)
//                    listener.onFailure(t);
//            }
//        });
//    }


    public void retriveStockInfo(final Context context, final ResponseBody listener) {
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            Log.d("upsto","blank token found");
            return;
        }

        if (EntPreferences.isRegistered(context)) {
            Date rightNow = new Date();
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
            String dateName = simpleDateformat.format(rightNow);
            if (dateName.equalsIgnoreCase("Sun") || dateName.equalsIgnoreCase("Sat")) {
                return;
            }
            String[] list = DBAdapter.getDBAdapter(context).getFavList();
            if (list != null && list.length > 0) {
                for (int index = 0; index < list.length; index++) {
                    ScriptModel model = new ScriptModel(list[index]);
                    //getLivePrice(context, model);
                    new Thread(new ScriptLoader(context,model.getSymbol())).start();
                    //loadLiveFeed(context, model.getSymbol());
                }
            }

            //retriveBankNifty(context);
            //retriveNifty(context);
            //retriveFNO(context, listener);
        }
    }


    public void loadSubscribedLiveFeed(final Context context) {
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        FeedService feedService = new FeedService(accessToken, apiCredentials);
        Call<UpstoxResponse<SubscriptionResponse>> myfed = feedService.subscribe("ltp","NSE_EQ","infy,lt,reliance,sbin,yesbank");
        myfed.enqueue(new Callback<UpstoxResponse<SubscriptionResponse>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<SubscriptionResponse>> call, Response<UpstoxResponse<SubscriptionResponse>> response) {
                if (response.isSuccessful()) {
                    UpstoxResponse<SubscriptionResponse> currentFeed = response.body();
                    SubscriptionResponse fullFeed = currentFeed.getData();

                    Log.d("Upsto", "Live Price for " );
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<SubscriptionResponse>> call, Throwable t) {
                Log.d("Err", "asdnoasdj");
            }
        });
    }

    public synchronized void loadLiveFeed(final Context context, String symbol) {
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        FeedService feedService = new FeedService(accessToken, apiCredentials);
        Call<UpstoxResponse<Feed>> myfed = feedService.liveFeed("NSE_EQ", symbol, "LTP");
        myfed.enqueue(new Callback<UpstoxResponse<Feed>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Feed>> call, Response<UpstoxResponse<Feed>> response) {
                if (response.isSuccessful()) {
                    UpstoxResponse<Feed> currentFeed = response.body();
                    Feed fullFeed = currentFeed.getData();
                    BigDecimal ltp = fullFeed.getLtp();//
                    String symbol = fullFeed.getSymbol();
                    BigDecimal open = fullFeed.getOpen();
                    DBAdapter.getDBAdapter(context).updateLiveLprice(String.valueOf(ltp), symbol, String.valueOf(open));
                    Log.d("Upsto", "Live Price for " + symbol + "  " + ltp);
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<Feed>> call, Throwable t) {
                Log.d("Err", "asdnoasdj");
            }
        });
    }

    private double getPriceFromTrigger(double triggerPrice,String signal){
        //when BUY -> price is always grather than trigger price
        //=ROUND((100.3*L9)/100,1)
        //=ROUND((99.7*L9)/100,1)
        int interval = 1;
        if(triggerPrice > 1000 && triggerPrice < 3000){
            interval = 3;
        }else if(triggerPrice > 3000 && triggerPrice < 5000){
            interval = 5;
        }else if(triggerPrice > 5000 && triggerPrice < 10000){
            interval = 10;
        }else if(triggerPrice > 10000 ){
            interval = 15;
        }
        if(signal.equalsIgnoreCase("b")){
            return  Utility.getInstance().convertTriggerPrice((triggerPrice + interval));
            //return Utility.getInstance().convertTriggerPrice(Math.round((100.3 * triggerPrice)/100));
        }else{
            return  Utility.getInstance().convertTriggerPrice((triggerPrice - interval));
            //return Utility.getInstance().convertTriggerPrice(Math.round((99.7 * triggerPrice)/100));
        }
    }

    public void placeOrder(Context context,String symbol,String signal,double t1Level,double slLevel,double triggerPrice,int quantity){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        if(symbol.contains("NIFTY")){
            return;
        }
        if(EntPreferences.getUpstoxKeyOrderType(context).equalsIgnoreCase("oco")){
            if(t1Level>0 && slLevel>0) {
                placeFreshOrderOCO(context, symbol, signal, slLevel, t1Level, triggerPrice, quantity);
            }
        }else{
            placeFreshOrder(context,symbol,signal,triggerPrice,quantity);
        }
    }

    public void placeModifyOrder(final Context context,String symbol,String signal,double triggerPrice,int quantity){
        /*if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }*/
        /*if(symbol.contains("NIFTY")){
            return;
        }*/
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setSymbol(symbol);
        orderRequest.setExchange("NSE_EQ");
        orderRequest.setOrderType("SL-M");
        orderRequest.setProduct("I");
        orderRequest.setTransactionType(signal);
        orderRequest.setQuantity(new Long(quantity));
        orderRequest.setTriggerPrice(new BigDecimal(triggerPrice));
        //orderRequest.setPrice(new BigDecimal(getPriceFromTrigger(triggerPrice,signal)));

        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Log.d("Upstox","Place Modify calling "+symbol);
        Call<UpstoxResponse<Order>> call = orderService.placeOrder(orderRequest);
        call.enqueue(new Callback<UpstoxResponse<Order>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Order>> call, Response<UpstoxResponse<Order>> response) {
                if(response.isSuccessful()){
                    UpstoxResponse<Order> orderUpdate = response.body();
                    if(orderUpdate!=null) {
                        Order orderInfo = orderUpdate.getData();
                        DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(),orderInfo.getOrderId(),orderInfo.getStatus());
                        Log.d("Upstox","Place Modify Order Normal succesfully "+orderInfo.getSymbol()+" ordid "+orderInfo.getOrderId()+" "+orderInfo.getStatus());
                        DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfoModify(orderInfo.getSymbol(), orderInfo.getOrderId());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<Order>> call, Throwable t) {
                //Toast.makeText(context,"Error in Order placed",Toast.LENGTH_LONG).show();
                Log.d("Upstox","Error getting in Place Order ");
                Log.d("Upstox",""+t.getLocalizedMessage());
            }
        });
    }


    public void modifyOrder(final Context context,final String orderId,double triggerPrice){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderId(orderId);
        orderRequest.setTriggerPrice(new BigDecimal(triggerPrice));

        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<Order>> call = orderService.modifyOrder(orderId,orderRequest);
        call.enqueue(new Callback<UpstoxResponse<Order>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Order>> call, Response<UpstoxResponse<Order>> response) {
                if(response.isSuccessful()){
                    UpstoxResponse<Order> orderUpdate = response.body();
                    if(orderUpdate!=null) {
                        Order orderInfo = orderUpdate.getData();
                        DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(),orderInfo.getOrderId(),orderInfo.getStatus());
                        Log.d("Upstox","Order Modified "+orderInfo.getSymbol()+" ordid "+orderInfo.getOrderId()+" "+orderInfo.getStatus());
                        DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfoModify(orderInfo.getSymbol(), orderInfo.getOrderId());
                        DBAdapter.getDBAdapter(context).insertActivityLogs(orderInfo.getSymbol(),orderInfo.getTransactionType(),"Modified status "+orderInfo.getOrderId()+" "+orderInfo.getStatus());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<Order>> call, Throwable t) {
                //Toast.makeText(context,"Error in Order placed",Toast.LENGTH_LONG).show();
                Log.d("Upstox","Error getting in modify Order ");
                Log.d("Upstox"," "+t.getLocalizedMessage());
            }
        });
    }

    private void placeFreshOrder(final Context context,String symbol,String signal,double triggerPrice,int quantity){

        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        if(symbol.contains("NIFTY")){
            return;
        }
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setSymbol(symbol);
        orderRequest.setExchange("NSE_EQ");
        orderRequest.setOrderType("SL");
        orderRequest.setProduct("I");
        orderRequest.setTransactionType(signal);
        orderRequest.setQuantity(new Long(quantity));
        orderRequest.setTriggerPrice(new BigDecimal(triggerPrice));
        orderRequest.setPrice(new BigDecimal(getPriceFromTrigger(triggerPrice,signal)));

        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<Order>> call = orderService.placeOrder(orderRequest);
        call.enqueue(new Callback<UpstoxResponse<Order>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Order>> call, Response<UpstoxResponse<Order>> response) {
                if(response.isSuccessful()){
                    UpstoxResponse<Order> orderUpdate = response.body();
                    if(orderUpdate!=null) {
                        Order orderInfo = orderUpdate.getData();
                        DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(),orderInfo.getOrderId(),orderInfo.getStatus());
                        Log.d("Upstox","Place Order Normal succesfully "+orderInfo.getOrderId()+" "+orderInfo.getStatus());
                        //if(orderInfo.getStatus().equalsIgnoreCase(OrderStatus.COMPLETE)) {
                            DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfo(orderInfo.getSymbol(), orderInfo.getOrderId());
                        //}
                    }
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<Order>> call, Throwable t) {
                //Toast.makeText(context,"Error in Order placed",Toast.LENGTH_LONG).show();
                Log.d("Upstox","Error getting in placeFreshOrder "+symbol);
                Log.d("Upstox",t.getLocalizedMessage());
            }
        });
    }

    private void placeFreshOrderOCO(final Context context,String symbol,String signal,double slLevel,double t1Level,double triggerPrice,int quantity){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        if(symbol.contains("NIFTY")){
            return;
        }
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setSymbol(symbol);
        orderRequest.setExchange("NSE_EQ");
        orderRequest.setOrderType("SL");
        orderRequest.setProduct("OCO");
        orderRequest.setTransactionType(signal);
        orderRequest.setQuantity(new Long(quantity));

        double trigger = Utility.getInstance().convertTriggerPrice(triggerPrice);
        double price = Utility.getInstance().convertTriggerPrice(getPriceFromTrigger(triggerPrice,signal));
        double target1 = Utility.getInstance().convertTriggerPrice(t1Level);
        double stopLossLevel = Utility.getInstance().convertTriggerPrice(slLevel);
        double squreOff = Utility.getInstance().convertTriggerPrice(target1 - triggerPrice);
        double sl = Utility.getInstance().convertTriggerPrice(triggerPrice - stopLossLevel);
        if(signal.equalsIgnoreCase(CONST_SELL)) {
            squreOff = Utility.getInstance().convertTriggerPrice(triggerPrice - target1);
            sl = Utility.getInstance().convertTriggerPrice(stopLossLevel - triggerPrice);
        }
        orderRequest.setPrice(new BigDecimal(price));
        orderRequest.setTriggerPrice(new BigDecimal(trigger));
        orderRequest.setSquareoff(new BigDecimal(squreOff));
        orderRequest.setStoploss(new BigDecimal(sl));

        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<Order>> call = orderService.placeOrder(orderRequest);
        call.enqueue(new Callback<UpstoxResponse<Order>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Order>> call, Response<UpstoxResponse<Order>> response) {
                if(response.isSuccessful()){
                    UpstoxResponse<Order> orderUpdate = response.body();
                    if(orderUpdate!=null) {
                        Order orderInfo = orderUpdate.getData();
                        DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(),orderInfo.getOrderId(),orderInfo.getStatus());
                        Log.d("Upstox","Place Order OCO succesfully "+orderInfo.getOrderId()+" "+orderInfo.getStatus());
                        if(orderInfo.getStatus().equalsIgnoreCase(OrderStatus.COMPLETE)) {
                            DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfo(orderInfo.getSymbol(), orderInfo.getOrderId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<Order>> call, Throwable t) {
                //Toast.makeText(context,"Error in Order placed",Toast.LENGTH_LONG).show();
                Log.d("Upstox","Error getting in place order ");
            }
        });
    }

    public void cancelSingeOrder(final Context context,String orderId){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        if(TextUtils.isEmpty(orderId)){
            return;
        }
        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<String>> call = orderService.cancelOrders(orderId);
        call.enqueue(new Callback<UpstoxResponse<String>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<String>> call, Response<UpstoxResponse<String>> response) {
                if(response.isSuccessful()) {
                    Log.d("Upstox","Order Canceled "+response.message());
                    /*DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(), orderInfo.getOrderId(), orderInfo.getStatus());
                    if (orderInfo.getStatus().equalsIgnoreCase(OrderStatus.COMPLETE)) {
                        DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfo(orderInfo.getSymbol(), orderInfo.getOrderId());
                    }*/
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<String>> call, Throwable t) {
                Log.d("Upstox","Error in cancel order");
            }
        });
    }

    public void cancelAllOrder(Context context){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<String>> call = orderService.cancelAllOrders();
        call.enqueue(new Callback<UpstoxResponse<String>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<String>> call, Response<UpstoxResponse<String>> response) {
                if(response.isSuccessful()) {
                    /*DBAdapter.getDBAdapter(context).updateOrderId(orderInfo.getSymbol(), orderInfo.getOrderId(), orderInfo.getStatus());
                    if (orderInfo.getStatus().equalsIgnoreCase(OrderStatus.COMPLETE)) {
                        DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfo(orderInfo.getSymbol(), orderInfo.getOrderId());
                    }*/
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<String>> call, Throwable t) {

            }
        });
    }

//    public void getPosition(final Context context){
//        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
//                EntPreferences.getUpstoxKeySecret(context));
//        AccessToken accessToken = new AccessToken();
//        accessToken.setToken(EntPreferences.getUpstoxToken(context));
//        accessToken.setType("Bearer");
//        accessToken.setExpiresIn(86400000l);
//        UserService userService = new UserService(accessToken,apiCredentials);
//        Call<UpstoxResponse<List<Position>>> call = userService.getPositions();
//        call.enqueue(new Callback<UpstoxResponse<List<Position>>>() {
//            @Override
//            public void onResponse(Call<UpstoxResponse<List<Position>>> call, Response<UpstoxResponse<List<Position>>> response) {
//                if(response.isSuccessful()){
//                    UpstoxResponse<List<Position>> body = response.body();
//                    List<Position> positionList = body.getData();
//                    for (int index=0;index<positionList.size();index++){
//                        //positionList.get(index).get
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<UpstoxResponse<List<Position>>> call, Throwable t) {
//                Log.d("asdas","asd");
//            }
//        });
//
//    }

    public void getOrderHistory(final Context context){
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(context))){
            return;
        }
        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(context),
                EntPreferences.getUpstoxKeySecret(context));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(context));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        OrderService orderService = new OrderService(accessToken,apiCredentials);
        Call<UpstoxResponse<List<Order>>> call = orderService.getOrderHistory();
        call.enqueue(new Callback<UpstoxResponse<List<Order>>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<List<Order>>> call, Response<UpstoxResponse<List<Order>>> response) {
                if(response.isSuccessful()){
                    List<Order> orderList = response.body().getData();
                    for (int index = 0; index < orderList.size(); index++) {
                        String symbol = orderList.get(index).getSymbol();
                        String order_type = orderList.get(index).getOrderType();
                        String transactionType = orderList.get(index).getTransactionType();
                        String exchange = orderList.get(index).getExchange();
                        String orderId = orderList.get(index).getOrderId();
                        BigDecimal price = orderList.get(index).getPrice();
                        long quantity = orderList.get(index).getQuantity();
                        String status = orderList.get(index).getStatus();
                        String message = orderList.get(index).getMessage();
                        if (TextUtils.isEmpty(message)) {
                            DBAdapter.getDBAdapter(context).updateOrder(orderId, symbol, order_type, transactionType, exchange, price, quantity, status);
                        }
                        if(status.equalsIgnoreCase(OrderStatus.COMPLETE)) {
                            DBAdapter.getDBAdapter(context).updateOrderIdInTriggerInfo(symbol, orderId);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UpstoxResponse<List<Order>>> call, Throwable t) {

            }
        });
    }
}
