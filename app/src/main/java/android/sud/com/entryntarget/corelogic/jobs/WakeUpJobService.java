package android.sud.com.entryntarget.corelogic.jobs;

import android.os.AsyncTask;
import android.sud.com.entryntarget.corelogic.StockSdk;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.utils.Utility;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import java.util.Calendar;



/**
 * Created by prasma04 on 13-05-2018.
 */


public class WakeUpJobService extends JobService {

    private static AsyncTask mBackgroundTask;
    private final static String TAG = "UpstoxWakeup";

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {

        mBackgroundTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                Calendar rightNow = Calendar.getInstance();
                //StockerPermissionManager.getInstance().showUserAction(context,"Nifty","Buy at 10450");
                Log.d(TAG,"onStartJob");
                int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
                int currentmin = rightNow.get(Calendar.MINUTE);

                Log.d(TAG, Utility.getInstance().getDateTime());

                if(currentHour >= 9 && currentHour < 16) {
                    if(currentHour ==9 && currentmin <14){
                        DBAdapter.getDBAdapter(WakeUpJobService.this).clearOldData();

                    }else if(currentHour==15 && currentmin>20){
                        DBAdapter.getDBAdapter(WakeUpJobService.this).closeAllOrders();
                        //StockSdk.getInstance().retriveStockInfo(WakeUpJobService.this, null);
                    }else {
                        StockSdk.getInstance().retriveStockInfo(WakeUpJobService.this, null);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                jobFinished(jobParameters, false);
            }
        };

        mBackgroundTask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        if (mBackgroundTask != null)
            mBackgroundTask.cancel(true);
        return true;
    }
}
