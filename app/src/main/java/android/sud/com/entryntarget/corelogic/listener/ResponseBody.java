package android.sud.com.entryntarget.corelogic.listener;


import android.sud.com.entryntarget.models.StockModel;

public interface ResponseBody {

    public void onResponse(StockModel response);

    public void onFailure(Throwable t);
}
