package android.sud.com.entryntarget.corelogic.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.sud.com.entryntarget.MainActivity;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.database.DBAdapter;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Arrays;


/**
 * Created by patisu06 on 03-04-2018.
 */

public class StockerPermissionManager {
    private static final StockerPermissionManager ourInstance = new StockerPermissionManager();
    private static final String CHANNEL_ID = "Stocker";
    private static final int NOTIFICATION_ID = 1000;

    public static StockerPermissionManager getInstance() {
        return ourInstance;
    }

    private StockerPermissionManager() {

    }




    public void showUserAction(Context context,String title,String message,int uniqueNumber) {
        Log.d("SDKAction", "showing notification");
        String[] list = DBAdapter.getDBAdapter(context).getFavList();
        if(Arrays.asList(list).contains(title)) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int notificationId = uniqueNumber;
                String channelId = "Stocker";
                String channelName = "Stocker";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(
                            channelId, channelName, importance);
                    notificationManager.createNotificationChannel(mChannel);
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message);

                mBuilder.setContentIntent(resultPendingIntent);
                mBuilder.setAutoCancel(true);
                notificationManager.notify(notificationId, mBuilder.build());
            } else {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        // Set Ticker Message
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message)
                        // Dismiss Notification
                        .setAutoCancel(true)
                        // Set PendingIntent into Notification
                        .setContentIntent(resultPendingIntent);

                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }
        }
    }
}
