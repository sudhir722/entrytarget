package android.sud.com.entryntarget.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.sud.com.entryntarget.corelogic.OrderInfo;
import android.sud.com.entryntarget.corelogic.StockSdk;
import android.sud.com.entryntarget.corelogic.notifications.StockerPermissionManager;
import android.sud.com.entryntarget.models.NiftyIndexModel;
import android.sud.com.entryntarget.models.StockInfo;
import android.sud.com.entryntarget.models.StockModel;
import android.sud.com.entryntarget.models.Stocks;
import android.sud.com.entryntarget.models.TriggerInfo;
import android.sud.com.entryntarget.upstockService.common.constants.OrderStatus;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.utils.Utility;
import android.text.TextUtils;
import android.util.Log;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.sud.com.entryntarget.database.DatabaseConstants.CREATED_TIME;
import static android.sud.com.entryntarget.database.DatabaseConstants.IS_ACTIVE;
import static android.sud.com.entryntarget.database.DatabaseConstants.KEY_CHANGE;
import static android.sud.com.entryntarget.database.DatabaseConstants.KEY_ORDER_ID;
import static android.sud.com.entryntarget.database.DatabaseConstants.UPDATED_TIME;
import static android.sud.com.entryntarget.utility.EntConstants.ACTION_TRIGGER_BUY;
import static android.sud.com.entryntarget.utility.EntConstants.ACTION_TRIGGER_NONE;
import static android.sud.com.entryntarget.utility.EntConstants.ACTION_TRIGGER_SELL;
import static android.sud.com.entryntarget.utility.EntConstants.CONST_BUY;
import static android.sud.com.entryntarget.utility.EntConstants.CONST_SELL;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T1;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T2;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T3;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T4;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T5;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T6;

/*
 * Its a singletone database adaptor class.  how to use it.
 *
 * From any of your activity create object of this class and use that object to get and insert data
 *
 *  Example : DBAdapter database = DBAdapter.getDBAdapter(this);
 *
 * @auther Sudhir Patil
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class DBAdapter implements Serializable {

    public static final String DATABASENAME = "trd.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAGS = "UpstoxBridge";
    private static final String TAG_PLACEORD = "UpstoxPlaceORD";
    private static final String TAG_MODI_ORD = "UpstoxModiORD";
    private static SQLiteDatabase db = null;
    private static DBAdapter mInstance;
    private static Context mContext;
    String TAG = "DBAdapter";
    private DatabaseHelper dbHelper = null;

    public DBAdapter() {
    }

    public DBAdapter(Context ctx) {
        dbHelper = new DatabaseHelper(ctx);
        open();
    }

    public static DBAdapter getDBAdapter(Context ctx) {
        if (mInstance == null) {
            mInstance = new DBAdapter(ctx);
        }
        mContext = ctx;
        return mInstance;
    }

    private DBAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        if (db != null && db.isOpen()) {
            dbHelper.close();
            db = null;
        }
    }

    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public synchronized long insertData(String tableName, ContentValues values) {
        try {
            return db.insert(tableName, null, values);
        } catch (Exception e) {
            return -1;
        }
    }

    public synchronized boolean updateData(String tableName, ContentValues values, String where) {
        try {
            return db.update(tableName, values, where, null) > 0 ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized long reInsertData(String tableName, ContentValues values) {
        isDelete(tableName, null);
        return insertData(tableName, values);
    }

    public synchronized boolean resetTable(String tableName) {
        try {
            db.delete(tableName, null, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public synchronized int isDelete(String tableName, String condition) {
        int affectedRow = -1;
        try {
            affectedRow = db.delete(tableName, condition, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return affectedRow;
    }

    public void insertFavSymbol(String symbol) {
        boolean isAdded = DBAdapter.getDBAdapter(mContext).isAddedInFav(symbol);
        if (!isAdded) {
            ContentValues values = new ContentValues();
            values.put(DatabaseConstants.KEY_SCRIP, symbol);
            values.put(DatabaseConstants.CREATED_TIME, System.currentTimeMillis());
            if (!DBAdapter.getDBAdapter(mContext).updateData(DatabaseConstants.TABLE_FAV_SYMBOLS, values, DatabaseConstants.KEY_SCRIP + "='" + symbol + "'")) {
                DBAdapter.getDBAdapter(mContext).insertData(DatabaseConstants.TABLE_FAV_SYMBOLS, values);
            }
        }
    }

    public synchronized int getCursorCount(String tableName, String whereCondition) {
        Cursor cursor = null;
        int rowCount = 0;
        try {
            cursor = db.query(tableName, new String[]{}, whereCondition,
                    null, whereCondition, null, null);
            rowCount = cursor.getCount();
        } catch (Exception e) {
            Log.e(TAG, "DBAdapter : getCursorCount" + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return rowCount;
    }

    public synchronized boolean isDataExists(String tableName, String uniqueColoumName, String WHERE) {
        Cursor cursor = null;
        boolean isDataExists = false;
        try {
            cursor = db.query(tableName, new String[]{}, WHERE,
                    null, null, null, " " + uniqueColoumName + " desc");
            if (cursor.getCount() > 0) {
                isDataExists = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in isDataExists() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return isDataExists;
    }


    public int clearData(String tablename) {
        return isDelete(tablename, null);
    }

    public synchronized void updateIndexData(StockModel stockList) {
        String dateFormatNse = Utility.getInstance().getDayNseFormat();
        if (stockList.getTime().contains(dateFormatNse)) {
            StockInfo stockLevels = getStockLevels(stockList.getNiftyIndexModelArrayList().get(0).getIndexName());
            if (stockLevels == null) {
                dumpLiveData(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)));
                stockLevels = getStockLevels(stockList.getNiftyIndexModelArrayList().get(0).getIndexName());
                if (stockLevels != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseConstants.KEY_LTP, stockList.getNiftyIndexModelArrayList().get(0).getCurrentPrice());
                    String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
                    updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
                    checkTriggerInfo(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)), stockLevels);
                }
            } else {
                checkTriggerInfo(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)), stockLevels);
            }
        }
    }

    public synchronized void extractStockData(Stocks stocks, String date) {
        StockInfo stockLevels = getStockLevels(stocks.getSymbol());
        if (stockLevels == null) {
            dumpLiveData(stocks);
            stockLevels = getStockLevels(stocks.getSymbol());
            if (stockLevels != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.KEY_LTP, stocks.getOpen());
                String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
                updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
                checkTriggerInfo(stocks, stockLevels);
            }
            checkTriggerInfo(stocks, stockLevels);
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.KEY_LTP, stocks.getOpen());
            String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
            checkTriggerInfo(stocks, stockLevels);
        }
    }


    public synchronized void updateLiveLprice(String ltp, String symbol, String open) {
        Stocks stocks = new Stocks();
        stocks.setLtP(ltp);
        stocks.setLow("0");
        stocks.setHigh("0");
        stocks.setOpen(open);
        stocks.setSymbol(symbol);
        StockInfo stockLevels = getStockLevels(stocks.getSymbol());
        if (stockLevels == null) {
            Log.d("UpstoxDump", symbol + " not in live data");
            dumpLiveData(stocks);
            stockLevels = getStockLevels(stocks.getSymbol());
            if (stockLevels != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.KEY_LTP, stocks.getOpen());
                String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
                updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
                checkTriggerInfo(stocks, stockLevels);
            }
            checkTriggerInfo(stocks, stockLevels);
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.KEY_LTP, stocks.getOpen());
            String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
            checkTriggerInfo(stocks, stockLevels);
        }
    }

    private boolean isFilteredSymbol(String symbol) {
        /*for (int index=0;index<DatabaseConstants.filteredSymbols.length;index++){
            if(symbol.equalsIgnoreCase(DatabaseConstants.filteredSymbols[index])){
                return true;
            }
        }*/
        return true;
    }

    public synchronized void updateStockIndex(StockModel stockList) {
        String currentDay = Utility.getInstance().formatDate();
        if (EntPreferences.getCurrentDate(mContext).equalsIgnoreCase(currentDay)) {
            String sd = Utility.getInstance().getDayNseFormat();
            if (stockList.getTime().contains(sd)) {
                EntPreferences.setCurrentDate(mContext, currentDay);
                ArrayList<Stocks> sList = stockList.getStocklist();
                for (int index = 0; index < sList.size(); index++) {
                    if (isFilteredSymbol(sList.get(index).getSymbol())) {
                        StockInfo stockLevels = getStockLevels(sList.get(index).getSymbol());
                        if (stockLevels == null) {
                            dumpLiveData(sList.get(index));
                        } else {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stockList.getNiftyIndexModelArrayList().get(0).getCurrentPrice()));
                            String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
                            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
                            checkTriggerInfo(sList.get(index), stockLevels);
                        }
                    }
                }
            }
        } else {
            ArrayList<Stocks> sList = stockList.getStocklist();
            for (int index = 0; index < sList.size(); index++) {
                if (isFilteredSymbol(sList.get(index).getSymbol())) {
                    StockInfo stockLevels = getStockLevels(sList.get(index).getSymbol());
                    if (stockLevels == null) {
                        dumpLiveData(sList.get(index));
                    }
                    if (stockLevels == null) {
                        stockLevels = getStockLevels(sList.get(index).getSymbol());
                    }
                    if (stockLevels != null) {
                        checkTriggerInfo(sList.get(index), stockLevels);
                    }
                }
            }
        }

    }

    private Stocks convertIndex(NiftyIndexModel model) {
        Stocks stocks = new Stocks();
        stocks.setSymbol(model.getIndexName());
        stocks.setLow(model.getLow());
        stocks.setLtP(model.getCurrentPrice());
        stocks.setOpen(model.getOpen());
        stocks.setPreviousClose("0");
        return stocks;
    }

    private synchronized void closeorder(String symbol, String signal, double ltp, double recPrice) {
        ContentValues values = new ContentValues();
        values.put(UPDATED_TIME, System.currentTimeMillis());
        values.put(DatabaseConstants.IS_ACTIVE, 2);
        values.put(DatabaseConstants.KEY_EXIS_PRICE, ltp);
        if (signal.equalsIgnoreCase(CONST_BUY)) {
            values.put(DatabaseConstants.KEY_CHANGE, ltp - recPrice);
        } else {
            values.put(DatabaseConstants.KEY_CHANGE, recPrice - ltp);
        }
        String whereCondi = DatabaseConstants.KEY_SCRIP + "='" + symbol + "' and " + IS_ACTIVE + "<>'0'";
        updateStockTriggerInfo(values, whereCondi);
    }

    public synchronized void closeAllOrders() {

        List<TriggerInfo> triggerInfoList = getIncompletedTriggers();
        if (triggerInfoList.size() > 0) {
            for (int index = 0; index < triggerInfoList.size(); index++) {
                ContentValues values = new ContentValues();
                values.put(UPDATED_TIME, Utility.getInstance().getDateTime());
                values.put(DatabaseConstants.IS_ACTIVE, 1);
                values.put(DatabaseConstants.KEY_STATUS, 0);
                values.put(DatabaseConstants.KEY_EXIS_PRICE, triggerInfoList.get(index).getLtp());
                if (triggerInfoList.get(index).getSignal().equalsIgnoreCase(CONST_BUY)) {
                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfoList.get(index).getLtp() - triggerInfoList.get(index).getRecPrice());
                } else {
                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfoList.get(index).getRecPrice() - triggerInfoList.get(index).getLtp());
                }

                String whereCondi = DatabaseConstants.ID + "='" + triggerInfoList.get(index).getId() + "'";
                updateStockTriggerInfo(values, whereCondi);
            }
        }
    }

    protected boolean isSessionClosed(String signal,StockInfo level){
        if (signal.equalsIgnoreCase(CONST_BUY)) {
            //close session when ltp cross below open price
            if(level.getLtp() < level.getOpen()){
                return true;
            }
        }else if (signal.equalsIgnoreCase(CONST_SELL)) {
            //close session when ltp cross above open price
            if(level.getLtp() > level.getOpen()){
                return true;
            }
        }
        return false;
    }

    public synchronized void checkTriggerInfo(Stocks stocks, StockInfo stockLevels) {

        /*//check todays levels
        StockInfo stockLevels = getStockLevels(stocks.getSymbol());*/
        if (stockLevels != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stocks.getLtP()));
            String where = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, where);
            TriggerInfo triggerInfo = getTriggerInfo(stocks.getSymbol());
            //check trigger is already captured ot not
            ContentValues values = new ContentValues();
            OrderInfo orderInfo = orderInfo = null;
            if(triggerInfo!=null) {
                orderInfo = orderInfo = getOpenOrderDetails(triggerInfo.getOrderId());
            }
            if(triggerInfo!=null && TextUtils.isEmpty(triggerInfo.getModOrderId())){
                if(orderInfo!=null && orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE)) {
                    if (triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY)) {
                        values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                        setStopLoss(triggerInfo.getOrderId(), stocks.getSymbol(), CONST_SELL, stockLevels.getBuySl());
                        Log.d(TAG_MODI_ORD, "Inilise STOP LOSS SET for " + stocks.getSymbol() + " BUY-SL @ " + stockLevels.getBuySl());
                        insertActivityLogs(stocks.getSymbol(), "s", "SET STOP LOSS BUY-SL @ " + stockLevels.getBuySl());
                    } else if (triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL)) {
                        values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                        setStopLoss(triggerInfo.getOrderId(), stocks.getSymbol(), CONST_BUY, stockLevels.getSellSl());
                        Log.d(TAG_MODI_ORD, "Inilise STOP LOSS SET for " + stocks.getSymbol() + " SELL-SL @ " + stockLevels.getSellSl());
                        insertActivityLogs(stocks.getSymbol(), "b", "SET STOP LOSS BUY-SL @ " + stockLevels.getSellSl());
                    }
                }
            }
            if (triggerInfo != null && triggerInfo.getIsTslHit() == 0) {
                String whereCondi = DatabaseConstants.ID + "='" + triggerInfo.getId() + "'";
                double ltp = Utility.getInstance().convertDouble(stocks.getLtP());
                //values.put(DatabaseConstants.IS_ACTIVE, 1);
                values.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stocks.getLtP()));
                values.put(UPDATED_TIME, Utility.getInstance().getDateTime());

                if(triggerInfo!=null && isSessionClosed(triggerInfo.getSignal(),stockLevels)){
                    /*
                    * Close Session and break the code
                    * **/
                    //OrderInfo orderInfo = getOpenOrderDetails(triggerInfo.getOrderId());
                    if(orderInfo!=null && !orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE)){
                        if(EntPreferences.isPaid(mContext)) {
                            StockSdk.getInstance().cancelSingeOrder(mContext, orderInfo.getOrder_id());
                        }
                    }
                    values.put(DatabaseConstants.KEY_STATUS, 0);
                    values.put(DatabaseConstants.IS_ACTIVE, 1);
                    if(triggerInfo.getExitPrice()<=0){
                        values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getBuySl());
                    }
                    updateStockTriggerInfo(values, whereCondi);
                    insertActivityLogs(stocks.getSymbol(),"","Session Closed...");
                    return;
                }
                //bypass to wait for order executation...
                if (triggerInfo.getIsActive() == -1) {
                    //not executed trade
                    if (triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY)) {
                        if (ltp < triggerInfo.getRecPrice()) {
                            //Log.d(TAG_PLACEORD, "Not executed trigger " + stocks.getSymbol());
                            return;
                        }
                    } else if (triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL)) {
                        if (ltp > triggerInfo.getRecPrice()) {
                            //Log.d(TAG_PLACEORD, "Not executed trigger " + stocks.getSymbol());
                            return;
                        }
                    }
                }
                if (triggerInfo.getExitPrice() > 0) {
                    //  SL/TSL HIT but session still open
                    return;
                }
                if (triggerInfo.getLow() > Utility.getInstance().convertDouble(stocks.getLtP())) {
                    values.put(DatabaseConstants.KEY_LOW, stocks.getLtP());
                }
                if (triggerInfo.getHigh() < Utility.getInstance().convertDouble(stocks.getLtP())) {
                    values.put(DatabaseConstants.KEY_HIGH, stocks.getLtP());
                }

                ///BUY TRIGGER
                if (triggerInfo.getSignal() == null || triggerInfo.getIsActive()==1) {
                    Log.d("NulRef", triggerInfo.getSymbol());
                    return;
                } else if (triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    if (Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getBuySl()) {
                        if(triggerInfo.getIsTslHit()==0) {
                            values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellSl());
                            values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSellSl());
                            values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSellSl());
                        }
                        cancelPendingOrder(triggerInfo.getOrderId());
                        values.put(DatabaseConstants.KEY_STATUS,0);
                        insertActivityLogs(stocks.getSymbol(),"b","CLOSE SESSION" + " @" + stockLevels.getSellSl());
                    }
                    if (triggerInfo.getIsActive() == 2) {
                        ///TSL HIT Don't do anything here
                        if (ltp <= stockLevels.getBuySl()) {
                            if (triggerInfo.getIsTslHit() == 0) {
                                values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getBuySl());
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                                values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getBuySl());
                                //setStopLoss(triggerInfo.getOrderId(), stocks.getSymbol(), CONST_SELL, stockLevels.getBuySl());
                            }
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        }
                    } else {
                        //case handle for rare case
                        values.put(DatabaseConstants.IS_ACTIVE, 0);
                        if (ltp < stockLevels.getBuySl()) {
                            if(triggerInfo.getIsTslHit()==0) {
                                values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getBuySl());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuySl());
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                                values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getBuySl() - triggerInfo.getRecPrice());
                                cancelPendingOrder(triggerInfo.getOrderId());
                            }
                            values.put(DatabaseConstants.KEY_STATUS, 0);
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        } else {
                            if (triggerInfo.getSl() == 0) {
                                //STOP LOSS SET- close session for Upstox
                                if(!TextUtils.isEmpty(triggerInfo.getOrderId())) {

                                    //values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getBuySl() - triggerInfo.getRecPrice());
                                    //StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS HIT " + " @" + stockLevels.getBuySl(), stockLevels.getId());
                                    //values.put(DatabaseConstants.IS_ACTIVE, 4);
                                    values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                                    //setStopLoss(triggerInfo.getOrderId(), stocks.getSymbol(), CONST_SELL, stockLevels.getBuySl());
                                    //Log.d(TAG_MODI_ORD, "STOP LOSS SET for " + stocks.getSymbol() + " BUY-SL @ " + stockLevels.getBuySl());
                                    //insertActivityLogs(stocks.getSymbol(), "b", "SET STOP LOSS BUY-SL @ " + stockLevels.getBuySl());
                                }
                            }
                            if (triggerInfo.getT6() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget6()) {
                                //TARGET 6 hit-
                                if (triggerInfo.getT6() == 0) {
                                    //not exec
                                    values.put(DatabaseConstants.KEY_IS_T6, stockLevels.getTarget6());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-6 Achieved " + " @" + stockLevels.getTarget6(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget4());
                                    //values.put(DatabaseConstants.IS_ACTIVE, 1);
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), CONST_SELL, stockLevels.getTarget4());
                                    insertActivityLogs(stocks.getSymbol(),CONST_BUY,"T6 HIT " + stockLevels.getTarget6());
                                } else if (ltp < stockLevels.getTarget4()) {
                                    //HIT TSL T4
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget4());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget4());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getTarget4());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getTarget4() - triggerInfo.getRecPrice());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),CONST_BUY,"STOP LOSS (TSL) T4 HIT " + " @" + stockLevels.getTarget4());
                                }
                            }
                            if (triggerInfo.getT5() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget5()) {
                                if (triggerInfo.getT5() == 0) {
                                    //TARGET 5 hit-
                                    values.put(DatabaseConstants.KEY_IS_T5, stockLevels.getTarget5());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-5 Achieved " + " @" + stockLevels.getTarget5(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget3());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), CONST_SELL, stockLevels.getTarget3());
                                    insertActivityLogs(stocks.getSymbol(),"b","T5 HIT " + stockLevels.getTarget5());
                                } else if (ltp < stockLevels.getTarget3()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget3());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget3());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getTarget3());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getTarget3() - triggerInfo.getRecPrice());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget3(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"b","STOP LOSS (TSL) HIT T3" + " @" + stockLevels.getTarget3());
                                }
                            }
                            if (triggerInfo.getT4() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget4()) {
                                if (triggerInfo.getT4() == 0) {
                                    //TARGET 4 hit-
                                    values.put(DatabaseConstants.KEY_IS_T4, stockLevels.getTarget4());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget2());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), CONST_SELL, stockLevels.getTarget2());
                                    insertActivityLogs(stocks.getSymbol(),"b","T4 HIT " + stockLevels.getTarget4());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-4 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                } else if (ltp < stockLevels.getTarget2()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget2());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget2());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getTarget2());
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getTarget2() - triggerInfo.getRecPrice());
                                    insertActivityLogs(stocks.getSymbol(),"b","STOP LOSS (TSL) HIT T2" + " @" + stockLevels.getTarget2());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget2(), stockLevels.getId());
                                }
                            }
                            if (triggerInfo.getT3() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget3()) {
                                if (triggerInfo.getT3() == 0) {
                                    //TARGET 3 hit-
                                    values.put(DatabaseConstants.KEY_IS_T3, stockLevels.getTarget3());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-3 Achieved " + " @" + stockLevels.getTarget3(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget1());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "s", stockLevels.getTarget1());
                                    insertActivityLogs(stocks.getSymbol(),"b","T3 HIT " + " @" + stockLevels.getTarget3());
                                } else if (ltp < stockLevels.getTarget1()) {
                                    values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget1());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget1());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getTarget1());
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getTarget1() - triggerInfo.getRecPrice());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget1(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"b","STOP LOSS (TSL) HIT T1" + " @" + stockLevels.getTarget1());
                                }
                            }
                            if (triggerInfo.getT2() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget2()) {
                                if (triggerInfo.getT2() == 0) {
                                    //TARGET 2 hit-
                                    values.put(DatabaseConstants.KEY_IS_T2, stockLevels.getTarget2());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "s", stockLevels.getBuyAt());
                                    insertActivityLogs(stocks.getSymbol(),"b","T2 HIT " + " @" + stockLevels.getTarget2());
                                } else if (ltp < stockLevels.getBuyAt()) {
                                    values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuyAt());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getBuyAt());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, stockLevels.getBuyAt() - triggerInfo.getRecPrice());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getBuyAt(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"b","STOP LOSS (TSL) HIT OPEN_PRICE" + " @" + stockLevels.getBuyAt());
                                }
                            }
                            if (triggerInfo.getT1() == 0 && Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget1()) {
                                //TARGET 1 hit-
                                values.put(DatabaseConstants.KEY_IS_T1, stockLevels.getTarget1());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-1 Achieved " + " @" + stockLevels.getTarget1(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                                insertActivityLogs(stocks.getSymbol(),"b","T1 HIT " + " @" + stockLevels.getTarget1());
                                if(TextUtils.isEmpty(triggerInfo.getOrderId())){
                                    insertActivityLogs(stocks.getSymbol(),"b","Trigger canceled..." + stockLevels.getScriptName());
                                    canclePendingOrder(stockLevels.getScriptName());
                                }

                            }
                        }
                    }
                } else if (triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL)) {
                    if(triggerInfo.getExitPrice()>0){
                        updateStockTriggerInfo(values, whereCondi);
                        return;
                    }
                    if (triggerInfo.getIsActive() == 2) {
                        //TSL hit don't do anything here
                        /*if (ltp > stockLevels.getBuySl()) {
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        }*/
                    } else {
                        ///SELL ORDER
                        if (Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getSellSl()) {
                            values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellSl());
                            if(triggerInfo.getIsTslHit()==0) {
                                values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSellSl());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSellAt());
                                values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSellSl());
                            }
                            values.put(DatabaseConstants.KEY_STATUS,0);
                            insertActivityLogs(stocks.getSymbol(),"b","CLOSE SESSION" + " @" + stockLevels.getSellSl());
                            //values.put(DatabaseConstants.IS_ACTIVE, 1);
                            //StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS HIT " + " @" + stockLevels.getSellSl(), stockLevels.getId());
                            //insertActivityLogs(stocks.getSymbol(),"b","STOP LOSS HIT OPEN_PRICE" + " @" + stockLevels.getSellSl());
                        } else {
                            if (triggerInfo.getSl() == 0) {
                                //STOP LOSS HIT- close session
                                if(!TextUtils.isEmpty(triggerInfo.getOrderId())) {
                                    values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellSl());
                                    values.put(DatabaseConstants.IS_ACTIVE, 0);
                                    //values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSellSl());
                                    //setStopLoss(triggerInfo.getOrderId(), stocks.getSymbol(), "b", stockLevels.getBuySl());

                                    insertActivityLogs(stocks.getSymbol(), "s", "STOP LOSS SET " + stockLevels.getSellSl());
                                }
                            }
                            if (triggerInfo.getT6() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target6()) {
                                if (triggerInfo.getT6() == 0) {
                                    //TARGET 6 hit-
                                    values.put(DatabaseConstants.KEY_IS_T6, stockLevels.getSell_target6());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target4());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "b", stockLevels.getSell_target4());
                                    insertActivityLogs(stocks.getSymbol(),"s","T6 HIT " + " @" + stockLevels.getSell_target6());
                                } else if (ltp > stockLevels.getSell_target4()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target4());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target4());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSell_target4());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSell_target4());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target4(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"s","STOP LOSS (TSL) T4 HIT " + " @" + stockLevels.getSell_target4());
                                }
                            }
                            if (triggerInfo.getT5() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target6()) {
                                if (triggerInfo.getT5() == 0) {
                                    //TARGET 5 hit-
                                    values.put(DatabaseConstants.KEY_IS_T5, stockLevels.getSell_target5());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-5 Achieved " + " @" + stockLevels.getSell_target5(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target3());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "b", stockLevels.getSell_target3());
                                    insertActivityLogs(stocks.getSymbol(),"s","T5 HIT " + " @" + stockLevels.getSell_target5());
                                } else if (ltp > stockLevels.getSell_target3()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target3());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target3());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSell_target3());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSell_target3());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target3(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"s","STOP LOSS (TSL) T3 HIT " + " @" + stockLevels.getSell_target3());
                                }
                            }
                            if (triggerInfo.getT4() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target4()) {
                                if (triggerInfo.getT4() == 0) {
                                    //TARGET 4 hit-
                                    values.put(DatabaseConstants.KEY_IS_T4, stockLevels.getSell_target4());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-4 Achieved " + " @" + stockLevels.getSell_target4(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target2());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "b", stockLevels.getSell_target2());
                                    insertActivityLogs(stocks.getSymbol(),"s","T4 HIT " + " @" + stockLevels.getSell_target4());
                                } else if (ltp > stockLevels.getSell_target2()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target2());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target2());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSell_target2());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSell_target2());
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target2(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"s","STOP LOSS (TSL) T2 HIT " + " @" + stockLevels.getSell_target2());
                                }
                            }
                            if (triggerInfo.getT3() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target3()) {
                                if (triggerInfo.getT3() == 0) {
                                    //TARGET 3 hit-
                                    values.put(DatabaseConstants.KEY_IS_T3, stockLevels.getSell_target3());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-3 Achieved " + " @" + stockLevels.getSell_target3(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target1());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "b", stockLevels.getSell_target1());
                                    insertActivityLogs(stocks.getSymbol(),"b","T3 HIT " + " @" + stockLevels.getSell_target3());
                                } else if (ltp > stockLevels.getSell_target1()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target1());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target1());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSell_target1());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSell_target1());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target1(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"s","STOP LOSS (TSL) T1 HIT " + " @" + stockLevels.getSell_target1());
                                }
                            }
                            if (triggerInfo.getT2() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target2()) {
                                if (triggerInfo.getT2() == 0) {
                                    //TARGET 2 hit-
                                    values.put(DatabaseConstants.KEY_IS_T2, stockLevels.getSell_target2());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getSell_target2(), stockLevels.getId());
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSellAt());
                                    modifyorder(triggerInfo.getOrderId(),triggerInfo.getModOrderId(),stocks.getSymbol(), "b", stockLevels.getSellAt());
                                    insertActivityLogs(stocks.getSymbol(),"s","T2 HIT " + " @" + stockLevels.getSell_target2());
                                } else if (ltp > stockLevels.getSellAt()) {
                                    //values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellAt());
                                    values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                    values.put(DatabaseConstants.KEY_TSL, stockLevels.getSellAt());
                                    values.put(DatabaseConstants.KEY_EXIS_PRICE, stockLevels.getSellAt());
                                    values.put(DatabaseConstants.IS_ACTIVE, 2);
                                    values.put(DatabaseConstants.KEY_CHANGE, triggerInfo.getRecPrice() - stockLevels.getSellAt());
                                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSellAt(), stockLevels.getId());
                                    insertActivityLogs(stocks.getSymbol(),"s","STOP LOSS (TSL) T1 HIT " + " @" + stockLevels.getSell_target1());
                                }
                            }
                            if (triggerInfo.getT1() == 0 && Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target1()) {
                                //TARGET 1 hit-
                                values.put(DatabaseConstants.KEY_IS_T1, stockLevels.getSell_target1());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-1 Achieved " + " @" + stockLevels.getSell_target1(), stockLevels.getId());
                                insertActivityLogs(stocks.getSymbol(),"s","T1 HIT " + " @" + stockLevels.getSell_target1());
                                if(TextUtils.isEmpty(triggerInfo.getOrderId())){
                                    insertActivityLogs(stocks.getSymbol(),"b","Trigger canceled..." + stockLevels.getScriptName());
                                    canclePendingOrder(stockLevels.getScriptName());
                                }
                            }
                        }
                    }
                }
                updateStockTriggerInfo(values, whereCondi);
            } else {
                double ltp = Utility.getInstance().convertTriggerPrice(Utility.getInstance().convertDouble(stocks.getLtP()));
                double open = Utility.getInstance().convertTriggerPrice(stockLevels.getOpen());
                int status = 1;
                if (triggerInfo == null) {
                    status = 1;
                } else {
                    if (triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL) && ltp > open) {
                        //CLOSE SELL SESSOIN
                        //Cancel order if pending
                        status = 0;
                    } else if (triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY) && ltp < open) {
                        //CLOSE BUY SESSOIN
                        //Cancel order if pending
                        status = 0;
                    }
                }
                if (status == 1) {
                    //CREATE NEW SESSION
                    //CHECK TSL HIT
                    if (triggerInfo != null && triggerInfo.getIsActive() == 2) {
                        if (triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY) && ltp > open) {
                            return;
                        }
                    }
                    if (triggerInfo != null && triggerInfo.getIsActive() == 2) {
                        if (triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL) && ltp < open) {
                            return;
                        }
                    }
                    int TRIGGER_ACTION = isTriggerLevel(stockLevels, stocks);
                    switch (TRIGGER_ACTION) {
                        case ACTION_TRIGGER_BUY:
                            if(ltp < stockLevels.getBuyAt()) {
                                insertActivityLogs(stocks.getSymbol(),"b","Session started...");
                                placeFreshOrder(stocks, stockLevels.getBuyAt(), stockLevels.getTarget1(), stockLevels.getBuySl(), CONST_BUY);
                            }else{
                                insertActivityLogs(stocks.getSymbol(),"b","Session started...but price not in our range");
                            }
                            break;
                        case ACTION_TRIGGER_SELL:
                            if(ltp > stockLevels.getSellAt()) {
                                insertActivityLogs(stocks.getSymbol(), "s", "Session started...");
                                placeFreshOrder(stocks, stockLevels.getSellAt(), stockLevels.getSell_target1(), stockLevels.getSellSl(), CONST_SELL);
                            }else{
                                insertActivityLogs(stocks.getSymbol(),"b","Session started...but price not in our range");
                            }
                            break;
                        case ACTION_TRIGGER_NONE:
                            break;
                    }
                }
            }
        }
        //if (getPendingOrdersCount() > 0) {
            StockSdk.getInstance().getOrderHistory(mContext);
        //}
    }

    public void cancelPendingOrder(String orderId) {
        OrderInfo orderInfo = getOrderInfoByOrderId(orderId);
        if(orderInfo!=null && !orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE)){
            if(EntPreferences.isPaid(mContext)) {
                StockSdk.getInstance().cancelSingeOrder(mContext, orderInfo.getOrder_id());
            }
        }
    }

    private int getQuantity(String symbol,double triggerPrice) {
        return  (int) Math.round(3000/triggerPrice);
    }




    /**
     * Method is responsible to place fresh order
     **/
    protected void placeFreshOrder(Stocks stocks, double entryPrice, double target1,double slLevel, String signal) {
        entryPrice = Utility.getInstance().convertTriggerPrice(entryPrice);
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_SCRIP, stocks.getSymbol());
        values.put(DatabaseConstants.KEY_DATE, Utility.getInstance().formatDate());
        values.put(DatabaseConstants.KEY_LTP, entryPrice);
        values.put(DatabaseConstants.KEY_RECORD_PRICE, entryPrice);
        values.put(DatabaseConstants.KEY_SIGNAL, signal);
        values.put(DatabaseConstants.KEY_LOW, entryPrice);
        values.put(DatabaseConstants.KEY_HIGH, entryPrice);
        values.put(DatabaseConstants.KEY_ORDER_ID, "null");
        //values.put(DatabaseConstants.KEY_TSL, open);
        values.put(DatabaseConstants.KEY_STATUS, 1);
        values.put(DatabaseConstants.IS_ACTIVE, -1);
        values.put(UPDATED_TIME, System.currentTimeMillis());
        values.put(DatabaseConstants.CREATED_TIME, System.currentTimeMillis());
        insertData(DatabaseConstants.TABLE_TRIGGER_INFO, values);
        insertActivityLogs(stocks.getSymbol(),signal,"Place Fresh order @ "+entryPrice);
        int quantity = getQuantity(stocks.getSymbol(),entryPrice);
        placeOrder(stocks.getSymbol(), "SL", quantity, signal, entryPrice);
        if(stocks.getSymbol().contains("NIFTY")){
            return;
        }
        if(!TextUtils.isEmpty(EntPreferences.getUpstoxToken(mContext))) {
            if(EntPreferences.isPaid(mContext)) {
                StockSdk.getInstance().placeOrder(mContext, stocks.getSymbol(), signal, target1, slLevel, entryPrice, quantity);
            }
        }

    }

    /**
     * Method is responsible to place fresh order
     **/
    protected void placeModifyOrder(Stocks stocks, double triggerPrice, String signal) {
        triggerPrice = Utility.getInstance().convertTriggerPrice(triggerPrice);
        insertActivityLogs(stocks.getSymbol(),signal,"Place modify order @ "+triggerPrice);
        int quantity = getQuantity(stocks.getSymbol(),triggerPrice);
        placeOrder(stocks.getSymbol(), "SL-M", quantity, signal, triggerPrice);
        if(EntPreferences.isPaid(mContext)) {
            StockSdk.getInstance().placeModifyOrder(mContext, stocks.getSymbol(), signal, triggerPrice, quantity);
        }
    }

    /**
     * Method is responsible to place fresh order
     **/
    public synchronized void insertActivityLogs(String symbol,String signal,String message) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_SCRIP, symbol);
        values.put(DatabaseConstants.KEY_DATETIME, Utility.getInstance().formatDate());
        values.put(DatabaseConstants.KEY_SIGNAL, signal);
        values.put(DatabaseConstants.KEY_STATUS, message);
        values.put(DatabaseConstants.CREATED_TIME, System.currentTimeMillis());
        insertData(DatabaseConstants.TABLE_ACTIVITY_LOGS, values);
        Log.d("UPSACT",symbol+" "+message+" "+signal);
    }

    /**
     * Check Trigger level
     * Logic :- place order before crossing the trigger price
     * canculate virtual trigger between open and trigger price
     * */
    private int isTriggerLevel(StockInfo stockLevels, Stocks stocks) {
        double ltp = Utility.getInstance().convertTriggerPrice(Utility.getInstance().convertDouble(stocks.getLtP()));
        //CHECK TRIGGER LEVEL FOR BUYING
        if (ltp > stockLevels.getOpen()) {
            double differenceInTrigger = stockLevels.getBuyAt() - stockLevels.getOpen();
            double virtalTriggerLevel = stockLevels.getOpen() + (differenceInTrigger / 2);
            if (ltp > virtalTriggerLevel) {
                return ACTION_TRIGGER_BUY;
            }
        } else if (ltp < stockLevels.getOpen()) {
            double differenceInTrigger = stockLevels.getOpen() - stockLevels.getSellAt();
            double virtalTriggerLevel = stockLevels.getOpen() - (differenceInTrigger / 2);
            if (ltp < virtalTriggerLevel) {
                return ACTION_TRIGGER_SELL;
            }
        } else {
            return ACTION_TRIGGER_NONE;
        }
        return ACTION_TRIGGER_NONE;
    }

    private synchronized void setStopLoss(String orderId,String symbol, String signal, double trigger) {
        if (TextUtils.isEmpty(EntPreferences.getUpstoxToken(mContext))) {
            return;
        }
        if (TextUtils.isEmpty(orderId)) {
            return;
        }
        OrderInfo info = getOpenOrderDetails(orderId);
        if(info!=null && info.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE)) {
            modifyorder(orderId,"", symbol, signal, trigger);
        }
    }

    private synchronized void modifyorder(String orderId,String modifiedOrderId,String symbol, String signal, double triggerAt) {
        //trigger pending
        Log.d(TAGS, "modify request called");
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(mContext))) {
            Log.d(TAGS, "null token found...exit "+symbol);
            return;
        }
        if(TextUtils.isEmpty(orderId)){
            Log.d(TAGS, "empty order id");
            insertActivityLogs(symbol,signal,"Order not executed "+orderId);
            return;
        }
        OrderInfo orderInfo = getOpenOrderDetails(orderId);
        if(orderInfo==null){
            Log.d(TAGS, symbol+" actual order not executed....not getting order info");
            return;
        }
        if(!orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE)){
            Log.d(TAGS, symbol+"  actual order not executed....So not able to set stoploss");
            insertActivityLogs(symbol,signal,"Order status not completed  "+symbol+ "  "+orderInfo.getOrder_status());
            return;
        }

        if (EntPreferences.getUpstoxKeyOrderType(mContext).equalsIgnoreCase("oco")) {
            insertActivityLogs(symbol,signal,"Order type is oco, So not modified any order for  "+symbol);
            Log.d(TAGS, "modify request cancled ...because type is OCO");
            return;
        }

        Stocks stocks = new Stocks();
        stocks.setSymbol(symbol);
        Log.d(TAGS, "modify request called for "+symbol);

        List<OrderInfo> lastOrderInfoList = getIncompleteOrderList(symbol);
        triggerAt = Utility.getInstance().convertTriggerPrice(triggerAt);
        OrderInfo modifiedOrder = getOrderInfoByOrderId(modifiedOrderId);
        if(modifiedOrder!=null && !modifiedOrder.getOrder_status().equalsIgnoreCase(OrderStatus.COMPLETE) && !modifiedOrder.getOrder_status().equalsIgnoreCase(OrderStatus.REJECTED) && !modifiedOrder.getOrder_status().equalsIgnoreCase(OrderStatus.CANCELLED)){
            Log.d(TAG_MODI_ORD, "Order modified triggerd " + symbol + triggerAt+" "+modifiedOrderId);
            canclePendingOrderById(symbol,modifiedOrderId);
            StockSdk.getInstance().modifyOrder(mContext,modifiedOrderId,triggerAt);
        }else /*if (lastOrderInfoList == null || TextUtils.isEmpty(modifiedOrderId))*/ {
            //no sl order placed
            canclePendingOrder(symbol);
            placeModifyOrder(stocks, triggerAt, signal);
            Log.d(TAG_MODI_ORD, "order SL_M Trigger Placed new " + symbol + triggerAt);
            insertActivityLogs(symbol,signal," Modify Order(STOP LOSS) for  "+symbol+" "+triggerAt);
        }
    }

    public void canclePendingOrder(String symbol){
        List<OrderInfo> list = getIncompleteOrderList(symbol);
        if(list!=null){
            String token="";
            StringBuffer orderIds = new StringBuffer();
            for (int index=0;index<list.size();index++) {
                orderIds.append(token+list.get(index).getOrder_id());
                token=",";
            }
            StockSdk.getInstance().cancelSingeOrder(mContext, orderIds.toString());
        }
    }

    public void canclePendingOrderById(String symbol,String orderid){
        List<OrderInfo> list = getIncompleteOrderList(symbol);
        if(list!=null){
            String token="";
            StringBuffer orderIds = new StringBuffer();
            for (int index=0;index<list.size();index++) {
                if(!list.get(index).getOrder_id().equalsIgnoreCase(orderid)) {
                    orderIds.append(token + list.get(index).getOrder_id());
                    token = ",";
                }
            }
            StockSdk.getInstance().cancelSingeOrder(mContext, orderIds.toString());
        }
    }

    private void updateStockTriggerInfo(ContentValues values, String where) {
        if (updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)) {
            Log.d("RefUpd", "Record updated...");
        } else {
            Log.d("RefUpd", "Record not updated...");
        }
    }

    public synchronized List<TriggerInfo> getIncompletedTriggers() {
        List<TriggerInfo> triggerList = new ArrayList<>();
        Cursor cursor = null;
        if (db == null) {
            open();
        }
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_STATUS + "=1",
                    null,
                    null, null, null
                    , null);

            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    try {
                        TriggerInfo triggerInfo = new TriggerInfo();
                        triggerInfo.setId(cursor.getInt(iId));
                        triggerInfo.setSymbol(cursor.getString(indexSymbol));
                        triggerInfo.setSignal(cursor.getString(iSignal));
                        triggerInfo.setLtp(cursor.getDouble(iLtp));
                        triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                        triggerInfo.setHigh(cursor.getDouble(iHigh));
                        triggerInfo.setLow(cursor.getDouble(iLow));
                        triggerInfo.setT1(cursor.getDouble(iT1));
                        triggerInfo.setT2(cursor.getDouble(iT2));
                        triggerInfo.setT3(cursor.getDouble(iT3));
                        triggerInfo.setT4(cursor.getDouble(iT4));
                        triggerInfo.setT5(cursor.getDouble(iT5));
                        triggerInfo.setT6(cursor.getDouble(iT6));
                        triggerInfo.setSl(cursor.getDouble(iSl));
                        triggerInfo.setTsl(cursor.getDouble(iTSL));
                        triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                        triggerInfo.setIsActive(cursor.getInt(iActive));
                        triggerList.add(triggerInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getIncompletedTriggers() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerList;
    }

    public synchronized TriggerInfo getTriggerInfo(String symbol) {
        TriggerInfo triggerInfo = null;
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=?" + " and " +
                            DatabaseConstants.KEY_DATE + "=? and " + DatabaseConstants.KEY_STATUS + "= ?",
                    new String[]{symbol, Utility.getInstance().formatDate(), String.valueOf(1)},
                    null, null, null, null);

            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
                int iOrdId = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
                int iModiOrdId = cursor.getColumnIndex(DatabaseConstants.KEY_MODIFY_ORDER_ID);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    triggerInfo = new TriggerInfo();
                    triggerInfo.setId(cursor.getInt(iId));
                    triggerInfo.setOrderId(cursor.getString(iOrdId));
                    triggerInfo.setModOrderId(cursor.getString(iModiOrdId));
                    triggerInfo.setSymbol(cursor.getString(indexSymbol));
                    triggerInfo.setSignal(cursor.getString(iSignal));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setT1(cursor.getDouble(iT1));
                    triggerInfo.setT2(cursor.getDouble(iT2));
                    triggerInfo.setT3(cursor.getDouble(iT3));
                    triggerInfo.setT4(cursor.getDouble(iT4));
                    triggerInfo.setT5(cursor.getDouble(iT5));
                    triggerInfo.setT6(cursor.getDouble(iT6));
                    triggerInfo.setSl(cursor.getDouble(iSl));
                    triggerInfo.setTsl(cursor.getDouble(iTSL));
                    triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                    triggerInfo.setIsActive(cursor.getInt(iActive));
                    triggerInfo.setSessionStatus(cursor.getInt(iStatus));

                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() 720 : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerInfo;
    }

    /*public synchronized String[] getFavList() {
        String[] favList = new String[]{
                "BAJAJFINSV",
                "OFSS",
                "HINDUNILVR",
                "ULTRACEMCO",
                "HEROMOTOCO",
                "BAJAJ-AUTO",
                "DRREDDY",
                "TCS",
                "HDFCBANK",
                "HDFC",
                "INDUSINDBK",
                "KOTAKBANK",
                "IBULHSGFIN",
                "LUPIN",
                "TITAN",
                "INFY",
                "SUNPHARMA",
                "TATASTEEL",
                "AXISBANK",
                "RBLBANK",
                "LICHSGFIN",
                "M&MFIN",
                "SBIN",
                "YESBANK",
                "MARUTI",
                "SRTRANSFIN",
                "BRITANNIA"
        };
        return favList;
    }*/

    public synchronized String[] getFavList() {
        Cursor cursor = null;
        String[] list = new String[0];
        try {
            String query = "select * from '" + DatabaseConstants.TABLE_FAV_SYMBOLS + "' group by script order by script desc limit 70";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() > 0) {
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                list = new String[cursor.getCount()];
                //*fetch last consumed data*//*
                int index = 0;
                while (cursor.moveToNext()) {
                    list[index] = cursor.getString(indexSymbol);
                    index++;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getGetFav() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    public synchronized boolean isAddedInFav(String symbol) {
        Cursor cursor = null;
        boolean isAdded = false;
        try {
            String query = "select * from '" + DatabaseConstants.TABLE_FAV_SYMBOLS + "'where " + DatabaseConstants.KEY_SCRIP + "='" + symbol + "' group by script order by script desc limit 50";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() > 0) {
                isAdded = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getGetFav() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return isAdded;
    }

    public synchronized List<TriggerInfo> getTriggerList(String symbol) {
        List<TriggerInfo> triggerList = new ArrayList<>();
        Cursor cursor = null;
        boolean isExec = false;
        try {
            String query = "";
            if (symbol.equalsIgnoreCase("") || symbol.equalsIgnoreCase("All")) {
                query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' " +
                        " where " + DatabaseConstants.KEY_DATE + "='" + Utility.getInstance().formatDate() + "'  order by id desc limit 100";
                cursor = db.rawQuery(query, null);
            } else if (symbol.equalsIgnoreCase("FAV")) {
                String[] favList = getFavList();
                query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "'" +
                        " WHERE " + DatabaseConstants.KEY_DATE + "='" + Utility.getInstance().formatDate() + "' and  script IN (" + TextUtils.join(",", Collections.nCopies(favList.length, "?")) + ") order by id desc limit 100";
                cursor = db.rawQuery(query, favList);
            } else if (symbol.equalsIgnoreCase("INDEX")) {
                String[] indexList = new String[]{"NIFTY BANK", "NIFTY 50"};
                query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "'" +
                        " WHERE " + DatabaseConstants.KEY_DATE + "='" + Utility.getInstance().formatDate() + "' and script IN (" + TextUtils.join(",", Collections.nCopies(indexList.length, "?")) + ") order by id desc limit 100";
                cursor = db.rawQuery(query, indexList);
            } else if (symbol.equalsIgnoreCase("STOCK")) {
                String[] indexList = new String[]{"NIFTY BANK", "NIFTY 50"};
                query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "'" +
                        " WHERE " + DatabaseConstants.KEY_DATE + "='" + Utility.getInstance().formatDate() + "' and script NOT IN (" + TextUtils.join(",", Collections.nCopies(indexList.length, "?")) + ") order by id desc limit 100";
                cursor = db.rawQuery(query, indexList);
            } else {
                cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                        DatabaseConstants.KEY_SCRIP + "='" + symbol + "' and " + DatabaseConstants.KEY_DATE + "='" + Utility.getInstance().formatDate() + "'",
                        null,
                        null, null, CREATED_TIME + " desc", "100");
            }

            //cursor = db.rawQuery(query,null);
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);
                int iChange = cursor.getColumnIndex(KEY_CHANGE);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);
                int iExitPrice = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    TriggerInfo triggerInfo = new TriggerInfo();
                    triggerInfo.setId(cursor.getInt(iId));
                    triggerInfo.setSymbol(cursor.getString(indexSymbol));
                    triggerInfo.setExitPrice(cursor.getDouble(iExitPrice));
                    triggerInfo.setSignal(cursor.getString(iSignal));
                    triggerInfo.setChange(cursor.getDouble(iChange));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setT1(cursor.getDouble(iT1));
                    triggerInfo.setT2(cursor.getDouble(iT2));
                    triggerInfo.setT3(cursor.getDouble(iT3));
                    triggerInfo.setT4(cursor.getDouble(iT4));
                    triggerInfo.setT5(cursor.getDouble(iT5));
                    triggerInfo.setT6(cursor.getDouble(iT6));
                    triggerInfo.setSl(cursor.getDouble(iSl));
                    triggerInfo.setIsActive(cursor.getInt(iActive));
                    triggerInfo.setUpdateTime(cursor.getString(iUpdateTime));
                    triggerInfo.setTsl(cursor.getDouble(iTSL));
                    triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                    triggerList.add(triggerInfo);

                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() 901 : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerList;
    }

    public synchronized List<String> getStockList() {
        List<String> scriptList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_LIVE_DATA, null,
                    null,
                    null,
                    DatabaseConstants.KEY_SCRIP, null, DatabaseConstants.KEY_SCRIP + " desc", null);
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+DatabaseConstants.IS_ACTIVE+" = 0 order by created_date desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    scriptList.add(cursor.getString(indexSymbol));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() 931: " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return scriptList;
    }

    public synchronized StockInfo getStockLevels(String symbol) {
        StockInfo triggerInfo = null;
        Cursor cursor = null;
        if (db == null) {
            open();
        }
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_LIVE_DATA, null,
                    DatabaseConstants.KEY_SCRIP + "=?" + " and " +
                            DatabaseConstants.KEY_DATE + "=?",
                    new String[]{symbol, Utility.getInstance().formatDate()},
                    null, null, null, null);

            /*String query = "select * from '" + DatabaseConstants.TABLE_LIVE_DATA + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' order by id desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iID = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iOpen = cursor.getColumnIndex(DatabaseConstants.KEY_OPEN);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iPClose = cursor.getColumnIndex(DatabaseConstants.KEY_PRE_CLOSE);
                int iBuySL = cursor.getColumnIndex(DatabaseConstants.KEY_BUY_STOP_LOSS);
                int iBuyAt = cursor.getColumnIndex(DatabaseConstants.KEY_BUY_AT);
                int iBT1 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_1);
                int iBT2 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_2);
                int iBT3 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_3);
                int iBT4 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_4);
                int iBT5 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_5);
                int iBT6 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_6);
                int iSellSL = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_STOP_LOSS);
                int iSellAt = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_AT);
                int iSellT1 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_1);
                int iSellT2 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_2);
                int iSellT3 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_3);
                int iSellT4 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_4);
                int iSellT5 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_5);
                int iSellT6 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_6);


                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    triggerInfo = new StockInfo();
                    triggerInfo.setId(cursor.getInt(iID));
                    triggerInfo.setScriptName(cursor.getString(indexSymbol));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setOpen(cursor.getDouble(iOpen));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setPreviousClose(cursor.getDouble(iPClose));
                    triggerInfo.setBuySl(cursor.getDouble(iBuySL));
                    triggerInfo.setBuyAt(cursor.getDouble(iBuyAt));
                    triggerInfo.setTarget1(cursor.getDouble(iBT1));
                    triggerInfo.setTarget2(cursor.getDouble(iBT2));
                    triggerInfo.setTarget3(cursor.getDouble(iBT3));
                    triggerInfo.setTarget4(cursor.getDouble(iBT4));
                    triggerInfo.setTarget5(cursor.getDouble(iBT5));
                    triggerInfo.setTarget6(cursor.getDouble(iBT6));
                    triggerInfo.setSellSl(cursor.getDouble(iSellSL));
                    triggerInfo.setSellAt(cursor.getDouble(iSellAt));
                    triggerInfo.setSell_target1(cursor.getDouble(iSellT1));
                    triggerInfo.setSell_target2(cursor.getDouble(iSellT2));
                    triggerInfo.setSell_target3(cursor.getDouble(iSellT3));
                    triggerInfo.setSell_target4(cursor.getDouble(iSellT4));
                    triggerInfo.setSell_target5(cursor.getDouble(iSellT5));
                    triggerInfo.setSell_target6(cursor.getDouble(iSellT6));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() 1009s : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerInfo;
    }

    public void dumpLiveData(Stocks stockModel) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_SCRIP, stockModel.getSymbol());
        values.put(DatabaseConstants.KEY_DATE, Utility.getInstance().formatDate());

        values.put(DatabaseConstants.KEY_OPEN, Utility.getInstance().convertDouble(stockModel.getOpen()));
        values.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stockModel.getLtP()));
        //values.put(DatabaseConstants.KEY_HIGH,Utility.getInstance().convertDouble(stockModel.getHigh()));
        //values.put(DatabaseConstants.KEY_LOW,Utility.getInstance().convertDouble(stockModel.getLow()));
        //values.put(DatabaseConstants.KEY_PRE_CLOSE,Utility.getInstance().convertDouble(stockModel.getPreviousClose()));
        double openPrice = Utility.getInstance().convertDouble(stockModel.getOpen());
        /**
         * Generating Triggers
         * */

        values.put(DatabaseConstants.KEY_BUY_STOP_LOSS, openPrice);
        values.put(DatabaseConstants.KEY_BUY_AT, calculateBuyValue(openPrice, getBuyFormula(openPrice)));
        values.put(DatabaseConstants.KEY_TARGET_1, calculateBuyValue(openPrice, FM_T1));
        values.put(DatabaseConstants.KEY_TARGET_2, calculateBuyValue(openPrice, FM_T2));
        values.put(DatabaseConstants.KEY_TARGET_3, calculateBuyValue(openPrice, FM_T3));
        values.put(DatabaseConstants.KEY_TARGET_4, calculateBuyValue(openPrice, FM_T4));
        values.put(DatabaseConstants.KEY_TARGET_5, calculateBuyValue(openPrice, FM_T5));
        values.put(DatabaseConstants.KEY_TARGET_6, calculateBuyValue(openPrice, FM_T6));

        values.put(DatabaseConstants.KEY_SELL_AT, calculateSellValue(openPrice, getBuyFormula(openPrice)));
        values.put(DatabaseConstants.KEY_SELL_TARGET_1, calculateSellValue(openPrice, FM_T1));
        values.put(DatabaseConstants.KEY_SELL_TARGET_2, calculateSellValue(openPrice, FM_T2));
        values.put(DatabaseConstants.KEY_SELL_TARGET_3, calculateSellValue(openPrice, FM_T3));
        values.put(DatabaseConstants.KEY_SELL_TARGET_4, calculateSellValue(openPrice, FM_T4));
        values.put(DatabaseConstants.KEY_SELL_TARGET_5, calculateSellValue(openPrice, FM_T5));
        values.put(DatabaseConstants.KEY_SELL_TARGET_6, calculateSellValue(openPrice, FM_T6));
        values.put(DatabaseConstants.KEY_SELL_STOP_LOSS, openPrice);


        long affected = insertData(DatabaseConstants.TABLE_LIVE_DATA, values);
        Log.d("Dbadp", values.toString() + affected);
    }

    private double getBuyFormula(double openPrice) {
        double triggerFormula = 0.125;
        if (openPrice > 15000) {
            triggerFormula = 0.0625;
        }
        return triggerFormula;
    }

    private double calculateBuyValue(double openPrice, double formula) {
        double sqrt = Math.sqrt(openPrice);
        double output = sqrt + formula;
        return Utility.getInstance().convertTwoDecimal((output * output));
    }

    private double calculateSellValue(double openPrice, double formula) {
        //return ((Math.sqrt(openPrice) - getBuyFormula(openPrice)) * 2);
        double sqrt = Math.sqrt(openPrice);
        double output = sqrt - formula;
        return Utility.getInstance().convertTwoDecimal(output * output);
    }


    public void clearOldData() {
        int recordCount = getCursorCount("historical_data", null);
        if (recordCount > 15000) {
            db.execSQL("delete from historical_data where id < ( select id from historical_data order by id limit 3000,1)");
        }
        int recordCount2 = getCursorCount("five_min_data", null);
        if (recordCount2 > 15000) {
            db.execSQL("delete from five_min_data where id < ( select id from five_min_data order by id limit 3000,1)");
        }
    }

    /*private void updateOrderSL(int id, double sl) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_SL_SET, 1);
        String where = DatabaseConstants.ID + "='" + id + "'";
        updateData(DatabaseConstants.TABLE_ORDER_INFO, values, where);
    }*/

    public void updateOrderStatus(String orderid, int status) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_STATUS, status);
        String where = DatabaseConstants.KEY_ORDER_ID + "='" + orderid + "'";
        if (!updateData(DatabaseConstants.TABLE_ORDER_INFO, values, where)) {
            /*where =  KEY_ORDER_ID+"='' and "+DatabaseConstants.KEY_SCRIP+"='"+symbol+"'";
            updateData(DatabaseConstants.TABLE_ORDER_INFO,values,where);*/
        }
    }

    public void clearTriggerInfo(String symbol){
        TriggerInfo trigger = getTriggerInfo(symbol);
        if(trigger!=null) {
            ContentValues values = new ContentValues();
            values.put(DatabaseConstants.KEY_ORDER_ID, "");
            String where = DatabaseConstants.ID + "=" + trigger.getId();
            if (!updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)) {
                Log.d(TAGS, "Order not updated intrigger info");
            }
        }
    }

    public void updateOrderIdInTriggerInfo(String symbol, String orderId) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_ORDER_ID, orderId);
        //String where = DatabaseConstants.KEY_ORDER_STATUS+"='open' and "+DatabaseConstants.KEY_SCRIP+"='"+symbol+"'";
        String where = DatabaseConstants.KEY_SCRIP+ "='" + symbol + "' and "+DatabaseConstants.KEY_ORDER_ID+"='null' ";
        if (!updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)) {
            TriggerInfo trigger = getTriggerInfo(symbol);
            if(trigger!=null) {
                where = DatabaseConstants.ID + "=" + trigger.getId();
                if(!updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)){
                    Log.d(TAGS, "Order not updated intrigger info");
                }

            }
        }else{
            Log.d(TAGS,"Order updated intrigger info");
        }
    }

    public void updateOrderIdInTriggerInfoModify(String symbol, String modifyOrderId) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_MODIFY_ORDER_ID, modifyOrderId);
        //String where = DatabaseConstants.ID+ "=(select id from "+DatabaseConstants.TABLE_TRIGGER_INFO+" where "+DatabaseConstants.KEY_SCRIP+"='"+symbol+"' order by id desc limit 1)";
        String where = DatabaseConstants.KEY_SCRIP+"='"+symbol+"' and "+DatabaseConstants.KEY_STATUS+"=1";
        if (!updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)) {
            TriggerInfo trigger = getTriggerInfo(symbol);
            if(trigger!=null) {
                where = DatabaseConstants.ID + "=" + trigger.getId();
                if(!updateData(DatabaseConstants.TABLE_TRIGGER_INFO, values, where)){
                    Log.d(TAGS, "Order not updated intrigger info");
                }

            }
        }else{
            Log.d(TAGS,"Order updated intrigger info");
        }
    }

    public void updateOrderId(String symbol, String orderId,String orderStatus) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_ORDER_ID, orderId);
        values.put(DatabaseConstants.KEY_STATUS, 1);
        values.put(DatabaseConstants.KEY_ORDER_STATUS, orderStatus);
        //String where = DatabaseConstants.KEY_ORDER_STATUS+"='open' and "+DatabaseConstants.KEY_SCRIP+"='"+symbol+"'";
        String where = DatabaseConstants.KEY_ORDER_ID + "='" + orderId + "'";
        if (!updateData(DatabaseConstants.TABLE_ORDER_INFO, values, where)) {
            where = KEY_ORDER_ID + "='' and " + DatabaseConstants.KEY_SCRIP + "='" + symbol + "'";
            updateData(DatabaseConstants.TABLE_ORDER_INFO, values, where);
        }
        updateOrderIdInTriggerInfo(symbol,orderId);
    }



    public void updateOrder(String orderId, String symbol, String orderType, String transactionType, String exchange, BigDecimal price, long qty, String status) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_STATUS, 0);
        values.put(DatabaseConstants.KEY_SCRIP, symbol);
        values.put(DatabaseConstants.KEY_QUANTITY, qty);
        values.put(DatabaseConstants.KEY_SIGNAL, transactionType);
        values.put(DatabaseConstants.KEY_ORDER_ID, orderId);
        values.put(DatabaseConstants.KEY_ORDER_STATUS, status);
        values.put(DatabaseConstants.KEY_ORDER_TYPE, orderType);
        //values.put(DatabaseConstants.KEY_RECORD_PRICE, ""+price);
        values.put(DatabaseConstants.KEY_CHANGE, 0.0);
        values.put(DatabaseConstants.KEY_STATUS, 1);
        values.put(DatabaseConstants.KEY_ORDER_ID, orderId);
        values.put(DatabaseConstants.KEY_DATETIME, Utility.getInstance().getDateTime());
        values.put(DatabaseConstants.KEY_TIMESTAMP, System.currentTimeMillis());
        String where = DatabaseConstants.KEY_ORDER_ID + "='" + orderId + "'";
        if (updateData(DatabaseConstants.TABLE_ORDER_INFO, values, where)) {
            //Log.d("UpstoxBridge","Update order info "+symbol);
        } else {
            Log.d("UpstoxBridge", "Inser New in Update order info " + symbol + " " + orderId);
            insertData(DatabaseConstants.TABLE_ORDER_INFO, values);
        }
    }

    public void placeOrder(String symbol, String orderType, int qty, String signal, double triggerPrice) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_STATUS, 0);
        values.put(DatabaseConstants.KEY_SCRIP, symbol);
        values.put(DatabaseConstants.KEY_QUANTITY, qty);
        values.put(DatabaseConstants.KEY_SIGNAL, signal);
        values.put(DatabaseConstants.KEY_ORDER_ID, "");
        values.put(DatabaseConstants.KEY_ORDER_STATUS, "open");
        values.put(DatabaseConstants.KEY_ORDER_TYPE, orderType);
        values.put(DatabaseConstants.KEY_RECORD_PRICE, triggerPrice);
        values.put(DatabaseConstants.KEY_CHANGE, 0.0);
        values.put(DatabaseConstants.KEY_STATUS, 0);
        values.put(DatabaseConstants.KEY_DATETIME, Utility.getInstance().getDateTime());
        values.put(DatabaseConstants.KEY_TIMESTAMP, System.currentTimeMillis());
        long insertId = insertData(DatabaseConstants.TABLE_ORDER_INFO, values);
        Log.d("UpstoxBridge", "Order placed " + symbol+"  "+signal);

    }

    public OrderInfo getOrderInfo(String symbol) {
        OrderInfo orderInfo = null;
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_ORDER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=? and " + DatabaseConstants.KEY_ORDER_STATUS + "=? or " + DatabaseConstants.KEY_ORDER_STATUS + "=? or " + DatabaseConstants.KEY_ORDER_STATUS + "=?",
                    new String[]{symbol, "complete", "pending", "trigger pending"},
                    null, null, null, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            if (cursor.moveToNext()) {
                orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfo;
    }

    public OrderInfo getOrderInfoByOrderId(String orderid) {
        OrderInfo orderInfo = null;
        Cursor cursor = null;
        if(TextUtils.isEmpty(orderid)){
            return null;
        }
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_ORDER_INFO, null,
                    DatabaseConstants.KEY_ORDER_ID + "=? ",
                    new String[]{orderid},
                    null, null, null, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            if (cursor.moveToNext()) {
                orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfo;
    }

    public OrderInfo getLastOrderInfo(String symbol) {
        OrderInfo orderInfo = null;
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_ORDER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=? ",
                    new String[]{symbol},
                    null, null, null, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            if (cursor.moveToNext()) {
                orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfo;
    }

    public OrderInfo getOpenOrderInfo(String symbol) {
        OrderInfo orderInfo = null;
        Cursor cursor = null;
        try {
            /*cursor = db.query(true, DatabaseConstants.TABLE_ORDER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=? and "+DatabaseConstants.KEY_ORDER_STATUS + "=? or "+DatabaseConstants.KEY_ORDER_STATUS + "=?",
                    new String[] {symbol,"open","trigger pending"},
                    null, null, DatabaseConstants.ID+" desc", null);*/
            String query = "SELECT DISTINCT * FROM order_info WHERE script='" + symbol + "' and order_status!='rejected' and (order_status='pending' or order_status='open' or order_status='trigger pending' or order_status='modify validation pending') group by order_id ORDER BY id desc";
            cursor = db.rawQuery(query, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            if (cursor.moveToNext()) {
                orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfo;
    }

    public List<OrderInfo> getIncompleteOrderList(String symbol) {
        List<OrderInfo> orderList = new ArrayList<>();
        Cursor cursor = null;
        try {
            //and order_status='"+ OrderStatus.TRIGGER_PENDING+"'
            String query = "SELECT DISTINCT * FROM order_info WHERE script='" + symbol + "'  group by order_id ORDER BY id desc";
            cursor = db.rawQuery(query, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            while (cursor.moveToNext()) {
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
                if(orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.MODIFY_PENDING) || orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.PUT_ORD_REQ_RCVD) || orderInfo.getOrder_status().equalsIgnoreCase(OrderStatus.TRIGGER_PENDING)) {
                    orderList.add(orderInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderList;
    }


    public OrderInfo getOpenOrderDetails(String orderid) {
        OrderInfo orderInfo = null;
        Cursor cursor = null;
        try {
            String query = "SELECT DISTINCT * FROM order_info WHERE "+DatabaseConstants.KEY_ORDER_ID+"='" + orderid + "' and order_status='"+ OrderStatus.COMPLETE+"' ORDER BY id desc";
            cursor = db.rawQuery(query, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            if (cursor.moveToNext()) {
                orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfo;
    }

    public List<OrderInfo> getDuplicateOrderInfo() {
        Cursor cursor = null;
        List<OrderInfo> orderInfoList = new ArrayList<>();
        try {
            /*cursor = db.query(true, DatabaseConstants.TABLE_ORDER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=? and "+DatabaseConstants.KEY_ORDER_STATUS + "=? or "+DatabaseConstants.KEY_ORDER_STATUS + "=?",
                    new String[] {symbol,"open","trigger pending"},
                    null, null, DatabaseConstants.ID+" desc", null);*/
            String query = "SELECT DISTINCT * FROM order_info WHERE order_status='trigger pending' ORDER BY script,id asc";
            cursor = db.rawQuery(query, null);
            int iId = cursor.getColumnIndex(DatabaseConstants.ID);
            int iSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
            int iQty = cursor.getColumnIndex(DatabaseConstants.KEY_QUANTITY);
            int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
            int iOrdno = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_ID);
            int iOrdStatus = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_STATUS);
            int iOrdType = cursor.getColumnIndex(DatabaseConstants.KEY_ORDER_TYPE);
            int iOrdTrigger = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
            int iOrdExit = cursor.getColumnIndex(DatabaseConstants.KEY_EXIS_PRICE);
            int iChange = cursor.getColumnIndex(DatabaseConstants.KEY_CHANGE);
            int iStatus = cursor.getColumnIndex(DatabaseConstants.KEY_STATUS);
            int iDateTime = cursor.getColumnIndex(DatabaseConstants.KEY_DATETIME);
            int iTimeStamp = cursor.getColumnIndex(DatabaseConstants.KEY_TIMESTAMP);
            while (cursor.moveToNext()) {
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setId(cursor.getInt(iId));
                orderInfo.setSymbol(cursor.getString(iSymbol));
                orderInfo.setQuantity(cursor.getInt(iQty));
                orderInfo.setTransactionType(cursor.getString(iSignal));
                orderInfo.setOrder_id(cursor.getString(iOrdno));
                orderInfo.setOrder_status(cursor.getString(iOrdStatus));
                orderInfo.setOrderType(cursor.getString(iOrdType));
                orderInfo.setEntry(cursor.getDouble(iOrdTrigger));
                orderInfo.setExit(cursor.getDouble(iOrdExit));
                orderInfo.setChange(cursor.getDouble(iChange));
                orderInfo.setStatus(cursor.getInt(iStatus));
                orderInfo.setDateTime(cursor.getString(iDateTime));
                orderInfo.setTimeStamp(cursor.getLong(iTimeStamp));
                orderInfoList.add(orderInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return orderInfoList;
    }

    public int getPendingOrdersCount() {
        int ordCount = 0;
        Cursor cursor = null;
        try {
            String query = "select * from '" + DatabaseConstants.TABLE_ORDER_INFO + "' where " + KEY_ORDER_ID + "=''";
            cursor = db.rawQuery(query, null);
            int artIndex = cursor.getColumnIndex("id");
            ordCount = cursor.getCount();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return ordCount;
    }

    public int getUniqueNumber(String symbol) {
        int uniqueAppNo = 0;
        Cursor cursor = null;
        try {
            String query = "select id from historical_data where symbol='" + symbol + "' limit 1";
            cursor = db.rawQuery(query, null);
            int artIndex = cursor.getColumnIndex("id");
            if (cursor.getCount() > 0) {
                if (cursor.moveToNext()) {
                    uniqueAppNo = cursor.getInt(artIndex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return uniqueAppNo;
    }


    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASENAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DatabaseConstants.CREATE_TABLE_ACTIVITY_LOGS);
                db.execSQL(DatabaseConstants.CREATE_TABLE_ORDER_INFO);
                db.execSQL(DatabaseConstants.CREATE_TABLE_FAV);
                db.execSQL(DatabaseConstants.CREATE_TABLE_LIVE_DATA);
                db.execSQL(DatabaseConstants.CREATE_TABLE_TRIGGER_INFO);

            } catch (Exception e) {
                Log.e(TAG, "Error in DBAdapter : OnCreate() : " + e.getLocalizedMessage());
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);


        }
    }

}
