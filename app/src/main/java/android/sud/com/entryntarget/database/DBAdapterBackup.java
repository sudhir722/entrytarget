package android.sud.com.entryntarget.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.sud.com.entryntarget.corelogic.notifications.StockerPermissionManager;
import android.sud.com.entryntarget.models.NiftyIndexModel;
import android.sud.com.entryntarget.models.StockInfo;
import android.sud.com.entryntarget.models.StockModel;
import android.sud.com.entryntarget.models.Stocks;
import android.sud.com.entryntarget.models.TriggerInfo;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.utils.Utility;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.sud.com.entryntarget.database.DatabaseConstants.CREATED_TIME;
import static android.sud.com.entryntarget.database.DatabaseConstants.UPDATED_TIME;
import static android.sud.com.entryntarget.utility.EntConstants.CONST_BUY;
import static android.sud.com.entryntarget.utility.EntConstants.CONST_SELL;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T1;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T2;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T3;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T4;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T5;
import static android.sud.com.entryntarget.utility.EntConstants.FM_T6;

/*
 * Its a singletone database adaptor class.  how to use it.
 *
 * From any of your activity create object of this class and use that object to get and insert data
 *
 *  Example : DBAdapter database = DBAdapter.getDBAdapter(this);
 *
 * @auther Sudhir Patil
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class DBAdapterBackup implements Serializable {

    public static final String DATABASENAME = "historytrg.db";
    private static final int DATABASE_VERSION = 4;
    private static SQLiteDatabase db = null;
    private static DBAdapterBackup mInstance;
    private static Context mContext;
    String TAG = "DBAdapterBackup";
    private DatabaseHelperBackup dbHelper = null;

    public DBAdapterBackup() {
    }

    public DBAdapterBackup(Context ctx) {
        dbHelper = new DatabaseHelperBackup(ctx);
        open();
    }

    public static DBAdapterBackup getDBAdapter(Context ctx) {
        if (mInstance == null) {
            mInstance = new DBAdapterBackup(ctx);
        }
        mContext = ctx;
        return mInstance;
    }

    private DBAdapterBackup open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        if (db != null && db.isOpen()) {
            dbHelper.close();
            db = null;
        }
    }

    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public synchronized long insertData(String tableName, ContentValues values) {
        try {
            return db.insert(tableName, null, values);
        } catch (Exception e) {
            return -1;
        }
    }

    public synchronized boolean updateData(String tableName, ContentValues values, String where) {
        try {
            return db.update(tableName, values, where, null) > 0 ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized long reInsertData(String tableName, ContentValues values) {
        isDelete(tableName, null);
        return insertData(tableName, values);
    }

    public synchronized boolean resetTable(String tableName) {
        try {
            db.delete(tableName, null, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public synchronized int isDelete(String tableName, String condition) {
        int affectedRow = -1;
        try {
            affectedRow = db.delete(tableName, condition, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return affectedRow;
    }

    public synchronized int getCursorCount(String tableName, String whereCondition) {
        Cursor cursor = null;
        int rowCount = 0;
        try {
            cursor = db.query(tableName, new String[]{}, whereCondition,
                    null, whereCondition, null, null);
            rowCount = cursor.getCount();
        } catch (Exception e) {
            Log.e(TAG, "DBAdapter : getCursorCount" + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return rowCount;
    }

    public synchronized boolean isDataExists(String tableName, String uniqueColoumName, String WHERE) {
        Cursor cursor = null;
        boolean isDataExists = false;
        try {
            cursor = db.query(tableName, new String[]{}, WHERE,
                    null, null, null, " " + uniqueColoumName + " desc");
            if (cursor.getCount() > 0) {
                isDataExists = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in isDataExists() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return isDataExists;
    }


    public int clearData(String tablename) {
        return isDelete(tablename, null);
    }

    public synchronized void extractStockData(Stocks stocks,String date){
        StockInfo stockLevels = getStockLevels(stocks.getSymbol(),date);
        if (stockLevels == null) {
            dumpLiveData(stocks,date);
            stockLevels = getStockLevels(stocks.getSymbol(),date);
            if (stockLevels != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DatabaseConstants.KEY_LTP,stocks.getOpen());
                String whereCondi = DatabaseConstants.ID+"='"+stockLevels.getId()+"'";
                updateData(DatabaseConstants.TABLE_LIVE_DATA,contentValues,whereCondi);
                checkTriggerInfo(stocks, stockLevels,Utility.getInstance().formatDate());
            }
            checkTriggerInfo(stocks, stockLevels,date);
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.KEY_LTP,stocks.getOpen());
            String whereCondi = DatabaseConstants.ID+"='"+stockLevels.getId()+"'";
            updateData(DatabaseConstants.TABLE_LIVE_DATA,contentValues,whereCondi);
            checkTriggerInfo(stocks, stockLevels,date);
        }
    }

    public synchronized void updateIndexData(StockModel stockList ){
        String dateFormatNse = Utility.getInstance().getDayNseFormat();
        if(stockList.getTime().contains(dateFormatNse)) {
            StockInfo stockLevels = getStockLevels(stockList.getNiftyIndexModelArrayList().get(0).getIndexName(),Utility.getInstance().formatDate());
            if (stockLevels == null) {
                dumpLiveData(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)),Utility.getInstance().formatDate());
                stockLevels = getStockLevels(stockList.getNiftyIndexModelArrayList().get(0).getIndexName(),Utility.getInstance().formatDate());
                if (stockLevels != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseConstants.KEY_LTP,stockList.getNiftyIndexModelArrayList().get(0).getCurrentPrice());
                    String whereCondi = DatabaseConstants.ID+"='"+stockLevels.getId()+"'";
                    updateData(DatabaseConstants.TABLE_LIVE_DATA,contentValues,whereCondi);
                    checkTriggerInfo(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)), stockLevels,Utility.getInstance().formatDate());
                }
            } else {
                checkTriggerInfo(convertIndex(stockList.getNiftyIndexModelArrayList().get(0)), stockLevels,Utility.getInstance().formatDate());
            }
        }
    }

    private  boolean isFilteredSymbol(String symbol){
        for (int index=0;index<DatabaseConstants.filteredSymbols.length;index++){
            if(symbol.equalsIgnoreCase(DatabaseConstants.filteredSymbols[index])){
                return true;
            }
        }
        return false;
    }
    public synchronized void updateStockIndex(StockModel stockList ){
        String currentDay = Utility.getInstance().formatDate();
        if(EntPreferences.getCurrentDate(mContext).equalsIgnoreCase(currentDay)){
            String sd = Utility.getInstance().getDayNseFormat();
            if(stockList.getTime().contains(sd)) {
                EntPreferences.setCurrentDate(mContext, currentDay);
                ArrayList<Stocks> sList = stockList.getStocklist();
                for (int index = 0; index<sList.size();index++ ) {
                    if(isFilteredSymbol(sList.get(index).getSymbol())) {
                        StockInfo stockLevels = getStockLevels(sList.get(index).getSymbol(),Utility.getInstance().formatDate());
                        if (stockLevels == null) {
                            dumpLiveData(sList.get(index),Utility.getInstance().formatDate());
                        } else {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stockList.getNiftyIndexModelArrayList().get(0).getCurrentPrice()));
                            String whereCondi = DatabaseConstants.ID + "='" + stockLevels.getId() + "'";
                            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, whereCondi);
                            checkTriggerInfo(sList.get(index), stockLevels,Utility.getInstance().formatDate());
                        }
                    }
                }
            }
        }else{
            ArrayList<Stocks> sList = stockList.getStocklist();
            for (int index = 0; index<sList.size();index++ ) {
                if(isFilteredSymbol(sList.get(index).getSymbol())) {
                    StockInfo stockLevels = getStockLevels(sList.get(index).getSymbol(),Utility.getInstance().formatDate());
                    if (stockLevels == null) {
                        dumpLiveData(sList.get(index),Utility.getInstance().formatDate());
                    }
                    if (stockLevels == null) {
                        stockLevels = getStockLevels(sList.get(index).getSymbol(),Utility.getInstance().formatDate());
                    }
                    if (stockLevels != null) {
                        checkTriggerInfo(sList.get(index), stockLevels,Utility.getInstance().formatDate());
                    }
                }
            }
        }

    }

    private Stocks convertIndex(NiftyIndexModel model){
        Stocks stocks = new Stocks();
        stocks.setSymbol(model.getIndexName());
        stocks.setLow(model.getLow());
        stocks.setLtP(model.getCurrentPrice());
        stocks.setOpen(model.getOpen());
        stocks.setPreviousClose("0");
        return stocks;
    }

    public synchronized void closeAllOrders(String date){
        ContentValues values = new ContentValues();
        values.put(UPDATED_TIME,System.currentTimeMillis());
        values.put(DatabaseConstants.IS_ACTIVE,1);
        String whereCondi = DatabaseConstants.IS_ACTIVE+"='0' and "+DatabaseConstants.KEY_DATE+"='"+date+"'";
        updateStockTriggerInfo(values,whereCondi);
    }

    public synchronized void checkTriggerInfo(Stocks stocks,StockInfo stockLevels,String date){

        /*//check todays levels
        StockInfo stockLevels = getStockLevels(stocks.getSymbol());*/
        if(stockLevels!=null){
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseConstants.KEY_LTP, Utility.getInstance().convertDouble(stocks.getLtP()));
            String where = DatabaseConstants.ID + "='" + stockLevels.getId() + "' and "+DatabaseConstants.KEY_DATE+"='"+date+"'";
            updateData(DatabaseConstants.TABLE_LIVE_DATA, contentValues, where);
            TriggerInfo triggerInfo = getTriggerInfo(stocks.getSymbol(),date);
            if(triggerInfo!=null && triggerInfo.getIsTslHit()==0) {
                String whereCondi = DatabaseConstants.ID+"='"+triggerInfo.getId()+"'";
                ContentValues values = new ContentValues();
                double ltp = Utility.getInstance().convertDouble(stocks.getLtP());
                values.put(DatabaseConstants.KEY_LTP,Utility.getInstance().convertDouble(stocks.getLtP()));
                values.put(UPDATED_TIME,System.currentTimeMillis());
                if(triggerInfo.getLow() > Utility.getInstance().convertDouble(stocks.getLtP())){
                    values.put(DatabaseConstants.KEY_LOW,stocks.getLtP());
                }
                if(triggerInfo.getHigh() < Utility.getInstance().convertDouble(stocks.getLtP())){
                    values.put(DatabaseConstants.KEY_HIGH,stocks.getLtP());
                }
                ///BUY TRIGGER
                if(triggerInfo.getSignal()==null ){
                    Log.d("NulRef",triggerInfo.getSymbol());
                }else if(triggerInfo.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    if(triggerInfo.getIsActive()==2){
                        if (ltp <= stockLevels.getBuySl()) {
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        }
                    }else {
                        if (triggerInfo.getSl()==0 && Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getBuySl()) {
                            //STOP LOSS HIT- close session
                            values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuySl());
                            values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuySl());
                            StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS HIT " + " @" + stockLevels.getBuySl(), stockLevels.getId());
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        }
                        if (triggerInfo.getT6() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget6()) {
                            //TARGET 6 hit-
                            if(triggerInfo.getT6() == 0) {
                                values.put(DatabaseConstants.KEY_IS_T6, stockLevels.getTarget6());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-6 Achieved " + " @" + stockLevels.getTarget6(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget4());
                            }else if(ltp < stockLevels.getTarget4()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget4());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget4());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget4(), stockLevels.getId());

                            }
                        }
                        if (triggerInfo.getT5() > 0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget5()) {
                            if(triggerInfo.getT5()==0) {
                                //TARGET 5 hit-
                                values.put(DatabaseConstants.KEY_IS_T5, stockLevels.getTarget5());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-5 Achieved " + " @" + stockLevels.getTarget5(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget3());
                            }else if(ltp < stockLevels.getTarget3()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget3());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget2());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget3(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT4()>0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget4()) {
                            if(triggerInfo.getT4()==0) {
                                //TARGET 4 hit-
                                values.put(DatabaseConstants.KEY_IS_T4, stockLevels.getTarget4());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-4 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget2());
                            }else if(ltp < stockLevels.getTarget2()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget2());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget2());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget2(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT3() >0 ||Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget3()) {
                            if(triggerInfo.getT3()==0) {
                                //TARGET 3 hit-
                                values.put(DatabaseConstants.KEY_IS_T3, stockLevels.getTarget3());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-3 Achieved " + " @" + stockLevels.getTarget3(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget1());
                            }else if(ltp < stockLevels.getTarget1()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getTarget1());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getTarget1());
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getTarget1(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT2() >0 || Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget2()) {
                            if(triggerInfo.getT2()==0) {
                                //TARGET 2 hit-
                                values.put(DatabaseConstants.KEY_IS_T2, stockLevels.getTarget2());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                            }else if(ltp < stockLevels.getBuyAt()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getBuyAt());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getBuyAt(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT1() == 0 && Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getTarget1()) {
                            //TARGET 1 hit-
                            values.put(DatabaseConstants.KEY_IS_T1, stockLevels.getTarget1());
                            StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-1 Achieved " + " @" + stockLevels.getTarget1(), stockLevels.getId());
                            values.put(DatabaseConstants.KEY_TSL, stockLevels.getBuyAt());
                        }
                    }
                }else if(triggerInfo.getSignal().equalsIgnoreCase(CONST_SELL)) {
                    if(triggerInfo.getIsActive()==2){
                        if (ltp >  stockLevels.getBuySl()) {
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                        }
                    }else {
                        if (triggerInfo.getSl()==0 && Utility.getInstance().convertDouble(stocks.getLtP()) >= stockLevels.getBuySl()) {
                            //STOP LOSS HIT- close session
                            values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellSl());
                            values.put(DatabaseConstants.IS_ACTIVE, 1);
                            StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS HIT " + " @" + stockLevels.getSellSl(), stockLevels.getId());
                        }
                        if (triggerInfo.getT6()> 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target6()) {
                            if(triggerInfo.getT6()==0) {
                                //TARGET 6 hit-
                                values.put(DatabaseConstants.KEY_IS_T6, stockLevels.getSell_target6());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getTarget4(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target4());
                            }else if(ltp > stockLevels.getSell_target4()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target4());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target4());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target4(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT5()> 0 ||Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target6()) {
                            if(triggerInfo.getT5()==0) {
                                //TARGET 5 hit-
                                values.put(DatabaseConstants.KEY_IS_T5, stockLevels.getSell_target5());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-5 Achieved " + " @" + stockLevels.getSell_target5(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target3());
                            }else if(ltp > stockLevels.getSell_target3()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target3());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target3());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target3(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT4()> 0 ||Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target4()) {
                            if(triggerInfo.getT4()==0) {
                                //TARGET 4 hit-
                                values.put(DatabaseConstants.KEY_IS_T4, stockLevels.getSell_target4());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-4 Achieved " + " @" + stockLevels.getSell_target4(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target2());
                            }else if(ltp > stockLevels.getSell_target2()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target2());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target2());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target2(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT3()> 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target3()) {
                            if(triggerInfo.getT3()==0) {
                                //TARGET 3 hit-
                                values.put(DatabaseConstants.KEY_IS_T3, stockLevels.getSell_target3());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-3 Achieved " + " @" + stockLevels.getSell_target3(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target1());
                            }else if(ltp > stockLevels.getSell_target1()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSell_target1());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSell_target1());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSell_target1(), stockLevels.getId());
                            }
                        }
                        if (triggerInfo.getT2()> 0 || Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target2()) {
                            if(triggerInfo.getT2()==0) {
                                //TARGET 2 hit-
                                values.put(DatabaseConstants.KEY_IS_T2, stockLevels.getSell_target2());
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-2 Achieved " + " @" + stockLevels.getSell_target2(), stockLevels.getId());
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSellAt());
                            }else if(ltp > stockLevels.getSellAt()){
                                values.put(DatabaseConstants.KEY_IS_SL, stockLevels.getSellAt());
                                values.put(DatabaseConstants.KEY_IS_TSL_HIT, 1);
                                values.put(DatabaseConstants.KEY_TSL, stockLevels.getSellAt());
                                values.put(DatabaseConstants.IS_ACTIVE, 2);
                                StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "STOP LOSS (TSL) HIT " + " @" + stockLevels.getSellAt(), stockLevels.getId());
                            }

                        }
                        if (triggerInfo.getT1() == 0 && Utility.getInstance().convertDouble(stocks.getLtP()) <= stockLevels.getSell_target1()) {
                            //TARGET 1 hit-
                            values.put(DatabaseConstants.KEY_IS_T1, stockLevels.getSell_target1());
                            StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), "Target-1 Achieved " + " @" + stockLevels.getSell_target1(), stockLevels.getId());
                        }
                    }
                }
                updateStockTriggerInfo(values,whereCondi);
            }else{
                //New Session will open
                ContentValues values = new ContentValues();
                boolean isvalidEntry = false;
                double ltp = Utility.getInstance().convertDouble(stocks.getLtP());
                String signal = "";
                double recPrice = Utility.getInstance().convertDouble(stocks.getLtP());
                if(ltp >= stockLevels.getBuyAt()){
                    isvalidEntry=true;
                    values.put(DatabaseConstants.KEY_SIGNAL,CONST_BUY);
                    recPrice = stockLevels.getBuyAt();
                    signal = CONST_BUY;
                }else if(ltp <= stockLevels.getSellAt()){
                    isvalidEntry=true;
                    recPrice = stockLevels.getSellAt();
                    values.put(DatabaseConstants.KEY_SIGNAL,CONST_SELL);
                    signal = CONST_SELL;
                }
                if(isvalidEntry){
                    values.put(DatabaseConstants.KEY_SCRIP,stocks.getSymbol());
                    values.put(DatabaseConstants.KEY_DATE,date);
                    values.put(DatabaseConstants.KEY_LTP,recPrice);
                    values.put(DatabaseConstants.KEY_RECORD_PRICE,recPrice);
                    values.put(DatabaseConstants.KEY_LOW,stocks.getLow());
                    values.put(DatabaseConstants.KEY_HIGH,stocks.getHigh());
                    values.put(DatabaseConstants.IS_ACTIVE,0);
                    values.put(UPDATED_TIME,System.currentTimeMillis());
                    values.put(DatabaseConstants.CREATED_TIME,System.currentTimeMillis());
                    insertData(DatabaseConstants.TABLE_TRIGGER_INFO,values);
                    StockerPermissionManager.getInstance().showUserAction(mContext, stocks.getSymbol(), signal+ " @" + stocks.getLtP(),stockLevels.getId());
                }


            }
        }


    }

    private void updateStockTriggerInfo(ContentValues values,String where) {
        if(updateData(DatabaseConstants.TABLE_TRIGGER_INFO,values,where)){
            Log.d("RefUpd","Record updated...");
        }else{
            Log.d("RefUpd","Record not updated...");
        }
    }

    public synchronized TriggerInfo getTriggerInfo(String symbol,String date) {
        TriggerInfo triggerInfo = null;
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=?" + " and "  +
                            DatabaseConstants.KEY_DATE + "=? and "+DatabaseConstants.IS_ACTIVE +"<> ?",
                    new String[] {symbol,date,String.valueOf(1)},
                    null, null, null , null);
            /*cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_SCRIP + "=?" + " and "  +
                            DatabaseConstants.IS_ACTIVE +"=?",
                    new String[] {symbol,String.valueOf(0)},
                    null, null, null , null);*/
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+DatabaseConstants.IS_ACTIVE+" = 0 order by created_date desc limit 1";
            cursor = db.rawQuery(query,null);*/
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+ DatabaseConstants.IS_ACTIVE+" == 0 or "+  DatabaseConstants.IS_ACTIVE+" == 1 order by id desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    triggerInfo = new TriggerInfo();
                    triggerInfo.setId(cursor.getInt(iId));
                    triggerInfo.setSymbol(cursor.getString(indexSymbol));
                    triggerInfo.setSignal(cursor.getString(iSignal));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setT1(cursor.getDouble(iT1));
                    triggerInfo.setT2(cursor.getDouble(iT2));
                    triggerInfo.setT3(cursor.getDouble(iT3));
                    triggerInfo.setT4(cursor.getDouble(iT4));
                    triggerInfo.setT5(cursor.getDouble(iT5));
                    triggerInfo.setT6(cursor.getDouble(iT6));
                    triggerInfo.setSl(cursor.getDouble(iSl));
                    triggerInfo.setTsl(cursor.getDouble(iTSL));
                    triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                    triggerInfo.setIsActive(cursor.getInt(iActive));

                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerInfo;
    }

    public synchronized List<TriggerInfo> getTriggerList() {
        List<TriggerInfo> triggerList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_DATE+"='"+Utility.getInstance().formatDate()+"'",
                    null,
                    null, null, CREATED_TIME+" desc" , null);
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+DatabaseConstants.IS_ACTIVE+" = 0 order by created_date desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    TriggerInfo triggerInfo = new TriggerInfo();
                    triggerInfo.setId(cursor.getInt(iId));
                    triggerInfo.setSymbol(cursor.getString(indexSymbol));
                    triggerInfo.setSignal(cursor.getString(iSignal));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setT1(cursor.getDouble(iT1));
                    triggerInfo.setT2(cursor.getDouble(iT2));
                    triggerInfo.setT3(cursor.getDouble(iT3));
                    triggerInfo.setT4(cursor.getDouble(iT4));
                    triggerInfo.setT5(cursor.getDouble(iT5));
                    triggerInfo.setT6(cursor.getDouble(iT6));
                    triggerInfo.setSl(cursor.getDouble(iSl));
                    triggerInfo.setIsActive(cursor.getInt(iActive));
                    triggerInfo.setUpdateTime(cursor.getString(iUpdateTime));
                    triggerInfo.setTsl(cursor.getDouble(iTSL));
                    triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                    triggerList.add(triggerInfo);

                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerList;
    }
    public synchronized List<TriggerInfo> getTriggerList(String symbol) {
        List<TriggerInfo> triggerList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_TRIGGER_INFO, null,
                    DatabaseConstants.KEY_SCRIP+"='"+symbol+"' and "+DatabaseConstants.KEY_DATE+"='"+Utility.getInstance().formatDate()+"'",
                    null,
                    null, null, CREATED_TIME+" desc" , null);
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+DatabaseConstants.IS_ACTIVE+" = 0 order by created_date desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iSignal = cursor.getColumnIndex(DatabaseConstants.KEY_SIGNAL);
                int iRecPrice = cursor.getColumnIndex(DatabaseConstants.KEY_RECORD_PRICE);
                int iT1 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T1);
                int iT2 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T2);
                int iT3 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T3);
                int iT4 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T4);
                int iT5 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T5);
                int iT6 = cursor.getColumnIndex(DatabaseConstants.KEY_IS_T6);
                int iSl = cursor.getColumnIndex(DatabaseConstants.KEY_IS_SL);
                int iActive = cursor.getColumnIndex(DatabaseConstants.IS_ACTIVE);
                int iUpdateTime = cursor.getColumnIndex(UPDATED_TIME);
                int iTSL = cursor.getColumnIndex(DatabaseConstants.KEY_TSL);
                int iTSLHIT = cursor.getColumnIndex(DatabaseConstants.KEY_IS_TSL_HIT);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    TriggerInfo triggerInfo = new TriggerInfo();
                    triggerInfo.setId(cursor.getInt(iId));
                    triggerInfo.setSymbol(cursor.getString(indexSymbol));
                    triggerInfo.setSignal(cursor.getString(iSignal));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setRecPrice(cursor.getDouble(iRecPrice));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setT1(cursor.getDouble(iT1));
                    triggerInfo.setT2(cursor.getDouble(iT2));
                    triggerInfo.setT3(cursor.getDouble(iT3));
                    triggerInfo.setT4(cursor.getDouble(iT4));
                    triggerInfo.setT5(cursor.getDouble(iT5));
                    triggerInfo.setT6(cursor.getDouble(iT6));
                    triggerInfo.setSl(cursor.getDouble(iSl));
                    triggerInfo.setIsActive(cursor.getInt(iActive));
                    triggerInfo.setUpdateTime(cursor.getString(iUpdateTime));
                    triggerInfo.setTsl(cursor.getDouble(iTSL));
                    triggerInfo.setIsTslHit(cursor.getInt(iTSLHIT));
                    triggerList.add(triggerInfo);

                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerList;
    }

    public synchronized List<String> getStockList() {
        List<String> scriptList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_LIVE_DATA, null,
                    null,
                    null,
                    DatabaseConstants.KEY_SCRIP, null, DatabaseConstants.KEY_SCRIP+" desc" , null);
            /*String query = "select * from '" + DatabaseConstants.TABLE_TRIGGER_INFO + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' and "+DatabaseConstants.IS_ACTIVE+" = 0 order by created_date desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iId = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);

                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    scriptList.add(cursor.getString(indexSymbol));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return scriptList;
    }

    public synchronized StockInfo getStockLevels(String symbol,String date) {
        StockInfo triggerInfo = null;
        Cursor cursor = null;
        try {
            cursor = db.query(true, DatabaseConstants.TABLE_LIVE_DATA, null,
                    DatabaseConstants.KEY_SCRIP + "=?" + " and "  +
                            DatabaseConstants.KEY_DATE + "=?",
                    new String[] {symbol,date},
                    null, null, null , null);

            /*String query = "select * from '" + DatabaseConstants.TABLE_LIVE_DATA + "' where "+DatabaseConstants.KEY_SCRIP+"='" + symbol + "' order by id desc limit 1";

            cursor = db.rawQuery(query,null);*/
            if (cursor.getCount() > 0) {
                int iID = cursor.getColumnIndex(DatabaseConstants.ID);
                int indexSymbol = cursor.getColumnIndex(DatabaseConstants.KEY_SCRIP);
                int iLtp = cursor.getColumnIndex(DatabaseConstants.KEY_LTP);
                int iOpen = cursor.getColumnIndex(DatabaseConstants.KEY_OPEN);
                int iHigh = cursor.getColumnIndex(DatabaseConstants.KEY_HIGH);
                int iLow = cursor.getColumnIndex(DatabaseConstants.KEY_LOW);
                int iPClose = cursor.getColumnIndex(DatabaseConstants.KEY_PRE_CLOSE);
                int iBuySL = cursor.getColumnIndex(DatabaseConstants.KEY_BUY_STOP_LOSS);
                int iBuyAt = cursor.getColumnIndex(DatabaseConstants.KEY_BUY_AT);
                int iBT1 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_1);
                int iBT2 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_2);
                int iBT3 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_3);
                int iBT4 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_4);
                int iBT5 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_5);
                int iBT6 = cursor.getColumnIndex(DatabaseConstants.KEY_TARGET_6);
                int iSellSL = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_STOP_LOSS);
                int iSellAt = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_AT);
                int iSellT1 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_1);
                int iSellT2 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_2);
                int iSellT3 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_3);
                int iSellT4 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_4);
                int iSellT5 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_5);
                int iSellT6 = cursor.getColumnIndex(DatabaseConstants.KEY_SELL_TARGET_6);


                /*fetch last consumed data*/
                while (cursor.moveToNext()) {
                    triggerInfo = new StockInfo();
                    triggerInfo.setId(cursor.getInt(iID));
                    triggerInfo.setScriptName(cursor.getString(indexSymbol));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setOpen(cursor.getDouble(iOpen));
                    triggerInfo.setHigh(cursor.getDouble(iHigh));
                    triggerInfo.setLow(cursor.getDouble(iLow));
                    triggerInfo.setLtp(cursor.getDouble(iLtp));
                    triggerInfo.setPreviousClose(cursor.getDouble(iPClose));
                    triggerInfo.setBuySl(cursor.getDouble(iBuySL));
                    triggerInfo.setBuyAt(cursor.getDouble(iBuyAt));
                    triggerInfo.setTarget1(cursor.getDouble(iBT1));
                    triggerInfo.setTarget2(cursor.getDouble(iBT2));
                    triggerInfo.setTarget3(cursor.getDouble(iBT3));
                    triggerInfo.setTarget4(cursor.getDouble(iBT4));
                    triggerInfo.setTarget5(cursor.getDouble(iBT5));
                    triggerInfo.setTarget6(cursor.getDouble(iBT6));
                    triggerInfo.setSellSl(cursor.getDouble(iSellSL));
                    triggerInfo.setSellAt(cursor.getDouble(iSellAt));
                    triggerInfo.setSell_target1(cursor.getDouble(iSellT1));
                    triggerInfo.setSell_target2(cursor.getDouble(iSellT2));
                    triggerInfo.setSell_target3(cursor.getDouble(iSellT3));
                    triggerInfo.setSell_target4(cursor.getDouble(iSellT4));
                    triggerInfo.setSell_target5(cursor.getDouble(iSellT5));
                    triggerInfo.setSell_target6(cursor.getDouble(iSellT6));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in getTriggerinfo() : " + e.getLocalizedMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return triggerInfo;
    }

    public void dumpLiveData(Stocks stockModel,String date){
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_SCRIP,stockModel.getSymbol());
        values.put(DatabaseConstants.KEY_DATE, date);

        values.put(DatabaseConstants.KEY_OPEN,Utility.getInstance().convertDouble(stockModel.getOpen()));
        values.put(DatabaseConstants.KEY_LTP,Utility.getInstance().convertDouble(stockModel.getLtP()));
        //values.put(DatabaseConstants.KEY_HIGH,Utility.getInstance().convertDouble(stockModel.getHigh()));
        //values.put(DatabaseConstants.KEY_LOW,Utility.getInstance().convertDouble(stockModel.getLow()));
        //values.put(DatabaseConstants.KEY_PRE_CLOSE,Utility.getInstance().convertDouble(stockModel.getPreviousClose()));
        double openPrice = Utility.getInstance().convertDouble(stockModel.getOpen());
        /**
         * Generating Triggers
         * */

        values.put(DatabaseConstants.KEY_BUY_STOP_LOSS,openPrice);
        values.put(DatabaseConstants.KEY_BUY_AT,calculateBuyValue(openPrice,getBuyFormula(openPrice)));
        values.put(DatabaseConstants.KEY_TARGET_1,calculateBuyValue(openPrice,FM_T1));
        values.put(DatabaseConstants.KEY_TARGET_2,calculateBuyValue(openPrice,FM_T2));
        values.put(DatabaseConstants.KEY_TARGET_3,calculateBuyValue(openPrice,FM_T3));
        values.put(DatabaseConstants.KEY_TARGET_4,calculateBuyValue(openPrice,FM_T4));
        values.put(DatabaseConstants.KEY_TARGET_5,calculateBuyValue(openPrice,FM_T5));
        values.put(DatabaseConstants.KEY_TARGET_6,calculateBuyValue(openPrice,FM_T6));

        values.put(DatabaseConstants.KEY_SELL_AT,calculateSellValue(openPrice,getBuyFormula(openPrice)));
        values.put(DatabaseConstants.KEY_SELL_TARGET_1,calculateSellValue(openPrice,FM_T1));
        values.put(DatabaseConstants.KEY_SELL_TARGET_2,calculateSellValue(openPrice,FM_T2));
        values.put(DatabaseConstants.KEY_SELL_TARGET_3,calculateSellValue(openPrice,FM_T3));
        values.put(DatabaseConstants.KEY_SELL_TARGET_4,calculateSellValue(openPrice,FM_T4));
        values.put(DatabaseConstants.KEY_SELL_TARGET_5,calculateSellValue(openPrice,FM_T5));
        values.put(DatabaseConstants.KEY_SELL_TARGET_6,calculateSellValue(openPrice,FM_T6));
        values.put(DatabaseConstants.KEY_SELL_STOP_LOSS,openPrice);


        long affected = insertData(DatabaseConstants.TABLE_LIVE_DATA,values);
        Log.d("Dbadp",values.toString()+affected);
    }

    private double getBuyFormula(double openPrice){
        double triggerFormula = 0.125;
        if(openPrice > 15000){
            triggerFormula = 0.0625;
        }
        return  triggerFormula;
    }

    private double calculateBuyValue(double openPrice,double formula){
        double sqrt = Math.sqrt(openPrice);
        double output = sqrt + formula;
        return Utility.getInstance().convertTwoDecimal((output * output));
    }

    private double calculateSellValue(double openPrice,double formula){
        //return ((Math.sqrt(openPrice) - getBuyFormula(openPrice)) * 2);
        double sqrt = Math.sqrt(openPrice);
        double output = sqrt - formula;
        return Utility.getInstance().convertTwoDecimal(output * output);
    }


    public void clearOldData(){
        int recordCount = getCursorCount("historical_data",null);
        if(recordCount>15000){
            db.execSQL("delete from historical_data where id < ( select id from historical_data order by id limit 3000,1)");
        }
        int recordCount2 = getCursorCount("five_min_data",null);
        if(recordCount2>15000){
            db.execSQL("delete from five_min_data where id < ( select id from five_min_data order by id limit 3000,1)");
        }
    }

    public int getUniqueNumber(String symbol) {
        int uniqueAppNo= 0;
        Cursor cursor = null;
        try {
            String query = "select id from historical_data where symbol='"+symbol+"' limit 1";
            cursor = db.rawQuery(query, null);
            int artIndex = cursor.getColumnIndex("id");
            if (cursor.getCount() > 0) {
                if (cursor.moveToNext()) {
                    uniqueAppNo = cursor.getInt(artIndex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return uniqueAppNo;
    }



    private class DatabaseHelperBackup extends SQLiteOpenHelper {

        public DatabaseHelperBackup(Context context) {
            super(context, DATABASENAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DatabaseConstants.CREATE_TABLE_LIVE_DATA);
                db.execSQL(DatabaseConstants.CREATE_TABLE_TRIGGER_INFO);

            } catch (Exception e) {
                Log.e(TAG, "Error in DBAdapter : OnCreate() : " + e.getLocalizedMessage());
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
            if(oldVersion==1){
                try {
                    db.execSQL("ALTER TABLE " + DatabaseConstants.TABLE_TRIGGER_INFO + " ADD " + DatabaseConstants.KEY_TSL + " REAL");
                    db.execSQL("ALTER TABLE " + DatabaseConstants.TABLE_TRIGGER_INFO + " ADD " + DatabaseConstants.KEY_IS_TSL_HIT + " int not null default 0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    db.execSQL("ALTER TABLE " + DatabaseConstants.TABLE_LIVE_DATA + " ADD " + DatabaseConstants.KEY_LTP + " REAL");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
