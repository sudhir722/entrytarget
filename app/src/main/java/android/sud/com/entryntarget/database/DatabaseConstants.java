package android.sud.com.entryntarget.database;

public class DatabaseConstants {


    public static final String TABLE_LIVE_DATA = "live_data";
    public static final String TABLE_TRIGGER_INFO = "trigger_info";
    public static final String TABLE_FAV_SYMBOLS = "fav_stock_info";
    public static final String TABLE_ORDER_INFO = "order_info";
    public static final String TABLE_ACTIVITY_LOGS = "activity_logs";


    /* Constant variables */
    public static final String ID = "id";
    public static final String KEY_SCRIP = "script";
    public static final String KEY_SIGNAL = "signal";
    public static final String KEY_DATE = "date";
    public static final String KEY_LTP = "ltp";
    public static final String KEY_RECORD_PRICE = "entry";
    public static final String KEY_OPEN = "open";
    public static final String KEY_HIGH = "high";
    public static final String KEY_LOW = "low";
    public static final String KEY_PRE_CLOSE = "pre_close";
    public static final String KEY_BUY_STOP_LOSS = "buy_stoploss";
    public static final String KEY_BUY_AT = "buy_at";
    public static final String KEY_TARGET_1 = "t1";
    public static final String KEY_TARGET_2 = "t2";
    public static final String KEY_TARGET_3 = "t3";
    public static final String KEY_TARGET_4 = "t4";
    public static final String KEY_TARGET_5 = "t5";
    public static final String KEY_TARGET_6 = "t6";

    public static final String KEY_SELL_STOP_LOSS = "sell_stoploss";
    public static final String KEY_SELL_AT = "sell_at";
    public static final String KEY_SELL_TARGET_1 = "sell_t1";
    public static final String KEY_SELL_TARGET_2 = "sell_t2";
    public static final String KEY_SELL_TARGET_3 = "sell_t3";
    public static final String KEY_SELL_TARGET_4 = "sell_t4";
    public static final String KEY_SELL_TARGET_5 = "sell_t5";
    public static final String KEY_SELL_TARGET_6 = "sell_t6";
    public static final String UPDATED_TIME = "updated_time";
    public static final String CREATED_TIME = "created_time";
    public static final String IS_ACTIVE = "is_active";
    public static final String KEY_COMMENTS = "txt_comments";
    public static final String KEY_IS_SL = "sl";
    public static final String KEY_CHANGE = "change_pl";
    public static final String KEY_TSL = "tsl";
    public static final String KEY_IS_TSL_HIT = "tsl_hit";
    public static final String KEY_EXIS_PRICE = "exit_price";
    public static final String KEY_IS_T1 = "t1";
    public static final String KEY_IS_T2 = "t2";
    public static final String KEY_IS_T3 = "t3";
    public static final String KEY_IS_T4 = "t4";
    public static final String KEY_IS_T5 = "t5";
    public static final String KEY_IS_T6 = "t6";


    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_MODIFY_ORDER_ID = "modi_order_id";
    public static final String KEY_ORDER_STATUS = "order_status";

    public static final String KEY_SL_SET = "is_sl_set";

    /**
     * m  market
     * l  limit
     * */
    public static final String KEY_ORDER_TYPE = "order_type";
    /*public static final String KEY_ORDER_TRIGGER = "trigger_price";*/
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATETIME = "datetime";
    public static final String KEY_TIMESTAMP = "timestamp";



    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_MAX_TRIGGERS = "max_triggers";


    public static final String CREATE_TABLE_LIVE_DATA = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LIVE_DATA
            + "( " + ID + " integer primary key autoincrement, "
            + " " + KEY_SCRIP + " text, "
            + " " + KEY_DATE + " text, "
            + " " + KEY_OPEN+ " REAL, "
            + " " + KEY_LTP+ " REAL, "
            + " " + KEY_HIGH + "  REAL, "
            + " " + KEY_LOW + "  REAL, "
            + " " + KEY_PRE_CLOSE + "  REAL, "
            + " " + KEY_BUY_AT + "  REAL, "
            + " " + KEY_BUY_STOP_LOSS + "  REAL, "
            + " " + KEY_TARGET_1 + "  REAL, "
            + " " + KEY_TARGET_2 + "  REAL, "
            + " " + KEY_TARGET_3 + "  REAL, "
            + " " + KEY_TARGET_4 + "  REAL, "
            + " " + KEY_TARGET_5 + "  REAL, "
            + " " + KEY_TARGET_6 + "  REAL, "
            + " " + KEY_SELL_AT + "  REAL, "
            + " " + KEY_SELL_STOP_LOSS + "  REAL, "
            + " " + KEY_SELL_TARGET_1 + "  REAL, "
            + " " + KEY_SELL_TARGET_2 + "  REAL, "
            + " " + KEY_SELL_TARGET_3 + "  REAL, "
            + " " + KEY_SELL_TARGET_4 + "  REAL, "
            + " " + KEY_SELL_TARGET_5 + "  REAL, "
            + " " + KEY_SELL_TARGET_6 + "  REAL, "

            + " " + UPDATED_TIME + " text not null default false " + ");";


    public static final String CREATE_TABLE_TRIGGER_INFO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TRIGGER_INFO
            + "( " + ID + " integer primary key autoincrement, "
            + " " + KEY_DATE + " text, "
            + " " + KEY_SCRIP + " text, "
            + " " + KEY_ORDER_ID + " text, "
            + " " + KEY_MODIFY_ORDER_ID + " text, "
            + " " + KEY_LTP + " REAL, "
            + " " + KEY_SIGNAL+ " text, "
            + " " + KEY_RECORD_PRICE + "  REAL, "
            + " " + KEY_LOW + "  REAL, "
            + " " + KEY_HIGH + "  REAL, "
            + " " + KEY_IS_T1 + "  text, "
            + " " + KEY_IS_T2 + "  text, "
            + " " + KEY_IS_T3 + "  text, "
            + " " + KEY_IS_T4 + "  text, "
            + " " + KEY_IS_T5 + "  text, "
            + " " + KEY_IS_T6 + "  text, "
            + " " + KEY_IS_SL + "  text, "
            + " " + KEY_TSL + "  REAL, "
            + " " + KEY_EXIS_PRICE + "  REAL, "
            + " " + KEY_IS_TSL_HIT + "  int not null default 0,"
            + " " + KEY_CHANGE + "  text, "
            + " " + KEY_STATUS + "  int, "
            + " " + IS_ACTIVE + "  int not null default 0, "
            + " " + CREATED_TIME + "  text not null default 0, "
            + " " + UPDATED_TIME + " text not null default false " + ");";


    public static final String CREATE_TABLE_FAV = "CREATE TABLE IF NOT EXISTS "
            + TABLE_FAV_SYMBOLS
            + "( " + ID + " integer primary key autoincrement, "
            + " " + KEY_SCRIP + " text, "
            + " " + KEY_QUANTITY + " int, "
            + " " + KEY_MAX_TRIGGERS + " int, "
            + " " + CREATED_TIME + " long  " + ");";


    public static final String CREATE_TABLE_ORDER_INFO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ORDER_INFO
            + "( " + ID + " integer primary key autoincrement, "
            + " " + KEY_SCRIP + " text, "
            + " " + KEY_QUANTITY + " int, "
            + " " + KEY_SIGNAL + " text, "
            + " " + KEY_ORDER_ID + " text, "
            + " " + KEY_ORDER_STATUS + " text, "
            + " " + KEY_ORDER_TYPE + " text, "
            + " " + KEY_RECORD_PRICE + " real, "
            + " " + KEY_EXIS_PRICE + " real, "
            + " " + KEY_CHANGE + " real, "
            + " " + KEY_STATUS + " int, "
            + " " + KEY_DATETIME + " text, "
            + " " + KEY_SL_SET + " int, "
            + " " + KEY_TIMESTAMP + " long, "
            + " " + CREATED_TIME + " long  " + ");";

    public static final String CREATE_TABLE_ACTIVITY_LOGS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ACTIVITY_LOGS
            + "( " + ID + " integer primary key autoincrement, "
            + " " + KEY_SCRIP + " text, "
            + " " + KEY_SIGNAL + " text, "
            + " " + KEY_STATUS + " text, "
            + " " + KEY_DATETIME + " text, "
            + " " + CREATED_TIME + " long  " + ");";

    public static String[] filteredSymbols = new String[]{"YESBANK","RELIANCE","HDFCBANK","HDFC","INFY","SBIN","KOTAKBANK","AXISBANK","SUNPHARMA","INDUSINDBK","BAJAJFINSV","BAJAJ-AUTO","TITAN","HEROMOTOCO","DRREDDY","LUPIN"};

}
