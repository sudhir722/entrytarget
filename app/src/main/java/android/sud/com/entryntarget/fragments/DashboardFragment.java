package android.sud.com.entryntarget.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.database.DatabaseConstants;
import android.sud.com.entryntarget.fragments.interfaces.OnFragmentInteractionListener;
import android.sud.com.entryntarget.models.StockInfo;
import android.sud.com.entryntarget.models.TriggerInfo;
import android.sud.com.entryntarget.ui.adapter.TriggerNewRecycleAdapter;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends BaseFragment implements  SwipeRefreshLayout.OnRefreshListener,View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    RecyclerView recyclerView;
    private List<TriggerInfo> stockList = new ArrayList<>();
    private TriggerNewRecycleAdapter mAdapter;
    private RelativeLayout blank;
    private AutoCompleteTextView autoSearch;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayAdapter<String> searchAdapter;
    String[] stockNameList = null;
    ImageView actionSearch;

    MyTextView actionAll,actionFav,actionIndex,actionStocks;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);//every item of the RecyclerView has a fix size
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        blank = (RelativeLayout) view.findViewById(R.id.blank);
        actionSearch = (ImageView) view.findViewById(R.id.action_signin);
        actionSearch.setOnClickListener(this);
        autoSearch = (AutoCompleteTextView) view.findViewById(R.id.searchView1);
        initAutoSearch();

        //EntPreferences.setUpstoxKeyOrderType(getActivity(),"oco");
        actionAll = (MyTextView) view.findViewById(R.id.action_all);
        actionFav = (MyTextView) view.findViewById(R.id.action_favourites);
        actionIndex = (MyTextView) view.findViewById(R.id.action_index);
        actionStocks = (MyTextView) view.findViewById(R.id.action_stocks);

        actionAll.setOnClickListener(this);
        actionFav.setOnClickListener(this);
        actionIndex.setOnClickListener(this);
        actionStocks.setOnClickListener(this);

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        //addOCOOrders();
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                refreshData();
                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        return  view;
    }

    public void refreshSearchData(final String symbol){
        StockInfo levels = DBAdapter.getDBAdapter(getActivity()).getStockLevels(symbol);
        if(levels==null){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("Do you want to add "+symbol.toUpperCase()+" in favorite");
            alertDialog.setPositiveButton("Add in Favorite", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    insertFavSymbol(symbol);
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            alertDialog.create();
            alertDialog.show();
        }else {
            List<TriggerInfo> mList = DBAdapter.getDBAdapter(getActivity()).getTriggerList(symbol);
            if (mList != null && mList.size() > 0) {
                stockList.clear();
                stockList.addAll(mList);
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
                blank.setVisibility(View.GONE);
                actionSearch.setImageResource(R.drawable.ic_exit);
            } else {
                blank.setVisibility(View.VISIBLE);
                actionSearch.setImageResource(R.drawable.ic_search_small);
            }
            if (stockList.size() == 0) {
                Toast.makeText(getActivity(), "Trigger information are not found, Please try again latar", Toast.LENGTH_LONG).show();
            } else {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public void refreshData(String action){
        List<TriggerInfo> mList = new ArrayList<>();
        mList = DBAdapter.getDBAdapter(getActivity()).getTriggerList(action);
        if(mList!=null && mList.size()>0){
            stockList.clear();
            stockList.addAll(mList);
            if(mAdapter!=null) {
                mAdapter.notifyDataSetChanged();
            }
            blank.setVisibility(View.GONE);
            //layoutActions.setVisibility(View.VISIBLE);
        }else{
            blank.setVisibility(View.VISIBLE);
            //layoutActions.setVisibility(View.GONE);
        }
        hideSearchBox(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        String[] list = DBAdapter.getDBAdapter(getActivity()).getFavList();
        if(list==null || list.length==0){
            showFavListDialog();
        }
    }

    private void insertFavSymbol(String symbol){
        boolean isAdded = DBAdapter.getDBAdapter(getActivity()).isAddedInFav(symbol);
        if(!isAdded) {
            ContentValues values = new ContentValues();
            values.put(DatabaseConstants.KEY_SCRIP, symbol);
            values.put(DatabaseConstants.CREATED_TIME, System.currentTimeMillis());
            if (!DBAdapter.getDBAdapter(getActivity()).updateData(DatabaseConstants.TABLE_FAV_SYMBOLS, values, DatabaseConstants.KEY_SCRIP + "='" + symbol + "'")) {
                DBAdapter.getDBAdapter(getActivity()).insertData(DatabaseConstants.TABLE_FAV_SYMBOLS, values);
            }
            Toast.makeText(getActivity(),"Stock added in your list",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getActivity(),"Stock already added in your list",Toast.LENGTH_LONG).show();
        }
    }

    public void refreshData(){
        prepareStockData();
        if(stockList.size()==0){
            Toast.makeText(getActivity(),"Trigger information are not found, Please try again latar",Toast.LENGTH_LONG).show();
        }else {
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
        hideSearchBox(true);
    }

    private synchronized void initAutoSearch(){
        if(stockNameList==null){
            stockNameList = getResources().getStringArray(R.array.stocklist);
        }
            //stockNameList = DBAdapter.getDBAdapter(getActivity()).getStockList();
        if(stockNameList!=null){
            searchAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, stockNameList);
            autoSearch.setAdapter(searchAdapter);
            autoSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    refreshSearchData(searchAdapter.getItem(position).toString());
                    hideSearchBox(true);
                }
            });
        }else{

        }


    }




    private synchronized void prepareStockData() {
        String action = EntPreferences.getDashboardAction(getActivity());
        List<TriggerInfo> mList = DBAdapter.getDBAdapter(getActivity()).getTriggerList(action);
        if(mList!=null && mList.size()>0){
            stockList.clear();
            stockList.addAll(mList);
            if(mAdapter!=null) {
                mAdapter.notifyDataSetChanged();
            }
            blank.setVisibility(View.GONE);
        }else{
            blank.setVisibility(View.VISIBLE);
        }
        hideSearchBox(true);
    }

    private void initListView(){
        mAdapter = new TriggerNewRecycleAdapter(getActivity(),stockList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStockData();
        initListView();


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        // Fetching data from server
        autoSearch.setText("");
        refreshData();
        // Stopping swipe refresh
        mSwipeRefreshLayout.setRefreshing(false);
        hideSearchBox(true);
    }

    private void hideSearchBox(boolean flag){
        if(flag){
            autoSearch.setVisibility(View.GONE);
            actionSearch.setImageResource(R.drawable.ic_search_small);
        }else{
            autoSearch.setVisibility(View.VISIBLE);
            actionSearch.setImageResource(R.drawable.ic_cross);
        }
    }

    private void showFavListDialog(){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose an Fav List");
        // add a list
        String[] animals = {"All", "Nikhil's favourite stocks", "Sudhirs favourite stocks"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // all stocks
                        String[] stockNameList = getResources().getStringArray(R.array.stocklist);
                        insertFavlist(stockNameList);
                        break;
                    case 1: // nikhils
                        addNikhilsFavList();
                    case 2: // sudhir
                        addSudhirFavList();
                    case 3: // sheep
                    case 4: // goat
                }
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /*private void addOCOOrders(){
        String[] list=new String[]{"BALKRISIND","TITAN","M&MFIN","JSWSTEEL","HEROMOTOCO"};
        DBAdapter.getDBAdapter(getActivity()).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        insertFavlist(list);
    }*/


    private void addSudhirFavList(){
        String[] favList = new String[]{
                "WABCOINDIA",
                "biocon",
                "escorts",
                "DIVISLAB",
                "bharatfin",
                "cholafin",
                "pel",
                "CENTURYTEX",
                "M&M",
                "srf",
                "indusindbk",
                "heromotoco",
                "maruti"
            };
        DBAdapter.getDBAdapter(getActivity()).clearData(DatabaseConstants.TABLE_FAV_SYMBOLS);
        insertFavlist(favList);
    }

    private void addNikhilsFavList(){
        String[] favList = new String[]{
                "BAJAJFINSV",
                "OFSS",
                "HINDUNILVR",
                "ULTRACEMCO",
                "HEROMOTOCO",
                "BAJAJ-AUTO",
                "DRREDDY",
                "TCS",
                "HDFCBANK",
                "HDFC",
                "INDUSINDBK",
                "KOTAKBANK",
                "IBULHSGFIN",
                "LUPIN",
                "TITAN",
                "INFY",
                "SUNPHARMA",
                "TATASTEEL",
                "AXISBANK",
                "RBLBANK",
                "LICHSGFIN",
                "M&MFIN",
                "YESBANK",
                "SRTRANSFIN",
                "BRITANNIA"
        };
        insertFavlist(favList);
    }

    private void insertFavlist(String[] favList){
        for (int index=0;index<favList.length;index++){
            DBAdapter.getDBAdapter(getActivity()).insertFavSymbol(favList[index]);
        }
    }
    @Override
    public void onClick(View v) {
        if(v.getId()==actionSearch.getId()){
            autoSearch.setText("");
            if(autoSearch.getVisibility()==View.VISIBLE){
                hideSearchBox(true);
                refreshData();
            }else{
                //autoSearch.setVisibility(View.VISIBLE);
                //actionSearch.setImageResource(R.drawable.ic_exit);
                autoSearch.requestFocus();
                hideSearchBox(false);
            }
        }else if(v.getId() == actionAll.getId()){
            refreshData("ALL");
            setTextSelection();
            EntPreferences.setDashboardAction(getActivity(),"ALL");
            actionAll.setTextColor(getResources().getColor(R.color.color_red));
        }else if(v.getId() == actionFav.getId()){
            refreshData("FAV");
            setTextSelection();
            EntPreferences.setDashboardAction(getActivity(),"FAV");
            actionFav.setTextColor(getResources().getColor(R.color.color_red));
        }else if(v.getId() == actionIndex.getId()){
            refreshData("INDEX");
            setTextSelection();
            EntPreferences.setDashboardAction(getActivity(),"INDEX");
            actionIndex.setTextColor(getResources().getColor(R.color.color_red));
        }else if(v.getId() == actionStocks.getId()){
            refreshData("STOCK");
            setTextSelection();
            EntPreferences.setDashboardAction(getActivity(),"STOCK");
            actionStocks.setTextColor(getResources().getColor(R.color.color_red));
        }
    }

    private void setTextSelection(){
        actionAll.setTextColor(getResources().getColor(R.color.white));
        actionFav.setTextColor(getResources().getColor(R.color.white));
        actionIndex.setTextColor(getResources().getColor(R.color.white));
        actionStocks.setTextColor(getResources().getColor(R.color.white));
    }
}
