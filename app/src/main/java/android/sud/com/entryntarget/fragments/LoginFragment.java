package android.sud.com.entryntarget.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.sud.com.entryntarget.fragments.interfaces.OnFragmentInteractionListener;
import android.sud.com.entryntarget.ui.fonts.MyEditText;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.sud.com.entryntarget.R;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static android.sud.com.entryntarget.utility.EntConstants.ACTION_LOGIN_SUCCESS;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static final String TAG = "LoginFragment";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private FirebaseAuth mAuth;
    MyEditText edtUserName;
    MyEditText edtUserPass;
    MyTextView actionLoginIn;
    MyTextView actionSignUp;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser()!=null){
            if(mListener!=null) {
                mListener.onFragmentInteractionChange(ACTION_LOGIN_SUCCESS);
                hideDialog();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        edtUserName = (MyEditText) view.findViewById(R.id.username);
        edtUserPass = (MyEditText) view.findViewById(R.id.password);
        actionLoginIn = (MyTextView) view.findViewById(R.id.signin);
        actionSignUp = (MyTextView) view.findViewById(R.id.action_signup);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        actionLoginIn.setOnClickListener(this);
        actionSignUp.setOnClickListener(this);
    }

    public boolean isValidForm(String email, String password) {

        if (TextUtils.isEmpty(email)) {
            showErrorAlert("Invalid Input", "Please enter email address");
            edtUserPass.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showErrorAlert("Invalid Input", "Please enter valid email address");
            edtUserPass.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            showErrorAlert("Invalid Input", "Please enter your password");
            edtUserPass.requestFocus();
        } else if (password.length() < 3) {
            showErrorAlert("Invalid Input", "At least 3 chars are required in your password");
            edtUserPass.requestFocus();
        } else if (password.length() > 9) {
            showErrorAlert("Invalid Input", "Password not more than 9 charactors");
            edtUserPass.requestFocus();
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==actionSignUp.getId()){

        }else if(v.getId() == actionLoginIn.getId()){
            String email = edtUserName.getText().toString().trim();
            String password = edtUserPass.getText().toString().trim();
            if(isValidForm(email,password)) {
                showProgressDialog("Please wait...");
                signIn(email,password);
            }
        }
    }


    private void signIn(String email,String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            showErrorAlert("Error","Invalid username and password, Please check your email id and password.");
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if(user!=null){
            //hideDialog();
            if(mListener!=null) {
                mListener.onFragmentInteractionChange(ACTION_LOGIN_SUCCESS);
                hideDialog();
            }
        }else{
            hideDialog();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
