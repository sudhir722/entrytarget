package android.sud.com.entryntarget.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.ui.fonts.MyEditText;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.TokenRequest;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.iface.OnTokenFound;
import android.sud.com.entryntarget.upstockService.login.LoginService;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.utils.Utility;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class UpstoxConnectionFragment extends AppCompatActivity implements View.OnClickListener{

    private MyEditText edtApiKey,edtApiSecret,edtToken,edtCode;
    private MyTextView actionGenerateToken,actionSubmit,actionReset;
    CheckBox chkOrderType;

    ProgressDialog mProgressBar = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upstox_setup_layout);
        edtApiKey = (MyEditText) findViewById(R.id.upstox_apikey);
        edtApiSecret = (MyEditText) findViewById(R.id.upstox_api_secret);
        edtCode = (MyEditText) findViewById(R.id.upstox_code);
        edtToken = (MyEditText) findViewById(R.id.upstox_api_token);
        actionGenerateToken = (MyTextView) findViewById(R.id.action_generatetokenn);
        actionSubmit = (MyTextView) findViewById(R.id.action_submit);
        chkOrderType = (CheckBox) findViewById(R.id.upstox_order_type);
        actionGenerateToken.setOnClickListener(this);
        actionSubmit.setOnClickListener(this);
        actionReset = (MyTextView) findViewById(R.id.action_reset);
        actionReset.setOnClickListener(this);
        String currentDate = Utility.getInstance().formatDate();
        if(currentDate.equalsIgnoreCase(EntPreferences.getUpstoxConnectionDate(this))){
           chkOrderType.setClickable(false);
           chkOrderType.setEnabled(false);
           edtToken.setText("");
           edtCode.setText("");
        }
        //getToken();
        edtApiKey.setText(EntPreferences.getUpstoxApiKey(this));
        edtToken.setText(EntPreferences.getUpstoxToken(this));
        edtApiSecret.setText(EntPreferences.getUpstoxKeySecret(this));
        edtApiKey.setText(EntPreferences.getUpstoxApiKey(this));
    }

    private void initDialog(){
        if(!isFinishing()) {
            mProgressBar = new ProgressDialog(this);
            mProgressBar.setCancelable(true);//you can cancel it by pressing back button
        }
    }

    public void showProgressDialog(String message){
        if(!isFinishing()) {
            if (mProgressBar == null) {
                initDialog();
            }
            mProgressBar.setMessage(message);
            mProgressBar.show();
        }
    }

    public void hideDialog(){
        if(!isFinishing()) {
            if (mProgressBar != null && mProgressBar.isShowing()) {
                mProgressBar.dismiss();
            }
        }
    }

    private void getToken(){


        ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(this),
                EntPreferences.getUpstoxKeySecret(this));
        AccessToken accessToken = new AccessToken();
        accessToken.setToken(EntPreferences.getUpstoxToken(this));
        accessToken.setType("Bearer");
        accessToken.setExpiresIn(86400000l);
        TokenRequest tokenRequest = new TokenRequest(EntPreferences.getUpstoxCode(this),"authorization_code","http://niftywatcher.in");
        LoginService service=new LoginService(tokenRequest,apiCredentials);
        try {
            service.getAccessToken(this, new OnTokenFound() {
                @Override
                public void onTokenFound(AccessToken token) {
                    if(token!=null){
                        if(!TextUtils.isEmpty(token.getToken())){
                            EntPreferences.setUpstoxToken(UpstoxConnectionFragment.this,token.getToken());
                            edtToken.setText(token.getToken());
                        }
                    }
                    hideDialog();
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void shareOnWhatsApp(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("apikey:"+edtApiKey.getText().toString());
        buffer.append("\napisecret:"+edtApiSecret.getText().toString());
        buffer.append("\ncode:"+edtCode.getText().toString());
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, buffer.toString());
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this,"Getting Error",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(EntPreferences.getUpstoxCode(UpstoxConnectionFragment.this))){
            showProgressDialog("Please wait");
            getToken();
            if(edtCode!=null){
                edtCode.setText(EntPreferences.getUpstoxCode(UpstoxConnectionFragment.this));
            }
            //actionGenerateToken.setText("Share");
        }
    }

    public boolean isValid(){
        String apikey = edtApiKey.getText().toString();
        String apiSecretkey = edtApiSecret.getText().toString();
        if(TextUtils.isEmpty(apikey)){
            showAlertBox("Error","Please enter Upstox API Key");
            edtApiKey.requestFocus();
            return false;
        }else if(TextUtils.isEmpty(apiSecretkey)){
            showAlertBox("Error","Please enter Upstox Secret  Key");
            edtApiSecret.requestFocus();
            return false;
        }else{
            return true;
        }
    }

    private void showAlertBox(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        // Create the AlertDialog object and return it
        builder.create();
        builder.show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == actionReset.getId()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirmation");
            builder.setMessage("Are you sure to reset your code and token");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    EntPreferences.setUpstoxToken(UpstoxConnectionFragment.this,"");
                    EntPreferences.setUpstoxCode(UpstoxConnectionFragment.this,"");
                    edtCode.setText("");
                    edtToken.setText("");
                    actionGenerateToken.setText("Generate");
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog object and return it
            builder.create();
            builder.show();
        }else if(v.getId()==actionGenerateToken.getId()){
                if (isValid()) {
                    EntPreferences.setUpstoxApiKey(UpstoxConnectionFragment.this, edtApiKey.getText().toString());
                    EntPreferences.setUpstoxKeySecret(UpstoxConnectionFragment.this, edtApiSecret.getText().toString());
                    String url = "https://api.upstox.com/index/dialog/authorize?apiKey=" + edtApiKey.getText().toString() + "&redirect_uri=http://niftywatcher.in&response_type=code";
                    EntPreferences.setSetUpUrl(UpstoxConnectionFragment.this, url);
                    Intent intent = new Intent(UpstoxConnectionFragment.this, WebFragment.class);
                    startActivity(intent);
                }
        }else if(v.getId() == actionSubmit.getId()){
            if(TextUtils.isEmpty(edtToken.getText().toString().trim())){
                showAlertBox("TOKEN REQUIRED","Token is important for connecting Upstock..\n This is every day task for us\n\n\n You simply send to Sudhir(9665385862) above code through whatsapp I will send you token");
            }else {
                if(chkOrderType.isChecked()){
                    EntPreferences.setUpstoxKeyOrderType(UpstoxConnectionFragment.this,"oco");
                }
                EntPreferences.setUpstoxApiKey(UpstoxConnectionFragment.this, edtApiKey.getText().toString());
                EntPreferences.setUpstoxKeySecret(UpstoxConnectionFragment.this, edtApiSecret.getText().toString());
                EntPreferences.setUpstoxToken(UpstoxConnectionFragment.this, edtToken.getText().toString());
                EntPreferences.setUpstoxConnectionDate(UpstoxConnectionFragment.this,Utility.getInstance().formatDate());
                finish();
            }
        }
    }
}
