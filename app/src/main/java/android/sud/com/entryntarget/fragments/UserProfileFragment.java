package android.sud.com.entryntarget.fragments;

import android.os.Bundle;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.ui.adapter.SimpleRVAdapter;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.sud.com.entryntarget.ui.fonts.MyTextViewBold;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.iface.OnTokenChange;
import android.sud.com.entryntarget.upstockService.users.UserService;
import android.sud.com.entryntarget.upstockService.users.models.Position;
import android.sud.com.entryntarget.upstockService.users.models.Profile;
import android.sud.com.entryntarget.upstockService.users.models.ProfileBalance;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.utils.Utility;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileFragment extends BaseFragment implements OnTokenChange{


    MyTextViewBold txtUserName;
    MyTextView txtemail,txtBalance;
    private RecyclerView recyclerView;
    SimpleRVAdapter simpleRVAdapter;
    List<String> exchangeList = new ArrayList<>();

    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(TextUtils.isEmpty(EntPreferences.getUpstoxToken(getActivity()))){
            //OverTheAir.getInstance(getActivity()).generateToken(this);
        }else {
            //OverTheAir.getInstance(getActivity()).getProfile(this);
            //OverTheAir.getInstance(getActivity()).getUserBalance(this);
            showProgressDialog("Please wait we are loading your profile");
            //EntPreferences.setUpstoxApiKey(getActivity(),"eKwYuPYl6Q8iQKpKAYSpr39Oy3T9dL4C83cWFuYK");
            ApiCredentials apiCredentials = new ApiCredentials(EntPreferences.getUpstoxApiKey(getActivity()),
                    EntPreferences.getUpstoxKeySecret(getActivity()));
            AccessToken accessToken = new AccessToken();
            accessToken.setToken(EntPreferences.getUpstoxToken(getActivity()));
            accessToken.setType("Bearer");
            accessToken.setExpiresIn(86400000l);
            UserService userService = new UserService(accessToken,apiCredentials);
            /*Call<UpstoxResponse<List<Position>>> pos = userService.getPositions();
            pos.enqueue(new Callback<UpstoxResponse<List<Position>>>() {
                @Override
                public void onResponse(Call<UpstoxResponse<List<Position>>> call, Response<UpstoxResponse<List<Position>>> response) {
                    if(response.isSuccessful()){
                        UpstoxResponse<List<Position>> body = response.body();
                        body.getData();
                    }
                }

                @Override
                public void onFailure(Call<UpstoxResponse<List<Position>>> call, Throwable t) {
                    Log.d("TAG","");
                }
            });*/
            Call<UpstoxResponse<Profile>> profileservice = userService.getProfile();
            profileservice.enqueue(new Callback<UpstoxResponse<Profile>>() {
                @Override
                public void onResponse(Call<UpstoxResponse<Profile>> call, Response<UpstoxResponse<Profile>> response) {
                    if(response.isSuccessful()){
                        UpstoxResponse<Profile> profileData = response.body();
                        Profile profileInfo = profileData.getData();
                        setProfileInfo(profileInfo);
                        hideDialog();
                    }
                }

                @Override
                public void onFailure(Call<UpstoxResponse<android.sud.com.entryntarget.upstockService.users.models.Profile>> call, Throwable t) {

                }
            });
            Call<UpstoxResponse<ProfileBalance>> userBalanceCall = userService.getProfileBalance("security");
            userBalanceCall.enqueue(new Callback<UpstoxResponse<ProfileBalance>>() {
                @Override
                public void onResponse(Call<UpstoxResponse<ProfileBalance>> call, Response<UpstoxResponse<ProfileBalance>> response) {
                    if(response.isSuccessful()){
                        UpstoxResponse<ProfileBalance> userInfo = response.body();
                        ProfileBalance balance = userInfo.getData();
                        setBalance(balance);
                    }
                }

                @Override
                public void onFailure(Call<UpstoxResponse<ProfileBalance>> call, Throwable t) {

                }
            });
        }
    }

    private void setBalance(ProfileBalance balance) {
        if(balance!=null){
            txtBalance.setText(String.valueOf(balance.getEquity().getAvailableMargin()));
        }
    }

    private void setProfileInfo(Profile profileInfo) {
        txtUserName.setText(profileInfo.getName());
        txtemail.setText(profileInfo.getEmail());
        updateUserInfo(profileInfo.getExchangesEnabled());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.userprofile_layout, container, false);
        txtUserName = (MyTextViewBold) view.findViewById(R.id.user_profile_name);
        txtemail = (MyTextView) view.findViewById(R.id.user_profile_email);
        txtBalance = (MyTextView) view.findViewById(R.id.txt_bal);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);//every item of the RecyclerView has a fix size
        simpleRVAdapter = new SimpleRVAdapter(exchangeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(simpleRVAdapter);
        //StockSdk.getInstance().getProfile(getActivity(),this);


        return view;
    }



    private void updateUserInfo(Set<String> set){
        exchangeList.clear();
        for (String s : set) {
            exchangeList.add(s);
            System.out.println(s);
        }
        simpleRVAdapter.notifyDataSetChanged();
    }


    @Override
    public void OnTokenChangeListener(String token) {
        if(TextUtils.isEmpty(token)){
            showErrorAlert("Error","Your Token not generated...");
        }else{
            EntPreferences.setUpstoxToken(getActivity(),token);
        }
    }

}
