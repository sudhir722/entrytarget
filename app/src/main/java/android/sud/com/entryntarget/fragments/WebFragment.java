package android.sud.com.entryntarget.fragments;

import android.content.Context;
import android.os.Bundle;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class WebFragment extends AppCompatActivity {

    WebView mWebview;
    Context mContext;
        @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_web);
        WebView webView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        String url = EntPreferences.getSetUpUrl(this);
        webView.loadUrl(url);
        mContext = this;
    }


    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            //view.loadUrl(EntPreferences.getSetUpUrl(WebFragment.this));
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url){
            Toast.makeText(WebFragment.this,url,Toast.LENGTH_LONG).show();
            if(url.contains("https://niftywatcher.in/?code")){
                //https://niftywatcher.in/?code=b95437824d76f427971e5293dd6d397ee7f825bd
                String code = url.replace("https://niftywatcher.in/?code=","");
                EntPreferences.setUpstoxCode(mContext,code);
                Toast.makeText(WebFragment.this," New code Found "+code,Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                  String description, String failingUrl){
            super.onReceivedError(view, errorCode, description, failingUrl);
        }
    }
}
