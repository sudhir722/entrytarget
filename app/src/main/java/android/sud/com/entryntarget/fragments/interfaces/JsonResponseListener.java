package android.sud.com.entryntarget.fragments.interfaces;

import com.google.gson.JsonObject;

public interface JsonResponseListener
{
    public void gotResponse(JsonObject response);

}
