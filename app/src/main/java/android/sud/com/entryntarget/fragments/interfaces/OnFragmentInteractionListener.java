package android.sud.com.entryntarget.fragments.interfaces;

import android.net.Uri;

public interface OnFragmentInteractionListener {

    void onFragmentInteraction(Uri uri);

    void onFragmentInteractionChange(int action);

}
