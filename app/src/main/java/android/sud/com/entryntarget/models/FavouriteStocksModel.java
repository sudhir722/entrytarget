package android.sud.com.entryntarget.models;

public class FavouriteStocksModel {

    String symbol;
    int quantity;
    int maxTriggers;
    String createdDate;

    public FavouriteStocksModel() {
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMaxTriggers() {
        return maxTriggers;
    }

    public void setMaxTriggers(int maxTriggers) {
        this.maxTriggers = maxTriggers;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public FavouriteStocksModel(String symbol, int quantity, int maxTriggers, String createdDate) {
        this.symbol = symbol;
        this.quantity = quantity;
        this.maxTriggers = maxTriggers;
        this.createdDate = createdDate;
    }
}
