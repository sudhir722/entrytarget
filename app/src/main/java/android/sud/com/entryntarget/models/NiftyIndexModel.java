package android.sud.com.entryntarget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class NiftyIndexModel  {

    @SerializedName("indexName")
    @Expose
    String indexName;

    @SerializedName("open")
    @Expose
    String open;

    @SerializedName("high")
    @Expose
    String high;

    @SerializedName("low")
    @Expose
    String low;

    @SerializedName("ltp")
    @Expose
    String currentPrice;

    @SerializedName("ch")
    @Expose
    double changeInPer;

    @SerializedName("per")
    @Expose
    double per;

    @SerializedName("yCls")
    @Expose
    double yCls;

    @SerializedName("mCls")
    @Expose
    double mCls;

    @SerializedName("yHigh")
    @Expose
    String yHigh;

    @SerializedName("yLow")
    @Expose
    String yLow;

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getCurrentPrice() {
        if(currentPrice.contains(",")){
            currentPrice = currentPrice.replace(",","");
        }
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getChangeInPer() {
        return changeInPer;
    }

    public void setChangeInPer(double changeInPer) {
        this.changeInPer = changeInPer;
    }

    public double getPer() {
        return per;
    }

    public void setPer(double per) {
        this.per = per;
    }

    public double getyCls() {
        return yCls;
    }

    public void setyCls(double yCls) {
        this.yCls = yCls;
    }

    public double getmCls() {
        return mCls;
    }

    public void setmCls(double mCls) {
        this.mCls = mCls;
    }

    public String getyHigh() {
        return yHigh;
    }

    public void setyHigh(String yHigh) {
        this.yHigh = yHigh;
    }

    public String getyLow() {
        return yLow;
    }

    public void setyLow(String yLow) {
        this.yLow = yLow;
    }

}
