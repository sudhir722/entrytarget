package android.sud.com.entryntarget.models;

public class StockInfo {
    int id;
    String scriptName;
    String date;
    double ltp;
    double open;
    double low;

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    double close;
    double high;
    double previousClose;
    double buySl;
    double buyAt;

    public String getScriptName() {
        return scriptName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLtp() {
        return ltp;
    }

    public void setLtp(double ltp) {
        this.ltp = ltp;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getPreviousClose() {
        return previousClose;
    }

    public void setPreviousClose(double previousClose) {
        this.previousClose = previousClose;
    }

    public double getBuySl() {
        return buySl;
    }

    public void setBuySl(double buySl) {
        this.buySl = buySl;
    }

    public double getBuyAt() {
        return buyAt;
    }

    public void setBuyAt(double buyAt) {
        this.buyAt = buyAt;
    }

    public double getTarget1() {
        return target1;
    }

    public void setTarget1(double target1) {
        this.target1 = target1;
    }

    public double getTarget2() {
        return target2;
    }

    public void setTarget2(double target2) {
        this.target2 = target2;
    }

    public double getTarget3() {
        return target3;
    }

    public void setTarget3(double target3) {
        this.target3 = target3;
    }

    public double getTarget4() {
        return target4;
    }

    public void setTarget4(double target4) {
        this.target4 = target4;
    }

    public double getTarget5() {
        return target5;
    }

    public void setTarget5(double target5) {
        this.target5 = target5;
    }

    public double getTarget6() {
        return target6;
    }

    public void setTarget6(double target6) {
        this.target6 = target6;
    }

    public double getSellAt() {
        return sellAt;
    }

    public void setSellAt(double sellAt) {
        this.sellAt = sellAt;
    }

    public double getSellSl() {
        return sellSl;
    }

    public void setSellSl(double sellSl) {
        this.sellSl = sellSl;
    }

    public double getSell_target1() {
        return sell_target1;
    }

    public void setSell_target1(double sell_target1) {
        this.sell_target1 = sell_target1;
    }

    public double getSell_target2() {
        return sell_target2;
    }

    public void setSell_target2(double sell_target2) {
        this.sell_target2 = sell_target2;
    }

    public double getSell_target3() {
        return sell_target3;
    }

    public void setSell_target3(double sell_target3) {
        this.sell_target3 = sell_target3;
    }

    public double getSell_target4() {
        return sell_target4;
    }

    public void setSell_target4(double sell_target4) {
        this.sell_target4 = sell_target4;
    }

    public double getSell_target5() {
        return sell_target5;
    }

    public void setSell_target5(double sell_target5) {
        this.sell_target5 = sell_target5;
    }

    public double getSell_target6() {
        return sell_target6;
    }

    public void setSell_target6(double sell_target6) {
        this.sell_target6 = sell_target6;
    }

    double target1;
    double target2;
    double target3;
    double target4;
    double target5;

    public StockInfo() {
    }

    double target6;
    double sellAt;
    double sellSl;
    double sell_target1;
    double sell_target2;
    double sell_target3;
    double sell_target4;
    double sell_target5;
    double sell_target6;



}
