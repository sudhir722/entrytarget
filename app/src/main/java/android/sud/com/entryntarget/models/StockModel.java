package android.sud.com.entryntarget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StockModel {
    @SerializedName("trdVolumesumMil")
    @Expose
    String trdVolumesumMil;

    @SerializedName("time")
    @Expose
    String time;

    public String getTrdVolumesumMil() {
        return trdVolumesumMil;
    }

    public void setTrdVolumesumMil(String trdVolumesumMil) {
        this.trdVolumesumMil = trdVolumesumMil;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ArrayList<NiftyIndexModel> getNiftyIndexModelArrayList() {
        return niftyIndexModelArrayList;
    }

    public void setNiftyIndexModelArrayList(ArrayList<NiftyIndexModel> contacts) {
        this.niftyIndexModelArrayList = contacts;
    }

    public int getDeclines() {
        return declines;
    }

    public void setDeclines(int declines) {
        this.declines = declines;
    }

    public String getTrdValueSum() {
        return trdValueSum;
    }

    public void setTrdValueSum(String trdValueSum) {
        this.trdValueSum = trdValueSum;
    }

    public ArrayList<Stocks> getStocklist() {
        return stocklist;
    }

    public void setStocklist(ArrayList<Stocks> stocklist) {
        this.stocklist = stocklist;
    }

    public String getTrdValueSumMil() {
        return trdValueSumMil;
    }

    public void setTrdValueSumMil(String trdValueSumMil) {
        this.trdValueSumMil = trdValueSumMil;
    }

    public int getUnchanged() {
        return unchanged;
    }

    public void setUnchanged(int unchanged) {
        this.unchanged = unchanged;
    }

    public String getTrdVolumesum() {
        return trdVolumesum;
    }

    public void setTrdVolumesum(String trdVolumesum) {
        this.trdVolumesum = trdVolumesum;
    }

    public int getAdvances() {
        return advances;
    }

    public void setAdvances(int advances) {
        this.advances = advances;
    }

    @SerializedName("latestData")
    @Expose
    private ArrayList<NiftyIndexModel> niftyIndexModelArrayList = new ArrayList<>();

    @SerializedName("declines")
    @Expose
    int declines;

    @SerializedName("trdValueSum")
    @Expose
    String trdValueSum;

    @SerializedName("data")
    @Expose
    private ArrayList<Stocks> stocklist = new ArrayList<>();

    @SerializedName("trdValueSumMil")
    @Expose
    String trdValueSumMil;

    @SerializedName("unchanged")
    @Expose
    int unchanged;

    @SerializedName("trdVolumesum")
    @Expose
    String trdVolumesum;

    @SerializedName("advances")
    @Expose
    int advances;
}
