package android.sud.com.entryntarget.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stocks {

    @SerializedName("symbol")
    @Expose
    String symbol;

    @SerializedName("open")
    @Expose
    String open;

    @SerializedName("high")
    @Expose
    String high;

    @SerializedName("low")
    @Expose
    String low;

    @SerializedName("ltP")
    @Expose
    String ltP;

    @SerializedName("ptsC")
    @Expose
    String ptsC;

    @SerializedName("per")
    @Expose
    String per;

    @SerializedName("trdVol")
    @Expose
    String trdVol;

    @SerializedName("trdVolM")
    @Expose
    String trdVolM;

    @SerializedName("ntP")
    @Expose
    String ntP;

    @SerializedName("mVal")
    @Expose
    String mVal;

    @SerializedName("wkhi")
    @Expose
    String wkhi;

    @SerializedName("wklo")
    @Expose
    String wklo;

    @SerializedName("wkhicm_adj")
    @Expose
    String wkhicm_adj;

    @SerializedName("wklocm_adj")
    @Expose
    String wklocm_adj;

    @SerializedName("xDt")
    @Expose
    String xDt;

    @SerializedName("cAct")
    @Expose
    String cAct;

    @SerializedName("previousClose")
    @Expose
    String previousClose;

    @SerializedName("dayEndClose")
    @Expose
    String dayEndClose;

    @SerializedName("iislPtsChange")
    @Expose
    String iislPtsChange;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getOpen() {

        if(open.contentEquals(",")){
            open = open.replaceAll(",","");
        }
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {

        if(high.contentEquals(",")){
            high = high.replaceAll(",","");
        }
        this.high = high;
    }

    public String getLow() {

        if(low.contentEquals(",")){
            low = low.replaceAll(",","");
        }
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getLtP() {
        if(ltP.contentEquals(",")){
            ltP = ltP.replaceAll(",","");
        }
        return ltP;
    }

    public void setLtP(String ltP) {
        if(ltP.contentEquals(",")){
            ltP = ltP.replaceAll(",","");
        }
        this.ltP = ltP;
    }

    public String getPtsC() {
        return ptsC;
    }

    public void setPtsC(String ptsC) {
        this.ptsC = ptsC;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }

    public String getTrdVol() {
        return trdVol;
    }

    public void setTrdVol(String trdVol) {
        this.trdVol = trdVol;
    }

    public String getTrdVolM() {
        return trdVolM;
    }

    public void setTrdVolM(String trdVolM) {
        this.trdVolM = trdVolM;
    }

    public String getNtP() {
        return ntP;
    }

    public void setNtP(String ntP) {
        this.ntP = ntP;
    }

    public String getmVal() {
        return mVal;
    }

    public void setmVal(String mVal) {
        this.mVal = mVal;
    }

    public String getWkhi() {
        return wkhi;
    }

    public void setWkhi(String wkhi) {
        this.wkhi = wkhi;
    }

    public String getWklo() {
        return wklo;
    }

    public void setWklo(String wklo) {
        this.wklo = wklo;
    }

    public String getWkhicm_adj() {
        return wkhicm_adj;
    }

    public void setWkhicm_adj(String wkhicm_adj) {
        this.wkhicm_adj = wkhicm_adj;
    }

    public String getWklocm_adj() {
        return wklocm_adj;
    }

    public void setWklocm_adj(String wklocm_adj) {
        this.wklocm_adj = wklocm_adj;
    }

    public String getxDt() {
        return xDt;
    }

    public void setxDt(String xDt) {
        this.xDt = xDt;
    }

    public String getcAct() {
        return cAct;
    }

    public void setcAct(String cAct) {
        this.cAct = cAct;
    }

    public String getPreviousClose() {
        return previousClose;
    }

    public void setPreviousClose(String previousClose) {
        this.previousClose = previousClose;
    }

    public String getDayEndClose() {
        return dayEndClose;
    }

    public void setDayEndClose(String dayEndClose) {
        this.dayEndClose = dayEndClose;
    }

    public String getIislPtsChange() {
        return iislPtsChange;
    }

    public void setIislPtsChange(String iislPtsChange) {
        this.iislPtsChange = iislPtsChange;
    }

    public String getIislPercChange() {
        return iislPercChange;
    }

    public void setIislPercChange(String iislPercChange) {
        this.iislPercChange = iislPercChange;
    }

    public String getyPC() {
        return yPC;
    }

    public void setyPC(String yPC) {
        this.yPC = yPC;
    }

    public String getmPC() {
        return mPC;
    }

    public void setmPC(String mPC) {
        this.mPC = mPC;
    }

    @SerializedName("iislPercChange")

    @Expose
    String iislPercChange;

    @SerializedName("yPC")
    @Expose
    String yPC;

    @SerializedName("mPC")
    @Expose
    String mPC;



}
