package android.sud.com.entryntarget.models;

import android.sud.com.entryntarget.database.DatabaseConstants;

public class TriggerInfo {
    int id;

    String orderId;

    public String getModOrderId() {
        return modOrderId;
    }

    public void setModOrderId(String modOrderId) {
        this.modOrderId = modOrderId;
    }

    String modOrderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String symbol;
    double ltp;
    int isTslHit;
    double change;

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public int getIsTslHit() {
        return isTslHit;
    }

    public void setIsTslHit(int isTslHit) {
        this.isTslHit = isTslHit;
    }

    public double getTsl() {
        return tsl;
    }

    public void setTsl(double tsl) {
        this.tsl = tsl;
    }

    double high;
    double low;
    String date;
    double close;
    String signal;
    double recPrice;
    double tsl;
    double t1;
    double t2;

    double exitPrice;

    public double getExitPrice() {
        return exitPrice;
    }

    public void setExitPrice(double exitPrice) {
        this.exitPrice = exitPrice;
    }

    public double getSl() {
        return sl;
    }

    public void setSl(double sl) {
        this.sl = sl;
    }

    double t3;
    double sl;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getLtp() {
        return ltp;
    }

    public void setLtp(double ltp) {
        this.ltp = ltp;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public double getRecPrice() {
        return recPrice;
    }

    public void setRecPrice(double recPrice) {
        this.recPrice = recPrice;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getT1() {
        return t1;
    }

    public void setT1(double t1) {
        this.t1 = t1;
    }

    public double getT2() {
        return t2;
    }

    public void setT2(double t2) {
        this.t2 = t2;
    }

    public double getT3() {
        return t3;
    }

    public void setT3(double t3) {
        this.t3 = t3;
    }

    public double getT4() {
        return t4;
    }

    public void setT4(double t4) {
        this.t4 = t4;
    }

    public double getT5() {
        return t5;
    }

    public void setT5(double t5) {
        this.t5 = t5;
    }

    public double getT6() {
        return t6;
    }

    public void setT6(double t6) {
        this.t6 = t6;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public int getSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(int sessionStatus) {
        this.sessionStatus = sessionStatus;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    double t4;
    double t5;
    double t6;
    int isActive;
    String updateTime;
    int sessionStatus;

    public TriggerInfo() {
    }
}
