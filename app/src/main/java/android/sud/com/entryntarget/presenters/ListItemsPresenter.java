package android.sud.com.entryntarget.presenters;

import android.sud.com.entryntarget.models.TriggerInfo;
import android.sud.com.entryntarget.models.TriggerModel;

public interface ListItemsPresenter {

    void onClick(TriggerModel itemModel);
    void onLoadMoreClick();
}
