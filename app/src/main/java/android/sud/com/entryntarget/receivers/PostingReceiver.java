package android.sud.com.entryntarget.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.sud.com.entryntarget.corelogic.MeterSdk;
import android.sud.com.entryntarget.corelogic.StockSdk;
import android.util.Log;

import java.util.Calendar;


public class PostingReceiver extends BroadcastReceiver {
    public PostingReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        if (context == null)
            return;
        Calendar rightNow = Calendar.getInstance();
        Log.d("DBAdapter","Posting receiver called");
        //StockerPermissionManager.getInstance().showUserAction(context,"Nifty","Buy at 10450");
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentmin = rightNow.get(Calendar.MINUTE);
        if(currentHour >= 9 && currentHour < 16) {
            if(currentHour ==9 && currentmin <15){

            }else if(currentHour==15 && currentmin>30){

            }else {
                StockSdk.getInstance().retriveStockInfo(context, null);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            MeterSdk.schedulePostingAlarmSp(context);
        } else {
            MeterSdk.getInstance().setAlarm(context);
        }
    }
}
