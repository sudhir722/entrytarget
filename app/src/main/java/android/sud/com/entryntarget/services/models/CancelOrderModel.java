package android.sud.com.entryntarget.services.models;

public class CancelOrderModel {

    String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public CancelOrderModel() {
    }

    public CancelOrderModel(String order_id) {
        this.order_id = order_id;
    }
}
