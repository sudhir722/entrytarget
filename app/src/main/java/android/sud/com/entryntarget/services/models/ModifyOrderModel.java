package android.sud.com.entryntarget.services.models;

public class ModifyOrderModel {

    String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    String order_type;
    double price;

    public ModifyOrderModel(String order_id, String order_type, double price) {
        this.order_id = order_id;
        this.order_type = order_type;
        this.price = price;
    }

    public ModifyOrderModel() {
    }
}
