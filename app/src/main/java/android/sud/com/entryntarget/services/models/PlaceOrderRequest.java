package android.sud.com.entryntarget.services.models;

public class PlaceOrderRequest {

    String token;
    String user_id;
    String type;
    String exchange;
    String symbol;
    int quantity;
    double price;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice_at() {
        return price;
    }

    public void setPrice_at(double price_at) {
        this.price = price_at;
    }

    public PlaceOrderRequest(String token, String user_id, String type, String exchange, String symbol, int quantity, double price_at) {
        this.token = token;
        this.user_id = user_id;
        this.type = type;
        this.exchange = exchange;
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price_at;
    }

    public PlaceOrderRequest() {
    }
}
