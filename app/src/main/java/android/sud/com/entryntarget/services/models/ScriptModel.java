package android.sud.com.entryntarget.services.models;

public class ScriptModel {

    public ScriptModel(String symbol) {
        this.symbol = symbol;
    }

    String symbol;

    public String getSymbol() {
        return symbol;
    }

    public ScriptModel() {
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
