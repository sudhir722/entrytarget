package android.sud.com.entryntarget.services.models;

public class UserModel {

    String token;

    public String getToken() {
        return token;
    }

    public UserModel() {
    }

    public UserModel(String token) {
        this.token = token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
