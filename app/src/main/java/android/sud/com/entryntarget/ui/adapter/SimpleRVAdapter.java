package android.sud.com.entryntarget.ui.adapter;

import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SimpleRVAdapter extends RecyclerView.Adapter<SimpleRVAdapter.SimpleViewHolder> {
    private List<String> dataSource;
    public SimpleRVAdapter(List<String> dataArgs){
        dataSource = dataArgs;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        /*View view = new MyTextView(parent.getContext());
        SimpleViewHolder viewHolder = new SimpleViewHolder(view);
        return viewHolder;*/
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_list_row, parent, false);
        SimpleViewHolder holder = new SimpleViewHolder(v);
        return holder;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        public MyTextView textView;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            textView = (MyTextView) itemView.findViewById(R.id.title);
        }
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.textView.setText(dataSource.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }
}