package android.sud.com.entryntarget.ui.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.sud.com.entryntarget.R;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.database.DatabaseConstants;
import android.sud.com.entryntarget.models.StockInfo;
import android.sud.com.entryntarget.models.TriggerInfo;
import android.sud.com.entryntarget.ui.fonts.MyTextView;
import android.sud.com.entryntarget.ui.fonts.MyTextViewBold;
import android.sud.com.entryntarget.utils.Utility;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static android.sud.com.entryntarget.utility.EntConstants.CONST_BUY;
import static android.sud.com.entryntarget.utility.EntConstants.CONST_SELL;


public class TriggerNewRecycleAdapter extends RecyclerView.Adapter<TriggerNewRecycleAdapter.MyViewHolder> {

    List<TriggerInfo> modelList;
    public Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyTextView txtSignal,recordPrice,txtStopLoss,txtUpdateTime,txtChange,txtTarget,txtNxtTarget;
        public  MyTextViewBold symbol, ltp;

        public MyTextView lbl_sl;
        public ImageView imgClosed;
        public View myView;
        public RelativeLayout contenar;

        public MyViewHolder(View view) {
            super(view);
            symbol = (MyTextViewBold) view.findViewById(R.id.txt_symbols);
            txtSignal = (MyTextView) view.findViewById(R.id.txt_signal);
            ltp = (MyTextViewBold) view.findViewById(R.id.txt_ltp);
            recordPrice = (MyTextView) view.findViewById(R.id.txt_recprice);
            lbl_sl = (MyTextView) view.findViewById(R.id.lbl_sl);
            txtStopLoss = (MyTextView) view.findViewById(R.id.txt_sl);
            txtChange = (MyTextView) view.findViewById(R.id.txt_change);
            txtUpdateTime = (MyTextView) view.findViewById(R.id.txt_updated_time);
            txtNxtTarget = (MyTextView) view.findViewById(R.id.txt_nxttarget);
            imgClosed = (ImageView) view.findViewById(R.id.img_is_closed);
            txtTarget = (MyTextView) view.findViewById(R.id.txt_target);
            myView = (View) view.findViewById(R.id.view);
            contenar = (RelativeLayout) view.findViewById(R.id.main_content);
        }
    }
    public TriggerNewRecycleAdapter(Context context, List<TriggerInfo> modelList1) {
        this.modelList = modelList1;
        this.mContext = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TriggerInfo model = modelList.get(position);
        holder.symbol.setText(model.getSymbol());


        holder.recordPrice.setText(String.valueOf(model.getRecPrice()));
        holder.txtSignal.setText(model.getSignal());
        //holder.txtComments.setText(model.getDate());
        holder.txtStopLoss.setText(String.valueOf(model.getSl()));
        holder.txtUpdateTime.setText(model.getUpdateTime());
        StringBuffer buffer = new StringBuffer();
        String token="";
        String previousTarget ="";
        String nextTarget ="T1 : "+model.getT1();
        StockInfo stocksLevel = DBAdapter.getDBAdapter(mContext).getStockLevels(model.getSymbol());
        holder.ltp.setText(String.valueOf(stocksLevel.getLtp()));
        if(model.getIsActive()==-1){
            holder.txtTarget.setText("Trigger Pending...");
        }else {
            if (model.getT1() > 0) {
                buffer.append("T1");
                previousTarget = "";
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T2 : " + stocksLevel.getTarget2();
                } else {
                    nextTarget = "T2 : " + stocksLevel.getSell_target2();
                }
                token = "->";
            } else {
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T1 : " + stocksLevel.getTarget1();
                } else {
                    nextTarget = "T1 : " + stocksLevel.getSell_target1();
                }
            }
            if (model.getT2() > 0) {
                buffer.append(token + "T2");
                previousTarget = "T1 : " + model.getT1();
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T3 : " + stocksLevel.getTarget3();
                } else {
                    nextTarget = "T3 : " + stocksLevel.getSell_target3();
                }
                token = "->";
            }
            if (model.getT3() > 0) {
                buffer.append(token + "T3");
                previousTarget = "T2 : " + model.getT2();
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T4 : " + stocksLevel.getTarget4();
                } else {
                    nextTarget = "T4 : " + stocksLevel.getSell_target4();
                }
                token = "->";
            }
            if (model.getT4() > 0) {
                buffer.append(token + "T4");
                previousTarget = "T3 : " + model.getT3();
                nextTarget = "T5 : " + model.getT5();
                token = "->";
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T5 : " + stocksLevel.getTarget5();
                } else {
                    nextTarget = "T5 : " + stocksLevel.getSell_target5();
                }
            }
            if (model.getT5() > 0) {
                buffer.append(token + "T5");
                previousTarget = "T4 : " + model.getT4();
                token = "->";
                //nextTarget ="T6 : "+model.getT6();
                if (model.getSignal().equalsIgnoreCase(CONST_BUY)) {
                    nextTarget = "T6 : " + stocksLevel.getTarget6();
                } else {
                    nextTarget = "T6 : " + stocksLevel.getSell_target6();
                }
            }
            if (model.getT6() > 0) {
                buffer.append(token + "T6");
                previousTarget = "T5 : " + model.getT5();
                token = "->";
            }
            if(model.getExitPrice()>0) {
                if (model.getIsTslHit() == 1) {
                    buffer.append(token + "TSL");
                } else if (model.getSl() > 0) {
                    buffer.append(token + "SL");
                }
            }
            holder.txtTarget.setText(buffer.toString());
        }

        double change =0.0;
        if(model.getSignal().equalsIgnoreCase(CONST_BUY)){
            holder.myView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green));
            holder.txtSignal.setBackgroundResource(R.drawable.rounded_corner_buy);
            if(stocksLevel!=null){
                holder.txtStopLoss.setText(String.valueOf(stocksLevel.getBuySl()));
            }
            if(model.getChange()!=0){
                change = model.getChange();
            }else if(model.getIsActive()==0) {
                change = model.getLtp() - model.getRecPrice();
            }else{
                if(model.getSl()>0) {
                    change = model.getSl() - model.getRecPrice();
                    holder.txtStopLoss.setText(String.valueOf(model.getSl()));
                }else if(model.getIsTslHit()==1){
                    change = model.getTsl() - model.getRecPrice();
                    holder.txtStopLoss.setText(String.valueOf(model.getTsl()));
                }else{
                    change = model.getLtp() - model.getRecPrice();
                    holder.txtStopLoss.setText(String.valueOf(model.getLtp()));
                }
            }
            //holder.ltp.setTextColor(mContext.getResources().getColor(R.color.color_green));

        }else if(model.getSignal().equalsIgnoreCase(CONST_SELL)){
            if(stocksLevel!=null){
                holder.txtStopLoss.setText(String.valueOf(stocksLevel.getSellSl()));
            }
            if(model.getIsActive()==0) {
                change = model.getRecPrice() - model.getLtp();
            }else{
                if(model.getChange()!=0){
                    change = model.getChange();
                }else  if(model.getSl()>0) {
                    change = model.getRecPrice() - model.getSl();
                    holder.txtStopLoss.setText(String.valueOf(model.getSl()));
                }else if(model.getIsTslHit()==1){
                    change = model.getRecPrice()- model.getTsl();
                    holder.txtStopLoss.setText(String.valueOf(model.getTsl()));
                }else{
                    change = model.getRecPrice() - model.getLtp();
                    holder.txtStopLoss.setText(String.valueOf(model.getLtp()));
                }
            }
            holder.txtSignal.setBackgroundResource(R.drawable.rounded_corner);
            holder.ltp.setTextColor(mContext.getResources().getColor(R.color.color_red));
            holder.myView.setBackgroundColor(mContext.getResources().getColor(R.color.color_red));

        }
        if(model.getIsActive()==2){
            holder.txtNxtTarget.setText("Profit Booked..");
            holder.imgClosed.setVisibility(View.VISIBLE);
            holder.txtStopLoss.setText(String.valueOf(model.getExitPrice()));
            holder.lbl_sl.setText("EXIT: ");
        }else if(model.getIsActive()==1){
            holder.txtNxtTarget.setText("Session closed.");
            holder.imgClosed.setVisibility(View.VISIBLE);
            holder.txtStopLoss.setText(String.valueOf(model.getExitPrice()));
            holder.lbl_sl.setText("EXIT: ");
        }else if(model.getIsActive()==3){
            holder.txtNxtTarget.setText("AUTO CLOSE");
            holder.imgClosed.setVisibility(View.VISIBLE);
            holder.txtStopLoss.setText(String.valueOf(model.getExitPrice()));
            holder.lbl_sl.setText("EXIT: ");
        }else{
            holder.imgClosed.setVisibility(View.GONE);
            holder.txtNxtTarget.setText(previousTarget+"   "+nextTarget);
            holder.lbl_sl.setText("STOP LOSS");
        }
        holder.txtChange.setText(String.valueOf(Utility.getInstance().convertTwoDecimal(change)));
        if(change>0){
            holder.txtChange.setTextColor(mContext.getResources().getColor(R.color.color_green));

        }else{
            holder.txtChange.setTextColor(mContext.getResources().getColor(R.color.color_red));
        }
        final String symbol = model.getSymbol();
        holder.contenar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showPopup(v,symbol);
                return false;
            }
        });
    }

    private void insertFavSymbol(String symbol){
        boolean isAdded = DBAdapter.getDBAdapter(mContext).isAddedInFav(symbol);
        if(!isAdded) {
            ContentValues values = new ContentValues();
            values.put(DatabaseConstants.KEY_SCRIP, symbol);
            values.put(DatabaseConstants.CREATED_TIME, System.currentTimeMillis());
            if (!DBAdapter.getDBAdapter(mContext).updateData(DatabaseConstants.TABLE_FAV_SYMBOLS, values, DatabaseConstants.KEY_SCRIP + "='" + symbol + "'")) {
                DBAdapter.getDBAdapter(mContext).insertData(DatabaseConstants.TABLE_FAV_SYMBOLS, values);
            }
            Toast.makeText(mContext,"Stock added in your list",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mContext,"Stock already added in your list",Toast.LENGTH_LONG).show();
        }
    }

    private void removeFromFavSymbol(String symbol){
        int isAffected = DBAdapter.getDBAdapter(mContext).isDelete(DatabaseConstants.TABLE_FAV_SYMBOLS,DatabaseConstants.KEY_SCRIP + "='" + symbol + "'");
        if(isAffected>0) {
            Toast.makeText(mContext,"Stock removed from Favorite",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(mContext,"Unable to remove stock",Toast.LENGTH_LONG).show();
        }
    }

    public void showPopup(View view,final String symbol) {
        boolean isAdded = DBAdapter.getDBAdapter(mContext).isAddedInFav(symbol);
        if (!isAdded) {
            PopupMenu popup = new PopupMenu(mContext, view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.popup_stockmore, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_fav:
                            //archive(item);
                            insertFavSymbol(symbol);
                            return true;
                        default:
                            return false;
                    }
                }
            });
            popup.show();
        } else {
            PopupMenu popup = new PopupMenu(mContext, view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.popup_stockdelete, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_removefav:
                            //archive(item);
                            removeFromFavSymbol(symbol);
                            return true;
                        default:
                            return false;
                    }
                }
            });
            popup.show();
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
