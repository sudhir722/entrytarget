/*
package android.sud.com.entryntarget.ui.livedata.model;

import android.arch.lifecycle.ViewModel;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.sud.com.entryntarget.models.TriggerModel;

public class TriggerViewModel {

    String symbol;
    String signal;
    String ltp;
    String recPrice;
    String sl;

    public TriggerViewModel() {
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getLtp() {
        return ltp;
    }

    public void setLtp(String ltp) {
        this.ltp = ltp;
    }

    public String getRecPrice() {
        return recPrice;
    }

    public void setRecPrice(String recPrice) {
        this.recPrice = recPrice;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getUpcomingtrigger() {
        return upcomingtrigger;
    }

    public void setUpcomingtrigger(String upcomingtrigger) {
        this.upcomingtrigger = upcomingtrigger;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public TriggerViewModel(String symbol, String signal, String ltp, String recPrice, String sl, String change, String upcomingtrigger, String updateTime) {
        this.symbol = symbol;
        this.signal = signal;
        this.ltp = ltp;
        this.recPrice = recPrice;
        this.sl = sl;
        this.change = change;
        this.upcomingtrigger = upcomingtrigger;
        this.updateTime = updateTime;
    }

    String change;
    String upcomingtrigger;
    String updateTime;

}
*/
