package android.sud.com.entryntarget.upstockService;

import android.content.Context;
import android.sud.com.entryntarget.fragments.UpstoxConnectionFragment;
import android.sud.com.entryntarget.services.models.PlaceOrderRequest;
import android.sud.com.entryntarget.upstockService.common.interceptors.AuthenticationInterceptor;
import android.sud.com.entryntarget.upstockService.common.models.AuthHeaders;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.iface.OnTokenChange;
import android.sud.com.entryntarget.upstockService.iface.UpstoxResponseHandler;
import android.sud.com.entryntarget.upstockService.iface.UserProfileListener;
import android.sud.com.entryntarget.upstockService.login.LoginApi;
import android.sud.com.entryntarget.upstockService.login.LoginService;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.sud.com.entryntarget.webservice.ApiService;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OverTheAir {
    private static final OverTheAir ourInstance = new OverTheAir();
    private static Retrofit retrofit=null;


    public static String BASE_URL = "https://api.upstox.com";
    private static String API_KEYS = "eKwYuPYl6Q8iQKpKAYSpr39Oy3T9dL4C83cWFuYK";
    private static String API_SECRET = "llukm2xf88";
    private static String TOKEN = "Bearer e1ec1d79aac258c27664aee0b116c0db29c84fce";
    private static Context mContext;

    public static OverTheAir getInstance(Context context) {
        mContext = context;
        return ourInstance;
    }

    private OverTheAir() {
    }



    public void generateToken(final OnTokenChange mListener) throws ExecutionException, InterruptedException {
        API_KEYS = EntPreferences.getUpstoxApiKey(mContext);
        API_SECRET = EntPreferences.getUpstoxKeySecret(mContext);
        String code = EntPreferences.getUpstoxCode(mContext);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new AuthenticationInterceptor(API_KEYS, API_SECRET));
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader(API_KEYS, API_SECRET)
                        .addHeader("x-api-key", API_KEYS)
                        /*.addHeader("content-type", "application/json")
                        .addHeader("code", EntPreferences.getUpstoxCode(mContext))*/
                        .build();
                return chain.proceed(request);
            }
        });
        Retrofit mRetrofit = new Retrofit.Builder().
                addConverterFactory(GsonConverterFactory.create()).
                baseUrl(BASE_URL).
                client(httpClient.build()).build();
        /*TokenRequest tokenRequest = new TokenRequest(
                EntPreferences.getUpstoxCode(mContext),
                "authorization_code",
                "http://niftywatcher.in"
        );*/
        //AuthHeaders authHeaders = new AuthHeaders(code,API_KEYS);
        TokenRequest tokenRequest = new TokenRequest(EntPreferences.getUpstoxCode(mContext),"authorization_code","http://niftywatcher.in");
        LoginApi apiLogin = mRetrofit.create(LoginApi.class);
        Call<AccessToken> token = apiLogin.getAccessToken(API_KEYS,tokenRequest);
        token.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, retrofit2.Response<AccessToken> response) {
                if(response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {

                Log.d("","");
            }
        });
        /*ApiService api  = mRetrofit.create(ApiService.class);
        Call<CompletableFuture<AccessToken>> call = api.getAccessToken(code,tokenRequest);
        call.enqueue(new Callback<CompletableFuture<AccessToken>>() {
            @Override
            public void onResponse(Call<CompletableFuture<AccessToken>> call, retrofit2.Response<CompletableFuture<AccessToken>> response) {
                if(response.isSuccessful()) {
                    try {
                        CompletableFuture<AccessToken> tokenCompletableFuture = response.body();
                        EntPreferences.setUpstoxToken(mContext,tokenCompletableFuture.get().getToken());
                        mListener.OnTokenChangeListener(tokenCompletableFuture.get().getToken());

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<CompletableFuture<AccessToken>> call, Throwable t) {
                Log.d("errr","Error in profile");
            }
        });*/

    }

    private Retrofit getService(){
        API_KEYS = EntPreferences.getUpstoxApiKey(mContext);
        API_SECRET = EntPreferences.getUpstoxKeySecret(mContext);
        TOKEN = EntPreferences.getUpstoxToken(mContext);
        if(retrofit==null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {

                    /*Request.Builder customHeader = chain.request().newBuilder();
                    customHeader.addHeader("x-api-key",API_KEYS);
                    customHeader.addHeader("authorization",TOKEN);
                    customHeader.addHeader("content-type","application/json");*/
                    Request request = chain.request().newBuilder()
                            .addHeader("x-api-key", API_KEYS)
                            .addHeader("authorization", TOKEN)
                            .addHeader("content-type", "application/json")
                            .build();
                    return chain.proceed(request);
                }
            });
            retrofit = new Retrofit.Builder().
                    addConverterFactory(GsonConverterFactory.create()).
                    baseUrl(BASE_URL).
                    client(httpClient.build()).build();
        }
        return retrofit;
    }

    public void getUserBalance(final UpstoxResponseHandler mListener){

        ApiService api  = getService().create(ApiService.class);
        Call<UpstoxResponse<ProfileBalance>> call = api.getUserBalance();
        call.enqueue(new Callback<UpstoxResponse<ProfileBalance>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<ProfileBalance>> call, retrofit2.Response<UpstoxResponse<ProfileBalance>> response) {
                if(response.isSuccessful()) {
                    try {
                        UpstoxResponse<ProfileBalance> upstoxResponse = response.body();
                        //upstoxResponse.getData().getEquity().getAvailableMargin();
                        mListener.onResponse(upstoxResponse);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<UpstoxResponse<ProfileBalance>> call, Throwable t) {
                Log.d("errr","Error in profile");
            }
        });
    }

    public void getProfile(final UserProfileListener mListener){

        ApiService api  = getService().create(ApiService.class);
        Call<UpstoxResponse<Profile>> call = api.getUserProfile();
        call.enqueue(new Callback<UpstoxResponse<Profile>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Profile>> call, retrofit2.Response<UpstoxResponse<Profile>> response) {
                if(response.isSuccessful()) {
                    try {
                        UpstoxResponse<Profile> upstoxResponse = response.body();
                        //upstoxResponse.getData().getEquity().getAvailableMargin();
                        mListener.onUserProfileResponse(upstoxResponse);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<UpstoxResponse<Profile>> call, Throwable t) {
                Log.d("errr","Error in profile");
            }
        });
    }

    public void placeOrder(PlaceOrderRequest placeOrderRequest){

        ApiService api  = getService().create(ApiService.class);
        Call<UpstoxResponse<Profile>> call = api.getUserProfile();
        call.enqueue(new Callback<UpstoxResponse<Profile>>() {
            @Override
            public void onResponse(Call<UpstoxResponse<Profile>> call, retrofit2.Response<UpstoxResponse<Profile>> response) {
                if(response.isSuccessful()) {
                    try {
                        UpstoxResponse<Profile> upstoxResponse = response.body();
                        //upstoxResponse.getData().getEquity().getAvailableMargin();
                        //mListener.onUserProfileResponse(upstoxResponse);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<UpstoxResponse<Profile>> call, Throwable t) {
                Log.d("errr","Error in profile");
            }
        });
    }
}
