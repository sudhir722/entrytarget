package android.sud.com.entryntarget.upstockService;

public class ProfileBalance {

    private BalanceModel equity;

    private BalanceModel commodity;

    public BalanceModel getEquity() {
        return equity;
    }

    public void setEquity(BalanceModel equity) {
        this.equity = equity;
    }

    public BalanceModel getCommodity() {
        return commodity;
    }

    public void setCommodity(BalanceModel commodity) {
        this.commodity = commodity;
    }
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileBalance that = (ProfileBalance) o;
        return Objects.equals(equity, that.equity) &&
                Objects.equals(commodity, that.commodity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(equity, commodity);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("equity", equity)
                .add("commodity", commodity)
                .toString();
    }*/
}
