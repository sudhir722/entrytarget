package android.sud.com.entryntarget.upstockService;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class TokenRequest {
    @SerializedName("code")
    private final String code;

    @SerializedName("grant_type")
    private final String grantType;

    @SerializedName("redirect_uri")
    private final String redirectUri;

    public String getCode() {
        return code;
    }

    public String getGrantType() {
        return grantType;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public TokenRequest(@NonNull final String code,
                        @NonNull final String grantType,
                        @NonNull final String redirectUri) {

        this.code = Objects.requireNonNull(code);
        this.grantType = Objects.requireNonNull(grantType);
        this.redirectUri = Objects.requireNonNull(redirectUri);
    }
}
