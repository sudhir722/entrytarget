package android.sud.com.entryntarget.upstockService;

import android.os.Build;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.Set;


public class UserPosition {

    @SerializedName("client_id")
    private String clientId;

    private String name;

    private String email;

    private String phone;

    @SerializedName("exchanges_enabled")
    private Set<String> exchangesEnabled;

    @SerializedName("products_enabled")
    private Set<String> productsEnabled;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<String> getExchangesEnabled() {
        return exchangesEnabled;
    }

    public void setExchangesEnabled(Set<String> exchangesEnabled) {
        this.exchangesEnabled = exchangesEnabled;
    }

    public Set<String> getProductsEnabled() {
        return productsEnabled;
    }

    public void setProductsEnabled(Set<String> productsEnabled) {
        this.productsEnabled = productsEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPosition profile = (UserPosition) o;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.equals(clientId, profile.clientId) &&
                    Objects.equals(name, profile.name) &&
                    Objects.equals(email, profile.email) &&
                    Objects.equals(phone, profile.phone) &&
                    Objects.equals(exchangesEnabled, profile.exchangesEnabled) &&
                    Objects.equals(productsEnabled, profile.productsEnabled);
        }else{
            return false;
        }
    }

    /*@Override
    public int hashCode() {
        return Objects.hash(clientId, name, email, phone, exchangesEnabled, productsEnabled);
    }*/

    /*@Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("clientId", clientId)
                .add("name", name)
                .add("email", email)
                .add("phone", phone)
                .add("exchangesEnabled", exchangesEnabled)
                .add("productsEnabled", productsEnabled)
                .toString();
    }*/
}
