/*
 * MIT License
 *
 * Copyright (c) 2018 Rishabh Joshi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package android.sud.com.entryntarget.upstockService.common;


import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.AuthHeaders;
import android.support.annotation.NonNull;

import java.util.Objects;



/**
 * Parent class for every Service class. Holds common methods.
 */
public abstract class Service {

    protected final AccessToken accessToken;
    protected final ApiCredentials credentials;

    /**
     * @param accessToken The user's access token
     * @param credentials The user's API credentials
     */
    public Service(@NonNull final AccessToken accessToken,
                   @NonNull final ApiCredentials credentials) {

        this.accessToken = Objects.requireNonNull(accessToken);
        this.credentials = Objects.requireNonNull(credentials);
    }

    protected <T> T prepareServiceApi(@NonNull final Class<T> type) {

        //log.debug("Preparing service API: {}", type.getName());
        final String token = accessToken.getType() + " " + accessToken.getToken();
        return ServiceGenerator.getInstance()
                .createService(type, new AuthHeaders(token, credentials.getApiKey()));
    }
}
