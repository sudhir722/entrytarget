/*
 * MIT License
 *
 * Copyright (c) 2018 Rishabh Joshi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package android.sud.com.entryntarget.upstockService.common;


import android.sud.com.entryntarget.upstockService.OverTheAir;
import android.sud.com.entryntarget.upstockService.common.converters.AlwaysListTypeAdapterFactory;
import android.sud.com.entryntarget.upstockService.common.converters.NumberString;
import android.sud.com.entryntarget.upstockService.common.converters.NumberStringDeserializer;
import android.sud.com.entryntarget.upstockService.common.converters.NumberStringSerializer;
import android.sud.com.entryntarget.upstockService.common.interceptors.AuthenticationInterceptor;
import android.sud.com.entryntarget.upstockService.common.models.AuthHeaders;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.sud.com.entryntarget.upstockService.common.constants.PropertyKeys.RIKO_CONNECT_TIMEOUT;
import static android.sud.com.entryntarget.upstockService.common.constants.PropertyKeys.RIKO_READ_TIMEOUT;
import static android.sud.com.entryntarget.upstockService.common.constants.PropertyKeys.RIKO_WRITE_TIMEOUT;

;


public class ServiceGenerator {


    private ServiceGenerator() {
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(NumberString.class, new NumberStringSerializer())
                .registerTypeAdapter(NumberString.class, new NumberStringDeserializer())
                .registerTypeAdapterFactory(new AlwaysListTypeAdapterFactory())
                .create();
        final String readTimeout = System.getProperty(RIKO_READ_TIMEOUT);
        final String writeTimeout = System.getProperty(RIKO_WRITE_TIMEOUT);
        final String connectTimeout = System.getProperty(RIKO_CONNECT_TIMEOUT);
        this.httpClient = new OkHttpClient.Builder();
        if (!TextUtils.isEmpty(readTimeout)) {
            this.httpClient.readTimeout(
                    Long.parseLong(readTimeout), TimeUnit.SECONDS);
        }
        if (!TextUtils.isEmpty(connectTimeout)) {
            this.httpClient.connectTimeout(
                    Long.parseLong(connectTimeout), TimeUnit.SECONDS);
        }
        if (!TextUtils.isEmpty(writeTimeout)) {
            this.httpClient.writeTimeout(
                    Long.parseLong(writeTimeout), TimeUnit.SECONDS);
        }
        /*final HttpUrl.Builder urlBuilder = new HttpUrl.Builder()
                .scheme(System.getProperty(RIKO_SERVER_SCHEME, RIKO_SERVER_SCHEME_DEFAULT))
                .host(System.getProperty(RIKO_SERVER_URL, RIKO_SERVER_URL_DEFAULT));
        final String port = System.getProperty(RIKO_SERVER_PORT);*/
        /*if (!TextUtils.isEmpty(port)) {
            urlBuilder.port(Integer.parseInt(port));
        }*/
        this.builder =
                new Retrofit.Builder()
                        .baseUrl(OverTheAir.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(httpClient.build());
        this.retrofit = builder.build();
    }

    public static final ServiceGenerator instance = new ServiceGenerator();

    public static ServiceGenerator getInstance() {
        return ServiceGenerator.instance;
    }

    private OkHttpClient.Builder httpClient;

    private Retrofit.Builder builder;

    private Retrofit retrofit;

    /**
     * A helper method for unit testing, allowing for random base URLs to be used per test.
     * <em>SHOULD NOT BE USED ON OUTSIDE OF UNIT TESTS</em>
     *
     * @param url The base URL to use.
     */
    public void rebuildWithUrl(HttpUrl url) {
        builder.client(httpClient.build());
        builder.baseUrl(url);
        retrofit = builder.build();
    }

    /**
     * Create service without authentication.
     *
     * @param serviceClass The Service
     * @param <S>          The type of Service
     * @return The retrofitted service
     */
    public <S> S createService(@NonNull final Class<S> serviceClass) {

        //log.debug("Creating service without authentication");
        return createService(Objects.requireNonNull(serviceClass), null, null);
    }

    /**
     * Create service with Basic Authentication
     *
     * @param serviceClass The Service
     * @param username     Username for basic authentication
     * @param password     Password for basic authentication
     * @param <S>          The type of Service
     * @return The retrofitted service
     */
    public <S> S createService(@NonNull final Class<S> serviceClass,
                               @Nullable final String username,
                               @Nullable final String password) {

        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(Objects.requireNonNull(serviceClass),
                    new AuthHeaders(authToken, username));
        }
        // Setup request headers without any auth
        return createService(Objects.requireNonNull(serviceClass), null);
    }

    /**
     * Create service with Token Authorization
     *
     * @param serviceClass The Service
     * @param headers      The Authentication headers
     * @param <S>          The type of Service
     * @return The retrofitted service
     */
    public <S> S createService(@NonNull final Class<S> serviceClass,
                               @Nullable final AuthHeaders headers) {

        enableAuthentication(headers);
        /*if (log.isDebugEnabled()) {
            enableHttpLogging();
        }*/
        //log.debug("Creating service with authorization headers");

        return retrofit.create(Objects.requireNonNull(serviceClass));
    }

    private void enableAuthentication(final AuthHeaders headers) {

        if (null != headers
                && !TextUtils.isEmpty(headers.getToken())
                && !TextUtils.isEmpty(headers.getApiKey())) {

            //log.debug("Adding auth headers interceptor");
            addInterceptor(
                    new AuthenticationInterceptor(
                            headers.getToken(), headers.getApiKey()));
        }
    }


    private void addInterceptor(Interceptor interceptor) {

        if (!httpClient.interceptors().contains(interceptor)) {
            httpClient.addInterceptor(interceptor);

            builder.client(httpClient.build());
            retrofit = builder.build();
        }
    }

}
