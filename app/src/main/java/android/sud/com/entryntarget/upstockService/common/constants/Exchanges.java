package android.sud.com.entryntarget.upstockService.common.constants;

public final class Exchanges {

    public static final String BSE_INDEX = "bse_index";
    public static final String NSE_INDEX = "nse_index";
    public static final String BSE_EQUITY = "bse_eq";
    public static final String BSE_CURRENCY_FnO = "bcd_fo";
    public static final String NSE_EQUITY = "nse_eq";
    public static final String NSE_FnO = "nse_fo";
    public static final String NSE_CURRENCY_FnO = "ncd_fo";
    public static final String MCX_FUTURES = "mcx_fo";
}
