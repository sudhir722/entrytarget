package android.sud.com.entryntarget.upstockService.common.constants;

public final class OhlcIntervals {

    public static final String ONE_MINUTE = "1MINUTE";
    public static final String FIVE_MINUTES = "5MINUTE";
    public static final String TEN_MINUTES = "10MINUTE";
    public static final String THIRTY_MINUTES = "30MINUTE";
    public static final String SIXTY_MINUTES = "60MINUTE";
    public static final String ONE_DAY = "1DAY";
    public static final String ONE_WEEK = "1WEEK";
    public static final String ONE_MONTH = "1MONTH";

}
