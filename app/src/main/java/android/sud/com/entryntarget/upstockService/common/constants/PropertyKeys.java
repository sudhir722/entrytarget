package android.sud.com.entryntarget.upstockService.common.constants;

public class PropertyKeys {
    public static final String RIKO_READ_TIMEOUT = "riko.read.timeout";
    public static final int RIKO_READ_TIMEOUT_DEFAULT = 10;

    public static final String RIKO_WRITE_TIMEOUT = "riko.write.timeout";
    public static final int RIKO_WRITE_TIMEOUT_DEFAULT = 10;

    public static final String RIKO_CONNECT_TIMEOUT = "riko.connect.timeout";
    public static final int RIKO_CONNECT_TIMEOUT_DEFAULT = 10;

    public static final String RIKO_SERVER_SCHEME = "riko.server.scheme";
    public static final String RIKO_SERVER_SCHEME_DEFAULT = "https";

    public static final String RIKO_SERVER_URL = "riko.server.url";
    public static final String RIKO_SERVER_URL_DEFAULT = "api.upstox.com";

    public static final String RIKO_SERVER_PORT = "riko.server.port";
    public static final int RIKO_SERVER_PORT_DEFAULT = 443;

    public static final String RIKO_WS_SERVER_SCHEME = "riko.ws.server.scheme";
    public static final String RIKO_WS_SERVER_SCHEME_DEFAULT = "https";

    public static final String RIKO_WS_SERVER_URL = "riko.ws.server.url";
    public static final String RIKO_WS_SERVER_URL_DEFAULT = "ws-api.upstox.com";

    public static final String RIKO_WS_SERVER_PORT = "riko.ws.server.port";
    public static final int RIKO_WS_SERVER_PORT_DEFAULT = 443;

    public static final String RIKO_WS_RECONNECT = "riko.ws.reconnect";
    public static final String RIKO_WS_RECONNECT_DEFAULT = "true";
}
