/*
 * MIT License
 *
 * Copyright (c) 2018 Rishabh Joshi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package android.sud.com.entryntarget.upstockService.feed;


import android.content.Context;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.common.Service;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.feed.models.Feed;
import android.sud.com.entryntarget.upstockService.feed.models.SubscriptionResponse;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedService extends Service {


    public FeedService(@NonNull final AccessToken accessToken,
                       @NonNull final ApiCredentials credentials) {

        super(Objects.requireNonNull(accessToken), Objects.requireNonNull(credentials));
    }

    /**
     * Get live feed information about a single instrument.
     *
     * @param exchange Name of the exchange. <em>Mandatory.</em>
     * @param symbol   Trading symbol. <em>Mandatory.</em>
     * @param type     'ltp' or 'full'. <em>Either. Mandatory.</em>
     * @return Instant of a live Feed
     */
    public Call<UpstoxResponse<Feed>> liveFeed(@NonNull final String exchange,
                                               @NonNull final String symbol,
                                               @NonNull final String type) {

        //log.debug("Validate parameters - GET Live Feed");
        validatePathParameters(exchange, symbol, type);

        //log.debug("Preparing service - GET Live Feed");
        final FeedApi api = prepareServiceApi(FeedApi.class);

        //log.debug("Making request - GET Live Feed");
        return api.liveFeed(exchange, symbol, type);
    }

    /**
     * Subscribe to the feed.
     *
     * @param type       'ltp' or 'full'. <em>Either. Mandatory.</em>
     * @param exchange   Name of the exchange. <em>Mandatory.</em>
     * @param symbolsCsv Comma separated list of trading symbols
     * @return Confirmation response
     */
    public Call<UpstoxResponse<SubscriptionResponse>> subscribe(@NonNull final String type,
                                      @NonNull final String exchange,
                                      @NonNull final String symbolsCsv) {

        //log.debug("Validate parameters - GET Subscribe");
        validatePathParameters(exchange, type, symbolsCsv);

        //log.debug("Preparing service - GET Subscribe");
        final FeedApi api = prepareServiceApi(FeedApi.class);

        //log.debug("Making request - GET Subscribe");
        return api.subscribe(type, exchange, symbolsCsv);
    }

    /**
     * Unsubscribe to the feed.
     *
     * @param type       'ltp' or 'full'. <em>Either. Mandatory.</em>
     * @param exchange   Name of the exchange. <em>Mandatory.</em>
     * @param symbolsCsv Comma separated list of trading symbols
     * @return Confirmation response
     */
/*    public CompletableFuture<UpstoxResponse<SubscriptionResponse>> unsubscribe(@NonNull final String type,
                                                                               @NonNull final String exchange,
                                                                               @NonNull final String symbolsCsv) {

        //log.debug("Validate parameters - GET Unsubscribe");
        validatePathParameters(exchange, type, symbolsCsv);

        //log.debug("Preparing service - GET Unsubscribe");
        final FeedApi api = prepareServiceApi(FeedApi.class);

        //log.debug("Making request - GET Unsubscribe");
        return api.unsubscribe(type, exchange, symbolsCsv);
    }*/

    private void validatePathParameters(String... values) {
        for (String value : values) {
            if (TextUtils.isEmpty(value)) {
                Log.d("upstoxerr","Argument validation failed. " +
                        "Arguments 'exchange', 'symbol(s)' and 'type' are mandatory.");
                throw new IllegalArgumentException(
                        "Arguments 'exchange', 'symbol(s)' and 'type' are mandatory. " +
                                "They cannot be null nor empty.");
            }
        }
    }

}
