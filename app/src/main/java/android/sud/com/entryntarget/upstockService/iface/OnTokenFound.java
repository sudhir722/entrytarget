package android.sud.com.entryntarget.upstockService.iface;

import android.sud.com.entryntarget.upstockService.AccessToken;

public interface OnTokenFound {
    public void onTokenFound(AccessToken token);
}
