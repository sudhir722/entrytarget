package android.sud.com.entryntarget.upstockService.iface;

import android.sud.com.entryntarget.upstockService.ProfileBalance;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;

public interface UpstoxResponseHandler {

    public void onResponse(UpstoxResponse<ProfileBalance> response);
}
