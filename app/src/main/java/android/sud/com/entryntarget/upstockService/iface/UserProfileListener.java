package android.sud.com.entryntarget.upstockService.iface;

import android.sud.com.entryntarget.upstockService.Profile;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;

public interface UserProfileListener {

    public void onUserProfileResponse(UpstoxResponse<Profile> response);
}
