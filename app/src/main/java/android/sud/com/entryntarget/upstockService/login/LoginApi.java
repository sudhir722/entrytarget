package android.sud.com.entryntarget.upstockService.login;

import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.TokenRequest;

import com.google.gson.JsonObject;

import java.util.concurrent.CompletableFuture;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginApi {
    /**
     * Retrieves the access code from Upstox Authorization URL through a synchronous call.<br>
     *
     * @param tokenRequest The token request, including the access code.
     * @return A CompletableFuture to execute the request (a)synchronously.
     */
    @Headers("Content-Type: application/json")
    @POST("/index/oauth/token")
    Call<AccessToken> getAccessToken(@Body TokenRequest tokenRequest);

    @Headers("Content-Type: application/json")
    @POST("/index/oauth/token")
    Call<AccessToken> getAccessToken(@Body JsonObject locationPost);

    @Headers("Content-Type: application/json")
    @POST("/index/oauth/token")
    Call<AccessToken> getAccessToken(@Header("x-api-key") String apikey, @Body TokenRequest tokenRequest);
}
