package android.sud.com.entryntarget.upstockService.login;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.TokenRequest;
import android.sud.com.entryntarget.upstockService.common.Service;
import android.sud.com.entryntarget.upstockService.common.ServiceGenerator;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.iface.OnTokenFound;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonObject;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginService {

    private final TokenRequest request;

    private final ApiCredentials credentials;

    /**
     * @param request     The TokenRequest object containing all fields,
     *                    including the access code received after authenticating the user on Upstox site.
     * @param credentials The API key and secret.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public LoginService(@NonNull final TokenRequest request,
                        @NonNull final ApiCredentials credentials) {

        this.request = Objects.requireNonNull(request);
        this.credentials = Objects.requireNonNull(credentials);
    }

    /**
     * Retrieves the access code from Upstox Authorization URL through a synchronous call.<br>
     *
     * @return An 'optional' AccessToken. Return object is empty in case of error.
     */
    public void getAccessToken(Context contex, final OnTokenFound mTokenListener) throws ExecutionException, InterruptedException {

        if (TextUtils.isEmpty(request.getCode())) {
            throw new IllegalArgumentException(
                    "Missing value for authorization code. Code: " + request.getCode());
        }

        // Create a very simple REST adapter which points the Upstox API endpoint.
        LoginApi loginApi =
                ServiceGenerator.getInstance().createService(
                        LoginApi.class,
                        credentials.getApiKey(),
                        credentials.getApiSecret());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("code", EntPreferences.getUpstoxCode(contex));
        jsonObject.addProperty("grant_type","authorization_code");
        jsonObject.addProperty("redirect_uri","http://niftywatcher.in");

        Call<AccessToken> future = loginApi.getAccessToken(jsonObject);
        future.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                try{
                    AccessToken token = response.body();
                    mTokenListener.onTokenFound(token);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                Log.d("Err","Error found");
                mTokenListener.onTokenFound(null);
            }

        });
    }

}
