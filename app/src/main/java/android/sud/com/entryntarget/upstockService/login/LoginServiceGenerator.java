package android.sud.com.entryntarget.upstockService.login;

import android.sud.com.entryntarget.upstockService.OverTheAir;
import android.sud.com.entryntarget.upstockService.common.ServiceGenerator;
import android.text.TextUtils;
import android.util.Base64;

import com.squareup.okhttp.OkHttpClient;

import okhttp3.Credentials;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginServiceGenerator {
    public static final String API_BASE_URL = OverTheAir.BASE_URL;

    private static final LoginServiceGenerator instance = new LoginServiceGenerator();

    public static LoginServiceGenerator getInstance() {
        return LoginServiceGenerator.instance;
    }

    private okhttp3.OkHttpClient.Builder httpClient = new okhttp3.OkHttpClient.Builder();

    //private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public  <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null, null);
    }

    public  <S> S createService(
            Class<S> serviceClass, String username, String password) {
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(serviceClass, authToken);
        }

        return createService(serviceClass, null);
    }

    public <S> S createService(
            Class<S> serviceClass, final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptorLogin interceptor =
                    new AuthenticationInterceptorLogin(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit.create(serviceClass);
    }
}
