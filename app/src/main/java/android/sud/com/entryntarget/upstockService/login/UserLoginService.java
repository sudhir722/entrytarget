package android.sud.com.entryntarget.upstockService.login;

import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.TokenRequest;
import android.sud.com.entryntarget.upstockService.common.Service;
import android.sud.com.entryntarget.upstockService.common.ServiceGenerator;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.AuthHeaders;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.Objects;

import retrofit2.Call;

public class UserLoginService extends Service {

    /**
     * @param accessToken The user's access token
     * @param credentials The user's API credentials
     */
    public UserLoginService(@NonNull final AccessToken accessToken,
                       @NonNull final ApiCredentials credentials) {

        super(Objects.requireNonNull(accessToken), Objects.requireNonNull(credentials));
    }

    private void validatePathParameters(String... values) {
        for (String value : values) {
            if (TextUtils.isEmpty(value)) {
                Log.d("LoginApi","Argument validation failed. " +
                        "Arguments 'exchange', 'symbol(s)' and 'type' are mandatory.");
                throw new IllegalArgumentException(
                        "Arguments 'exchange', 'symbol(s)' and 'type' are mandatory. " +
                                "They cannot be null nor empty.");
            }
        }
    }

    public Call<AccessToken> accessToken(@NonNull final TokenRequest tokenRequest) {

        //log.debug("Validate parameters - GET Live Feed");
        //validatePathParameters(exchange, symbol, type);

        //log.debug("Preparing service - GET Live Feed");
        final LoginApi api = prepareServiceApi(LoginApi.class);

        //log.debug("Making request - GET Live Feed");
        return api.getAccessToken(tokenRequest);
    }


    /*private void validateSymbolAndToken(final String symbol, final String token) {
        if (Strings.isNullOrEmpty(symbol) && Strings.isNullOrEmpty(token)) {
            log.error("Argument validation failed. Either 'symbol' or 'token' must be specified.");
            throw new IllegalArgumentException("Provide either the 'symbol' or 'token'. Both cannot be null nor empty.");
        }
    }*/

    /*private void validateExchange(String exchange) {
        if (Strings.isNullOrEmpty(exchange)) {
            log.error("Argument validation failed. Argument 'exchange' is mandatory.");
            throw new IllegalArgumentException("Argument 'exchange' is mandatory. It cannot be null nor empty.");
        }
    }*/

    protected <T> T prepareServiceApi(@NonNull final Class<T> type) {

        //log.debug("Preparing service API: {}", type.getName());
        final String token = accessToken.getType() + " " + accessToken.getToken();
        return ServiceGenerator.getInstance()
                .createService(type, new AuthHeaders(token, credentials.getApiKey()));
    }

}
