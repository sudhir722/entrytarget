package android.sud.com.entryntarget.upstockService.orders;

import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.common.Service;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.orders.models.Order;
import android.sud.com.entryntarget.upstockService.orders.models.OrderRequest;
import android.sud.com.entryntarget.upstockService.orders.models.Trade;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import retrofit2.Call;

public class OrderService extends Service {

    /**
     * @param accessToken The user's access token
     * @param credentials The user's API credentials
     */
    public OrderService(@NonNull final AccessToken accessToken,
                        @NonNull final ApiCredentials credentials) {

        super(Objects.requireNonNull(accessToken), Objects.requireNonNull(credentials));
    }

    /**
     * Fetches the list of orders placed by the user.
     *
     * @return List of Order
     */
    public Call<UpstoxResponse<List<Order>>> getOrderHistory() {

        //log.debug("Preparing service - GET Order History");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - GET Order History");
        return api.getOrderHistory();
    }

    /**
     * Fetches the details of the particular order the user has placed.
     *
     * @param orderId The id of the order whose details need to be fetched.
     * @return List of Order
     */
    public Call<UpstoxResponse<List<Order>>> getOrderDetails(@NonNull final String orderId) {

        //log.debug("Validate parameters - GET Order Details");
        validateOrderId(orderId);

        //log.debug("Preparing service - GET Order Details");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - GET Order Details");
        return api.getOrderDetails(orderId);
    }

    /**
     * Fetches the trades for the current day.
     *
     * @return List of Trade
     */
    public Call<UpstoxResponse<List<Trade>>> getTradeBook() {

        //log.debug("Preparing service - GET Trade Book");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - GET Trade Book");
        return api.getTradeBook();
    }

    /**
     * Fetches the trades for the given order.
     *
     * @param orderId The id of the order whose trade history need to be fetched.
     * @return List of Trade
     */
    public Call<UpstoxResponse<List<Trade>>> getTradeHistory(@NonNull final String orderId) {

        //log.debug("Validate parameters - GET Trade History");
        validateOrderId(orderId);

        //log.debug("Preparing service - GET Trade History");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - GET Trade History");
        return api.getTradeHistory(orderId);
    }

    /**
     * Place an order to the exchange via Upstox.
     *
     * @param request The order request
     * @return The creates order
     */
    public Call<UpstoxResponse<Order>> placeOrder(@NonNull final OrderRequest request) {

        //log.debug("Validate parameters - POST Place Order");
        validateOrderRequest(request);

        //log.debug("Preparing service - POST Place Order");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - POST Place Order");
        return api.placeOrder(request);
    }

    /**
     * Modify the order.
     *
     * @param orderId The id of the order to be modified
     * @param request The order request
     * @return The modified order
     */
    public Call<UpstoxResponse<Order>> modifyOrder(@NonNull final String orderId,
                                                                @NonNull final OrderRequest request) {

        //log.debug("Validate parameters - PUT Modify Order");
        validateOrderId(orderId);
        validateOrderRequest(request);

        //log.debug("Preparing service - PUT Modify Order");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - PUT Modify Order");
        return api.modifyOrder(orderId, request);
    }

    /**
     * Cancel a single or multiple orders.
     *
     * @param orderIdsCsv The comma separated string of order ids that need to cancelled.
     * @return The comma separate string of affected orders
     */
    public Call<UpstoxResponse<String>> cancelOrders(@NonNull final String orderIdsCsv) {

        //log.debug("Validate parameters - DELETE Orders");
        validateOrderId(orderIdsCsv);

        //log.debug("Preparing service - DELETE Orders");
        final OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - DELETE Orders");
        return api.cancelOrders(orderIdsCsv);
    }

    /**
     * Cancel all open orders.
     *
     * @return The comma separate string of affected orders
     */
    public Call<UpstoxResponse<String>> cancelAllOrders() {

        //log.debug("Preparing service - DELETE All Orders");
        OrderApi api = prepareServiceApi(OrderApi.class);

        //log.debug("Making request - DELETE All Orders");
        return api.cancelAllOrders();
    }

    private void validateOrderRequest(OrderRequest request) {
        if (null == request) {
            //log.error("Order placement request parameters are missing.");
            throw new IllegalArgumentException("Order placement request parameters are missing.");
        }
    }

    private void validateOrderId(String orderId) {
        if (TextUtils.isEmpty(orderId)) {
            //log.error("Argument validation failed. Argument 'orderId' is mandatory.");
            throw new IllegalArgumentException("Argument 'orderId' is mandatory. It cannot be null nor empty.");
        }
    }
}
