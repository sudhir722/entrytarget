package android.sud.com.entryntarget.upstockService.users;

import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.common.Service;
import android.sud.com.entryntarget.upstockService.common.models.ApiCredentials;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;
import android.sud.com.entryntarget.upstockService.users.models.Contract;
import android.sud.com.entryntarget.upstockService.users.models.Holding;
import android.sud.com.entryntarget.upstockService.users.models.Position;
import android.sud.com.entryntarget.upstockService.users.models.Profile;
import android.sud.com.entryntarget.upstockService.users.models.ProfileBalance;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import retrofit2.Call;


public class UserService extends Service {


    /**
     * @param accessToken The user's access token
     * @param credentials The user's API credentials
     */
    public UserService(@NonNull final AccessToken accessToken,
                       @NonNull final ApiCredentials credentials) {

        super(Objects.requireNonNull(accessToken), Objects.requireNonNull(credentials));
    }

    /**
     * Retrieves the user's profile
     *
     * @return User's profile details. It will be empty if request 'returns' an error.
     */
    public Call<UpstoxResponse<Profile>> getProfile() {

        //log.debug("Preparing service - GET Profile");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET Profile");
        return api.getProfile();
    }

    /**
     * Retrieve the profile balance.
     *
     * @param accountType The account type - 'security' or 'commodity'
     * @return the user's account balance.
     */
    public Call<UpstoxResponse<ProfileBalance>> getProfileBalance(
            @Nullable final String accountType) {

        //log.debug("Preparing service - GET Profile Balance");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET Profile Balance");
        return api.getProfileBalance(accountType);
    }

    /**
     * Fetches the current positions for the user for the current day.
     *
     * @return List of Position
     */
    public Call<UpstoxResponse<List<Position>>> getPositions() {

        //log.debug("Preparing service - GET Positions");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET Positions");
        return api.getPositions();
    }

    /**
     * Fetches the holdings which the user has bought/sold in previous trading sessions.
     *
     * @return List of Holding
     */
    public Call<UpstoxResponse<List<Holding>>> getHoldings() {

        //log.debug("Preparing service - GET Holdings");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET Holdings");
        return api.getHoldings();
    }

    /**
     * Get all available contracts as a Stream.
     *
     * @param exchange Name of the exchange. <em>Mandatory</em>. Valid values are:<br/>
     *                 <ul>
     *                 <li><code>bse_index</code> - BSE Index</li>
     *                 <li><code>nse_index</code> - NSE Index</li>
     *                 <li><code>bse_eq</code> - BSE Equity</li>
     *                 <li><code>bcd_fo</code> - BSE Currency Futures & Options</li>
     *                 <li><code>nse_eq</code> - NSE Equity</li>
     *                 <li><code>nse_fo</code> - NSE Futures & Options</li>
     *                 <li><code>ncd_fo</code> - NSE Currency Futures & Options</li>
     *                 <li><code>mcx_fo</code> - MCX Futures</li>
     *                 </ul>
     * @return The Json response from Upstox as a Stream, because the Json could be very large.
     * @throws IllegalArgumentException if exchange is not specified.
     * @implNote You'll need to extract the <em>data</em> element from the stream.
     * The <em>data</em> element is a list of strings.
     * The first item in the list contains the headers for the CSV.
     * Rest of the items in the list are the individual CSV records.
     */
    /*public Call<InputStream> getAllMasterContracts(@NonNull final String exchange) {

        //log.debug("Validate parameters - GET All Contracts");
        validateExchange(exchange);

        //log.debug("Preparing service - GET All Contracts");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET All Contracts");
        return api.getAllMasterContracts(exchange);
                //.thenApply(ResponseBody::byteStream);
    }*/

    /**
     * Get available contract for given symbol/token.
     *
     * @param exchange Name of the exchange. <em>Mandatory</em>. Valid values are:<br/>
     *                 <ul>
     *                 <li><code>bse_index</code> - BSE Index</li>
     *                 <li><code>nse_index</code> - NSE Index</li>
     *                 <li><code>bse_eq</code> - BSE Equity</li>
     *                 <li><code>bcd_fo</code> - BSE Currency Futures & Options</li>
     *                 <li><code>nse_eq</code> - NSE Equity</li>
     *                 <li><code>nse_fo</code> - NSE Futures & Options</li>
     *                 <li><code>ncd_fo</code> - NSE Currency Futures & Options</li>
     *                 <li><code>mcx_fo</code> - MCX Futures</li>
     *                 </ul>
     * @param symbol   Trading symbol which could be a combination of symbol name,
     *                 instrument, expiry date, etc. Optional if token is provided.
     * @param token    Unique indentifier within an exchange. Optional, if symbol is provided.
     * @throws IllegalArgumentException if exchange is not specified.
     *                                  Also, if both symbol and token are null or empty.
     */
    public Call<UpstoxResponse<Contract>> getMasterContract(@NonNull final String exchange,
                                                                         @Nullable final String symbol,
                                                                         @Nullable final String token) {

        //log.debug("Validate parameters - GET Contract");
        validateExchange(exchange);
        validateSymbolAndToken(symbol, token);

        //log.debug("Preparing service - GET Contract");
        final UsersApi api = prepareServiceApi(UsersApi.class);

        //log.debug("Making request - GET Contract");
        return api.getMasterContract(exchange, symbol, token);
    }

    private void validateSymbolAndToken(final String symbol, final String token) {
        if (TextUtils.isEmpty(symbol) && TextUtils.isEmpty(token)) {
            //log.error("Argument validation failed. Either 'symbol' or 'token' must be specified.");
            throw new IllegalArgumentException("Provide either the 'symbol' or 'token'. Both cannot be null nor empty.");
        }
    }

    private void validateExchange(String exchange) {
        if (TextUtils.isEmpty(exchange)) {
            //log.error("Argument validation failed. Argument 'exchange' is mandatory.");
            throw new IllegalArgumentException("Argument 'exchange' is mandatory. It cannot be null nor empty.");
        }
    }

}
