package android.sud.com.entryntarget.upstockService.users.models;


import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.Objects;

public class Position {

    private String exchange;

    private String product;

    private String symbol;

    private BigInteger token;

    @SerializedName("buy_amount")
    private double buyAmount;

    @SerializedName("sell_amount")
    private double sellAmount;

    @SerializedName("buy_quantity")
    private int buyQuantity;

    @SerializedName("sell_quantity")
    private int sellQuantity;

    @SerializedName("cf_buy_amount")
    private double cfBuyAmount;

    @SerializedName("cf_sell_amount")
    private double cfSellAmount;

    @SerializedName("cf_buy_quantity")
    private int cfBuyQuantity;

    @SerializedName("cf_sell_quantity")
    private int cfSellQuantity;

    @SerializedName("avg_buy_price")
    private String averageBuyPrice;

    @SerializedName("avg_sell_price")
    private String averageSellPrice;

    @SerializedName("net_quantity")
    private int netQuantity;

    @SerializedName("close_price")
    private double closePrice;

    @SerializedName("last_traded_price")
    private double lastTradedPrice;

    @SerializedName("realized_profit")
    @Nullable
    private String realizedProfit;

    @SerializedName("unrealized_profit")
    @Nullable
    private String unrealizedProfit;

    @SerializedName("cf_avg_price")
    @Nullable
    private String cfAveragePrice;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigInteger getToken() {
        return token;
    }

    public void setToken(BigInteger token) {
        this.token = token;
    }

    public double getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(double buyAmount) {
        this.buyAmount = buyAmount;
    }

    public double getSellAmount() {
        return sellAmount;
    }

    public void setSellAmount(double sellAmount) {
        this.sellAmount = sellAmount;
    }

    public int getBuyQuantity() {
        return buyQuantity;
    }

    public void setBuyQuantity(int buyQuantity) {
        this.buyQuantity = buyQuantity;
    }

    public int getSellQuantity() {
        return sellQuantity;
    }

    public void setSellQuantity(int sellQuantity) {
        this.sellQuantity = sellQuantity;
    }

    public double getCfBuyAmount() {
        return cfBuyAmount;
    }

    public void setCfBuyAmount(double cfBuyAmount) {
        this.cfBuyAmount = cfBuyAmount;
    }

    public double getCfSellAmount() {
        return cfSellAmount;
    }

    public void setCfSellAmount(double cfSellAmount) {
        this.cfSellAmount = cfSellAmount;
    }

    public int getCfBuyQuantity() {
        return cfBuyQuantity;
    }

    public void setCfBuyQuantity(int cfBuyQuantity) {
        this.cfBuyQuantity = cfBuyQuantity;
    }

    public int getCfSellQuantity() {
        return cfSellQuantity;
    }

    public void setCfSellQuantity(int cfSellQuantity) {
        this.cfSellQuantity = cfSellQuantity;
    }

    public String getAverageBuyPrice() {
        return averageBuyPrice;
    }

    public void setAverageBuyPrice(String averageBuyPrice) {
        this.averageBuyPrice = averageBuyPrice;
    }

    public String getAverageSellPrice() {
        return averageSellPrice;
    }

    public void setAverageSellPrice(String averageSellPrice) {
        this.averageSellPrice = averageSellPrice;
    }

    public int getNetQuantity() {
        return netQuantity;
    }

    public void setNetQuantity(int netQuantity) {
        this.netQuantity = netQuantity;
    }

    public double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(double closePrice) {
        this.closePrice = closePrice;
    }

    public double getLastTradedPrice() {
        return lastTradedPrice;
    }

    public void setLastTradedPrice(double lastTradedPrice) {
        this.lastTradedPrice = lastTradedPrice;
    }

    public String getRealizedProfit() {
        return realizedProfit;
    }

    public void setRealizedProfit(String realizedProfit) {
        this.realizedProfit = realizedProfit;
    }

    public String getUnrealizedProfit() {
        return unrealizedProfit;
    }

    public void setUnrealizedProfit(String unrealizedProfit) {
        this.unrealizedProfit = unrealizedProfit;
    }

    public String getCfAveragePrice() {
        return cfAveragePrice;
    }

    public void setCfAveragePrice(String cfAveragePrice) {
        this.cfAveragePrice = cfAveragePrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(exchange, position.exchange) &&
                Objects.equals(product, position.product) &&
                Objects.equals(symbol, position.symbol) &&
                Objects.equals(token, position.token) &&
                Objects.equals(buyAmount, position.buyAmount) &&
                Objects.equals(sellAmount, position.sellAmount) &&
                Objects.equals(buyQuantity, position.buyQuantity) &&
                Objects.equals(sellQuantity, position.sellQuantity) &&
                Objects.equals(cfBuyAmount, position.cfBuyAmount) &&
                Objects.equals(cfSellAmount, position.cfSellAmount) &&
                Objects.equals(cfBuyQuantity, position.cfBuyQuantity) &&
                Objects.equals(cfSellQuantity, position.cfSellQuantity) &&
                Objects.equals(averageBuyPrice, position.averageBuyPrice) &&
                Objects.equals(averageSellPrice, position.averageSellPrice) &&
                Objects.equals(netQuantity, position.netQuantity) &&
                Objects.equals(closePrice, position.closePrice) &&
                Objects.equals(lastTradedPrice, position.lastTradedPrice) &&
                Objects.equals(realizedProfit, position.realizedProfit) &&
                Objects.equals(unrealizedProfit, position.unrealizedProfit) &&
                Objects.equals(cfAveragePrice, position.cfAveragePrice);
    }

    @Override
    public int hashCode() {

        return Objects.hash(exchange, product, symbol, token, buyAmount, sellAmount, buyQuantity,
                sellQuantity, cfBuyAmount, cfSellAmount, cfBuyQuantity, cfSellQuantity, averageBuyPrice,
                averageSellPrice, netQuantity, closePrice, lastTradedPrice, realizedProfit, unrealizedProfit,
                cfAveragePrice);
    }

    /*@Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("exchange", exchange)
                .add("product", product)
                .add("symbol", symbol)
                .add("token", token)
                .add("buyAmount", buyAmount)
                .add("sellAmount", sellAmount)
                .add("buyQuantity", buyQuantity)
                .add("sellQuantity", sellQuantity)
                .add("cfBuyAmount", cfBuyAmount)
                .add("cfSellAmount", cfSellAmount)
                .add("cfBuyQuantity", cfBuyQuantity)
                .add("cfSellQuantity", cfSellQuantity)
                .add("averageBuyPrice", averageBuyPrice)
                .add("averageSellPrice", averageSellPrice)
                .add("netQuantity", netQuantity)
                .add("closePrice", closePrice)
                .add("lastTradedPrice", lastTradedPrice)
                .add("realizedProfit", realizedProfit)
                .add("unrealizedProfit", unrealizedProfit)
                .add("cfAveragePrice", cfAveragePrice)
                .toString();
    }*/
}
