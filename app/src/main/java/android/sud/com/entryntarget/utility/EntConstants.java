package android.sud.com.entryntarget.utility;

public class EntConstants {
    public static final String KEY_USER_ID = "key_user_id";
    public static final int ACTION_LOGIN_SUCCESS = 1001;
    public static final int ACTION_LOGIN_FAIL = 1002;


    public static final boolean ACTION_VIEW = false;
    public static final boolean ACTION_ORDER_OCO = true;
    public static final boolean ACTION_ORDER_FULL = false;

    public static final int ACTION_TRIGGER_NONE = 0;
    public static final int ACTION_TRIGGER_BUY = 1;
    public static final int ACTION_TRIGGER_SELL = 2;


    //public static final double FM_T1 = 0.125;
    public static final double FM_T1 = 0.1875;
    public static final double FM_T2 = 0.25;
    public static final double FM_T3 = 0.3125;
    public static final double FM_T4 = 0.375;
    public static final double FM_T5 = 0.4375;
    public static final double FM_T6 = 0.5;

    public static final String CONST_BUY = "b";
    public static final String CONST_SELL = "s";

    public static final String DASHBOARD_ACION = "dash_action";


    public static final String IS_PAID = "is_paid";

}
