package android.sud.com.entryntarget.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * The type Nxt preferences.
 */
public class EntPreferences {

    private static final String PREF_KEY = "EntPreferences";
    private static final String IS_REG = "reg_status";
    private static final String DASHBOARD_ACTION = "dash_action";

    private static final String UPSTOX_SETUP_URL = "setup_upstox";
    private static final String UPSTOX_CODE = "upstox_code";
    private static final String UPSTOX_TOKEN = "upstox_token";
    private static final String UPSTOX_API_KEY = "upstox_api_key";
    private static final String UPSTOX_KEY_SECRET = "upstox_secret_key";
    private static final String UPSTOX_KEY_ORDER_TYPE = "upstox_order_type";
    private static final String UPSTOX_CONNECTION_DATE = "conn_date_upst";

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
    }

    /**
     * Put long.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void putLong(Context context, String key, long value) {
        getPreferences(context).edit().putLong(key, value).commit();
    }

    /**
     * Gets long.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     * @return the long
     */
    public static long getLong(Context context, String key, long value) {
        return getPreferences(context).getLong(key, value);
    }

    /**
     * Put string.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void putString(Context context, String key, String value) {
        getPreferences(context).edit().putString(key, value).commit();
    }

    /**
     * Gets string.
     *
     * @param context  the context
     * @param key      the key
     * @param defValue the def value
     * @return the string
     */
    public static String getString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /**
     * Gets int.
     *
     * @param context the context
     * @param key     the key
     * @param def     the def
     * @return the int
     */
    public static int getInt(Context context, String key, int def) {
        return getPreferences(context).getInt(key, def);
    }

    /**
     * Put int.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void putInt(Context context, String key, int value) {
        getPreferences(context).edit().putInt(key, value).commit();
    }

    /**
     * Gets boolean.
     *
     * @param context the context
     * @param key     the key
     * @param def     the def
     * @return the boolean
     */
    public static boolean getBoolean(Context context, String key, boolean def) {
        return getPreferences(context).getBoolean(key, def);
    }

    /**
     * Put boolean.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void putBoolean(Context context, String key, boolean value) {
        getPreferences(context).edit().putBoolean(key, value).commit();
    }

    /**
     * Clear.
     *
     * @param context the context
     */
    public static void clear(Context context) {
        getPreferences(context).edit().clear().commit();
    }

    public static void setSyncCount(Context context, int count) {
        putInt(context, "syncTime", count);
    }

    public static int getSyncCount(Context context) {
        return getInt(context,"syncTime",0);
    }

    public static void setPackageChangedStatus(Context context, boolean isTrue) {
        putBoolean(context, "packagechanged", isTrue);
    }

    public static boolean getPackageChangedStatus(Context applicationContext) {
        return false;
    }

    public static void setRegistration(Context context, boolean isTrue) {
        putBoolean(context, IS_REG, isTrue);
    }

    public static boolean isRegistered(Context context) {

        return getBoolean(context,IS_REG,false);
    }

    public static void setCurrentDate(Context context, String date) {
        putString(context, "currentdate", date);
    }

    public static String getCurrentDate(Context context) {

        return getString(context,"currentdate","");
    }

    public static void setDashboardAction(Context context, String action) {
        putString(context, DASHBOARD_ACTION, action);
    }

    public static String getDashboardAction(Context context) {

        return getString(context,DASHBOARD_ACTION,"");
    }

    public static void setSetUpUrl(Context context, String url) {
        putString(context, UPSTOX_SETUP_URL, url);
    }

    public static String getSetUpUrl(Context context){
        return getString(context,UPSTOX_SETUP_URL,"");
    }

    public static void setUpstoxToken(Context context, String url) {
        putString(context, UPSTOX_TOKEN, url);
    }

    public static String getUpstoxToken(Context context){
        return getString(context,UPSTOX_TOKEN,"");
    }

    public static void setUpstoxCode(Context context, String code) {
        putString(context, UPSTOX_CODE, code);
    }

    public static String getUpstoxCode(Context context){
        return getString(context,UPSTOX_CODE,"");
    }

    public static void setUpstoxApiKey(Context context, String apikey) {
        putString(context, UPSTOX_API_KEY, apikey);
    }

    public static String getUpstoxApiKey(Context context){
        return getString(context,UPSTOX_API_KEY,"");
    }

    public static void setUpstoxKeySecret(Context context, String apikey) {
        putString(context, UPSTOX_KEY_SECRET, apikey);
    }

    public static String getUpstoxKeySecret(Context context){
        return getString(context,UPSTOX_KEY_SECRET,"");
    }
    public static void setUpstoxKeyOrderType(Context context, String apikey) {
        putString(context, UPSTOX_KEY_ORDER_TYPE, apikey);
    }

    public static String getUpstoxKeyOrderType(Context context){
        return getString(context,UPSTOX_KEY_ORDER_TYPE,"");
    }

    public static void setUpstoxConnectionDate(Context context, String apikey) {
        putString(context, UPSTOX_CONNECTION_DATE, apikey);
    }

    public static String getUpstoxConnectionDate(Context context){
        return getString(context,UPSTOX_CONNECTION_DATE,"");
    }

    public static boolean isPaid(Context context){
        return EntPreferences.getBoolean(context, EntConstants.IS_PAID,false);
    }
}
