package android.sud.com.entryntarget.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.sud.com.entryntarget.database.DBAdapter;
import android.sud.com.entryntarget.database.DBAdapterBackup;
import android.sud.com.entryntarget.models.Stocks;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileUtil {
    private static final FileUtil ourInstance = new FileUtil();
    private static Context mContext;

    public static FileUtil getInstance(Context context) {
        mContext = context;
        return ourInstance;
    }

    private FileUtil() {
    }

    public void getHistory(){
        new readTriggers().execute();
    }

    protected synchronized void readFileData(){
        String path = Environment.getExternalStorageDirectory().toString()+"/26JUN";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {
            Log.d("Files", "FileName:" + files[i].getName());
            if(files[i].getName().contains("_F1")){

            }else {
                readFileContent(files[i]);
            }
            //break;
        }
    }

    class readTriggers extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            readFileData();
            return null;
        }
    }


    protected synchronized void readFileContent(File file){
        StringBuilder text = new StringBuilder();
        BufferedReader br = null;
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            br = new BufferedReader(new FileReader(file));
            String line;
            DBAdapter databse = DBAdapter.getDBAdapter(mContext);
            while ((line = br.readLine()) != null) {
                text.append(line);
                //Log.i("Files", "text : "+text+" : end");
                String[] stoksInfo = line.split(",");
                String date = stoksInfo[1];
                String time = stoksInfo[2];
                if(time.equalsIgnoreCase("15:15")){
                    //DBAdapterBackup.getDBAdapter(mContext).closeAllOrders(date);
                    databse.closeAllOrders();
                    break;
                }
                //text.append('\n');
                Stocks stocks = new Stocks();
                stocks.setSymbol(stoksInfo[0]);
                stocks.setLow(stoksInfo[5]);
                stocks.setLtP(stoksInfo[6]);
                stocks.setHigh(stoksInfo[4]);
                stocks.setOpen(stoksInfo[3]);
                stocks.setPreviousClose("0");
                databse.extractStockData(stocks,date);
            }
        }catch (IOException e) {
            e.printStackTrace();

        }
        finally{
            try {
                if(br!=null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
