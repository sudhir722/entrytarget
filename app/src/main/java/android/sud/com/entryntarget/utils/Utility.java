package android.sud.com.entryntarget.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.sud.com.entryntarget.utility.EntPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Utility {
    private static final Utility ourInstance = new Utility();

    public static Utility getInstance() {
        return ourInstance;
    }

    private Utility() {
    }

    public void addAutoStartup(Context context) {

        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if  (list.size() > 0) {
                context.startActivity(intent);
            }
        } catch (Exception e) {
            Log.e("exc" , String.valueOf(e));
        }
    }
    public  double convertTwoDecimal(Double dValue){

        DecimalFormat df = new DecimalFormat("#.00");
        String value = df.format(dValue);
        return convertDouble(value);
    }

    public double convertTriggerPrice(double price){
        double affectedPrice = Math.round(price * 100.0) / 100.0;
        DecimalFormat df = new DecimalFormat("#.#");
        return convertDouble(df.format(affectedPrice));
    }

    public double convertDouble(String value){
        if(value.contentEquals(",")){
            value = value.replace(",","");
        }
        if(TextUtils.isEmpty(value)){
            return 0.0;
        }else{
            if(value.contains(",")){
                value = value.replaceAll(",","");
            }
            return Double.valueOf(value);
        }
    }

    public String formatDate() {
        /*return "23-Sep-18";*/
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }

    public String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss");
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }
    public String getDateTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd, hh:mm");
        Date currentDate = new Date(time);
        return sdf.format(currentDate);
    }

    public String getDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM");
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }

    public String getDayNseFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }
    public String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }

    public String getTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM, hh:mm");
        Date currentDate = new Date(time);
        return sdf.format(currentDate);
    }

    public int getMinuteInterval(int minute) {
        if(minute>=0 && minute <=5){
            return 1;
        }else if(minute > 5  && minute <=10){
            return 2;
        }else if(minute > 10  && minute <=15){
            return 3;
        }else if(minute > 15  && minute <=20){
            return 4;
        }else if(minute > 20  && minute <=25){
            return 5;
        }else if(minute > 25  && minute <=30){
            return 6;
        }else if(minute > 30  && minute <=35){
            return 7;
        }else if(minute > 35  && minute <=40){
            return 8;
        }else if(minute > 40  && minute <=45){
            return 9;
        }else if(minute > 45  && minute <=50){
            return 10;
        }else if(minute > 50  && minute <=55){
            return 11;
        }else if(minute > 55  && minute <=60){
            return 12;
        }else{
            return -1;
        }
    }

    public double calculateAR(double low, double close, double high,double previousClose) {
        if(close > high){
            high = close;
        }
        if(close < low){
            low = close;
        }
        double hL = high- low;
        if(previousClose>0) {
            double hPC = Math.abs(high - previousClose);
            double lPC = Math.abs(low - previousClose);
            double max1 = Math.max(hL,hPC);
            double max2 = Math.max(max1,lPC);
            return convertTwoDecimal(max2);
        }else{
            return convertTwoDecimal(hL);
        }
    }

    public  double calculateATR(Context context,String symbol,String tableName){
        return 0;
    }

    public  double calculateATR(Context context,double ar,double previousAtr){
        //=(previousatr * 13 + curratr)/14
        return convertTwoDecimal((previousAtr * 13 + ar)/14);
    }


    public String parseJSON(String dailyResponse){
        dailyResponse = dailyResponse.trim();
        try {
            dailyResponse = dailyResponse.replace(" ","");
            dailyResponse = dailyResponse.replace("</table>","");
            dailyResponse = dailyResponse.replace(":</div>","");
            dailyResponse = dailyResponse.trim();
            dailyResponse = dailyResponse.substring(dailyResponse.lastIndexOf(">")+1, dailyResponse.length());
            return dailyResponse;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }


}
