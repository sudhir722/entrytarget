package android.sud.com.entryntarget.webservice;


import android.sud.com.entryntarget.models.StockModel;
import android.sud.com.entryntarget.services.models.CancelOrderModel;
import android.sud.com.entryntarget.services.models.ModifyOrderModel;
import android.sud.com.entryntarget.services.models.PlaceOrderRequest;
import android.sud.com.entryntarget.services.models.ScriptModel;
import android.sud.com.entryntarget.services.models.UserModel;
import android.sud.com.entryntarget.upstockService.AccessToken;
import android.sud.com.entryntarget.upstockService.Profile;
import android.sud.com.entryntarget.upstockService.ProfileBalance;
import android.sud.com.entryntarget.upstockService.TokenRequest;
import android.sud.com.entryntarget.upstockService.common.models.UpstoxResponse;

import com.google.gson.JsonObject;

import java.util.concurrent.CompletableFuture;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of ContactList
    */
    @GET("/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json")
    Call<StockModel> getMyJSON();

    @GET("/live_market/dynaContent/live_watch/stock_watch/bankNiftyStockWatch.json")
    //@GET("/live_market/dynaContent/live_watch/stock_watch/foSecStockWatch.json")
    Call<StockModel> getBankNifty();


    @GET("/live_market/dynaContent/live_watch/stock_watch/foSecStockWatch.json")
    Call<StockModel> getFNO();

    //@GET("/live_market/dynaContent/live_watch/stock_watch/bankNiftyStockWatch.json")
    @GET("/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json")
    Call<StockModel> getNifty();

    //Nifty
    @GET("/products/dynaContent/equities/indices/historicalindices.jsp?indexType=NIFTY%2050&fromDate=17-04-2018&toDate=04-06-2018")
    Call<String> getHistoricalData();


    @POST("/userProfile")
    Call<JsonObject> getUserProfile(@Body UserModel token);


    @POST("/placeOrder")
    Call<JsonObject> placeNewOrder(@Body PlaceOrderRequest token);

    @POST("/placeOrderSL")
    Call<JsonObject> placeNewOrderSl(@Body PlaceOrderRequest token);

    @POST("/placeOrderOCO")
    Call<JsonObject> placeNewOrderOCO(@Body PlaceOrderRequest token);

    @POST("/getOrder")
    Call<JsonObject> getOrders();

    @POST("/modifyOrder")
    Call<JsonObject> modifyOrder(@Body ModifyOrderModel orderInfo);

    @POST("/cancelOrder")
    Call<JsonObject> cancelOrder(@Body CancelOrderModel orderInfo);


    @POST("/getLivePriceInfo")
    Call<JsonObject> getLiveOrder(@Body ScriptModel request);

    @GET("/live/profile/balance")
    Call<UpstoxResponse<ProfileBalance>> getUserBalance();

    @GET("/index/profile")
    Call<UpstoxResponse<Profile>> getUserProfile();

    @Headers("Content-Type: application/json")
    @POST("/index/oauth/token")
    Call<CompletableFuture<AccessToken>> getAccessToken(@Header("Authorization") String token, @Body TokenRequest tokenRequest);

    @POST("/placeOrderSL")
    Call<JsonObject> placerderSl(@Body PlaceOrderRequest token);
}
