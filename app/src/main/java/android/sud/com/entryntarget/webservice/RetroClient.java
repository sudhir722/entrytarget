package android.sud.com.entryntarget.webservice;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetroClient {
    /********
     * URLS
     *******/
    private static final String ROOT_URL = "https://www.nseindia.com/";
    private static final String ROOT_URL_SERVER = "http://10.229.100.184:8081/";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getAutomatedServerInstane() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL_SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static Retrofit getStringRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getNodeJsApiService() {
        return getAutomatedServerInstane().create(ApiService.class);
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiServiceString() {
        return getStringRetrofitInstance().create(ApiService.class);
    }
}
